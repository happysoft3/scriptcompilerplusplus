
#include "premodule.h"
#include "functionPool.h"
#include "scriptString.h"
#include "scriptProcess.h"
#include "scriptStream.h"
#include "macroSwap.h"
#include "makeFunction.h"
#include "preprocess\importLib.h"
#include "tokenizer.h"
#include "scriptFunction.h"
#include "scriptVariable.h"
#include "scriptTypes.h"
#include "global/builtinsDef.h"

#define USE_LATEST_VERSION_

PreModule::PreModule(const std::string &name, std::shared_ptr<ScriptStream> mainStream)
{
	m_functionPool = std::make_shared<FunctionPool>();
	m_strManager = std::make_shared<ScriptString>();
	m_name = name;
	m_typeChecker = std::make_unique<ScriptTypes>();

    m_mainStreamRef = mainStream;
}

PreModule::~PreModule()
{ }

std::string PreModule::AdditionalPath()
{
    std::shared_ptr<ScriptStream> mainstream(m_mainStreamRef.lock());

    if (!mainstream)
        return{};

    std::string topPath = mainstream->HeaderPath();

    if (m_name.length() <= topPath.length())
        return{};

    std::string temp = m_name.substr(topPath.length());
    size_t seppos = temp.find_last_of('\\');

    return temp.substr(1, seppos - 1);
}

std::list<std::string> PreModule::GetExternList() const
{
    std::list<std::string> ext;

    for (auto &ef : m_exflist)
        ext.push_back(ef->FunctionName());
    return ext;
}

bool PreModule::Load()
{
    std::shared_ptr<ScriptStream> mainstream = m_mainStreamRef.lock();

    if (!mainstream)
        return false;

	m_scrStream = std::make_shared<ScriptStream>(m_name);
	
    m_scrStream->SetHeaderPath(FileStream::GetFilepath(m_name));
	
	if (m_scrStream->FileProcess())
	{
		m_processes.emplace_back(std::make_unique<Tokenizer>(m_scrStream, m_strManager));
        m_processes.emplace_back(std::make_unique<ImportLib>(m_scrStream, m_strManager, mainstream));
		m_processes.emplace_back(std::make_unique<MacroSwap>(m_scrStream, mainstream));
		m_processes.emplace_back(std::make_unique<MakeFunction>(m_scrStream, m_functionPool));

		bool pResult = true;
		std::string errorProcess;

		while (m_processes.size())
		{
			if (pResult)
			{
				m_processes.front()->Initialize();
				pResult = m_processes.front()->Process();

				m_processes.front()->Deinitialize(pResult);
				if (!pResult)
				{
					if (!errorProcess.length())
						errorProcess = m_processes.front()->ProcessName();
				}
			}
			m_processes.pop_front();
		}
		if (pResult == false)
			std::cout << "compile error -" << errorProcess << std::endl;
		else
		{
            MakeExternalFunction();
			/*if (!MakeExternalFunction())
				return false;*/
		}
		return pResult;
	}
	else
		std::cout << m_name << " load fail\n";
	return false;
}

bool PreModule::External(std::shared_ptr<FunctionPool> modf)
{
	for (const auto &fn : m_exflist)
	{
		if (modf->GetFunctionNode(fn->FunctionName()))
		{
			return false;
		}
		else
		{
			modf->Append(fn->FunctionName(), m_typeChecker->GetTypeName(fn->GetReturnType()));
			auto &&cf = modf->GetLatestFunction();
			auto &&arglist = fn->GetArgumentList();

			for (auto ref : arglist)
			{
				auto orgvar = ref.lock();
				ScriptVariable *var = cf->CreateVariable();

				if (!cf->AppendVariable(orgvar->Name()))
					return false;
				var->SetArgument();
				var->SetType(orgvar->Type());
				if (orgvar->IsPointer())
					var->SetPointer();
			}
		}
	}
	return true;
}

void PreModule::CollectExternalMethod(std::map<std::string, std::shared_ptr<ScriptFunction>> &functionTable)
{
	for (const auto &fn : m_exflist)
	{
		auto keyIterator = functionTable.find(fn->FunctionName());
		if (keyIterator == functionTable.end())
		{
			functionTable.emplace(fn->FunctionName(), fn);
		}
	}
}

std::string PreModule::GetExternalFunctionName(const std::string &raw)
{
	std::string exportKey = "export ", check;
	std::string needInit = "needinit ";

	if (std::equal(exportKey.begin(), exportKey.end(), raw.begin()))
	{
		std::string check = raw.substr(exportKey.length());
		if (std::equal(needInit.begin(), needInit.end(), check.begin()))	//needinit keyword
			return check.substr(needInit.length());
		else
			return check;
	}
	return{};
}

void PreModule::pushExternFunction(std::shared_ptr<ScriptFunction> &fn)
{
    m_exflist.push_back(fn);
    m_extFnMap[fn->FunctionName()] = std::prev(m_exflist.end());
}

void PreModule::eraseExternFunction(const std::string &id)
{
    auto iter = m_extFnMap.find(id);

    if (iter != m_extFnMap.cend())
    {
        m_exflist.erase(iter->second);
        m_extFnMap.erase(iter);
    }
}

bool PreModule::MakeExternalFunction()
{
	int count = m_strManager->Count();
	std::string fn;

#ifndef USE_LATEST_VERSION_
	for (int i = 1; i < count; i++)
	{
		fn = GetExternalFunctionName(m_strManager->FindStringRaw(i));
		auto f = m_functionPool->GetFunctionNode(fn);

		if (f)
			m_exflist.emplace_back(f);
		/*else
			return false;*/
	}
#else
        m_functionPool->Preorder([this](std::shared_ptr<ScriptFunction> &f) { pushExternFunction(f); });

        /*else
        return false;*/
    eraseExternFunction(MODULE_ENTRY_FUNCTION);
#endif
	return true;
}
