
#include "scrFileWatcher.h"
#include "abstractFileListener.h"
#include "utils\stringUtils.h"

#include <windows.h>

ScrFileWatcher::ScrFileWatcher(const std::string &directory)
{
	m_bRunning = false;
	StringUtils::AnsiToUnicode(directory, m_sDir);
}

ScrFileWatcher::~ScrFileWatcher()
{
	Stop();
}

std::string ScrFileWatcher::GetWatchDirectory()
{
	std::string watchpath;

	StringUtils::UnicodeToAnsi(m_sDir, watchpath);
	return watchpath;
}

void ScrFileWatcher::AddFileChangeListener(AbstractFileListener *listener)
{
	m_Listeners.push_back(listener);
}

bool ScrFileWatcher::Start()
{
	if (m_bRunning)
		return false;
	m_bRunning = true;
	m_pFileWatcherThread = std::unique_ptr<std::thread>(new std::thread([this]() { this->Monitoring(); }));
	m_pFileWatcherThread->detach();
	return true;
}

void ScrFileWatcher::Stop()
{
	if (m_bRunning)
		m_bRunning = false;
}

bool ScrFileWatcher::RunningState() const
{
	return m_bRunning;
}

void ScrFileWatcher::OnFileChange(const std::wstring &sFile)
{
	for (auto &listener : m_Listeners)
		listener->OnFileChange(sFile);
}

void ScrFileWatcher::OnFileAdded(const std::wstring &sFile)
{
	for (auto &listener : m_Listeners)
		listener->OnFileAdded(sFile);
}

void ScrFileWatcher::OnFileRemoved(const std::wstring &sFile)
{
	for (auto &listener : m_Listeners)
		listener->OnFileRemoved(sFile);
}

void ScrFileWatcher::OnFileRenamed(const std::wstring &sFile)
{
	for (auto &listener : m_Listeners)
		listener->OnFileRenamed(sFile);
}

void ScrFileWatcher::Monitoring()
{
	HANDLE hDir = CreateFile(m_sDir.c_str(), // pointer to the file name
		FILE_LIST_DIRECTORY,                // access (read/write) mode
		FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,  // share mode
		NULL,                               // security descriptor
		OPEN_EXISTING,                      // how to create
		FILE_FLAG_BACKUP_SEMANTICS,         // file attributes
		NULL                                // file with attributes to copy
		);
	wchar_t filename[MAX_PATH];
	FILE_NOTIFY_INFORMATION Buffer[1024];
	DWORD BytesReturned;
	while (ReadDirectoryChangesW(
		hDir,                                  // handle to directory
		&Buffer,                                    // read results buffer
		sizeof(Buffer),                                // length of buffer
		FALSE,                                 // monitoring option
		FILE_NOTIFY_CHANGE_SECURITY |
		FILE_NOTIFY_CHANGE_CREATION |
		FILE_NOTIFY_CHANGE_LAST_ACCESS |
		FILE_NOTIFY_CHANGE_LAST_WRITE |
		FILE_NOTIFY_CHANGE_SIZE |
		FILE_NOTIFY_CHANGE_ATTRIBUTES |
		FILE_NOTIFY_CHANGE_DIR_NAME |
		FILE_NOTIFY_CHANGE_FILE_NAME,            // filter conditions
		&BytesReturned,              // bytes returned
		NULL,                          // overlapped buffer
		NULL// completion routine
		) && m_bRunning)
	{
		int offset = 0;
		FILE_NOTIFY_INFORMATION* pNotify;
		pNotify = (FILE_NOTIFY_INFORMATION*)((char*)Buffer + offset);
		wcscpy(filename, L"");

		wcsncpy(filename, pNotify->FileName, pNotify->FileNameLength / 2);

		filename[pNotify->FileNameLength / 2] = NULL;

		switch (Buffer[0].Action)
		{
		case FILE_ACTION_MODIFIED:
			OnFileChange(filename);
			break;
		case FILE_ACTION_ADDED:
			OnFileAdded(filename);
			break;
		case FILE_ACTION_REMOVED:
			OnFileRemoved(filename);
			break;
		case FILE_ACTION_RENAMED_OLD_NAME:
			OnFileRenamed(filename);
			break;
		case FILE_ACTION_RENAMED_NEW_NAME:
			OnFileRenamed(filename);
			break;
		}
	}

	CloseHandle(hDir);
}