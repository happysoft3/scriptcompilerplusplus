
#ifndef PRE_MODULE_PROC_H__
#define PRE_MODULE_PROC_H__

#include "scriptProcess.h"

#include <memory>
#include <list>

class ScriptStream;
class FunctionPool;
class PreModule;
class PremoduleProc : public ScriptProcess
{
private:
	std::weak_ptr<ScriptStream> m_streamRef;
	std::weak_ptr<FunctionPool> m_functionPoolRef;
	std::string m_additionalPath;
	std::list<std::shared_ptr<PreModule>> m_premods;

	std::list<std::string> m_errorStrings;

public:
	explicit PremoduleProc(std::shared_ptr<FunctionPool> fpool, std::shared_ptr<ScriptStream> stream);
	virtual ~PremoduleProc() override;

private:
	virtual void Initialize() override;
	bool FetchExternFunctions();
	virtual bool Process() override;
};

#endif