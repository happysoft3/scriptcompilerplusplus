
#include "postStream.h"
#include "postScrData.h"
#include "postScrFunction.h"
#include <algorithm>
#include <iterator>

PostStream::PostStream(std::shared_ptr<PostScrData> postdata, const std::string &filename)
	: FileStream(filename)
{
	m_postdata = postdata;
	m_endofproc = false;
}

PostStream::~PostStream()
{ }

bool PostStream::ReadStrHeader()
{
	uint32_t fieldLength = 4;
	std::string header;

	header.reserve(fieldLength);
	while (fieldLength--)
	{
		if (m_rawstream.size())
		{
			header.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return false;
	}
	return header == "STRG";
}

uint32_t PostStream::ReadStrCount()
{
	uint32_t fieldLength = 4;
	std::vector<uint8_t> field;

	field.reserve(fieldLength);
	while (fieldLength--)
	{
		if (m_rawstream.size())
		{
			field.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return -1;
	}
	return *reinterpret_cast<uint32_t *>(field.data());
}

bool PostStream::ReadStrSingle(uint32_t strCount)
{
	if (strCount == 0)
		return true;

	std::vector<uint8_t> lengthFieldV;
	uint32_t lengthFieldLength = 4;

	lengthFieldV.reserve(lengthFieldLength);
	if (strCount--)	//skip. 0 index string
	{
		while (lengthFieldLength--)
		{
			if (m_rawstream.size())
			{
				lengthFieldV.push_back(m_rawstream.front());
				m_rawstream.pop_front();
			}
			else
				return false;
		}
		uint32_t skipFieldLength = *reinterpret_cast<uint32_t *>(lengthFieldV.data());
		while (skipFieldLength--)
		{
			if (m_rawstream.size())
				m_rawstream.pop_front();
			else
				return false;
		}
		m_postdata.lock()->AppendScrString(std::string("NOXSCRIPT3.0"));
	}
	lengthFieldV.clear();
	std::vector<uint8_t> strFieldV;
	uint32_t strLength = 0;
	strFieldV.reserve(0x1ff);
	while (strCount--)
	{
		lengthFieldLength = 4;
		while (lengthFieldLength--)
		{
			if (m_rawstream.size())
			{
				lengthFieldV.push_back(m_rawstream.front());
				m_rawstream.pop_front();
			}
			else
				return false;
		}
		strLength = *reinterpret_cast<uint32_t *>(lengthFieldV.data());
		lengthFieldV.clear();
		while (strLength--)
		{
			if (m_rawstream.size())
			{
				strFieldV.push_back(m_rawstream.front());
				m_rawstream.pop_front();
			}
			else
				return false;
		}
		
		m_postdata.lock()->AppendScrString(std::string(reinterpret_cast<char*>(strFieldV.data()), strFieldV.size()));
		strFieldV.clear();
	}
	return true;
}

bool PostStream::ReadStrSection()
{
	while (ReadStrHeader())
	{
		uint32_t strcount = ReadStrCount();

		if (strcount == -1)
			break;
		if (ReadStrSingle(strcount))
			return true;
		break;
	}
	return false;
}

bool PostStream::ReadHeader()
{
	uint32_t fieldLength = 8;
	std::string header;

	header.reserve(fieldLength);
	while (fieldLength --)
	{
		if (m_rawstream.size())
		{
			header.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return false;
	}
	return header == "SCRIPT03";
}

bool PostStream::ReadCodeHeader()
{
	uint32_t fieldLength = 4;
	std::string header;

	header.reserve(fieldLength);
	while (fieldLength--)
	{
		if (m_rawstream.size())
		{
			header.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return false;
	}
	return header == "CODE";
}

uint32_t PostStream::ReadFunctionCount()
{
	uint32_t fieldLength = 4;
	std::vector<uint8_t> numberField;

	numberField.reserve(fieldLength);
	while (fieldLength--)
	{
		if (m_rawstream.size())
		{
			numberField.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return -1;
	}
	return *reinterpret_cast<uint32_t *>(numberField.data());
}

bool PostStream::ReadFunctionMetaData()
{
	uint32_t headerLength = 4;
	std::string functionHeader;

	functionHeader.reserve(headerLength);
	while (headerLength--)
	{
		if (m_rawstream.size())
		{
			functionHeader.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return false;
	}
	if (functionHeader != "FUNC")
		return false;
	uint32_t nFieldLength = 4;
	std::vector<uint8_t> numberField;

	numberField.reserve(nFieldLength);
	while (nFieldLength--)
	{
		if (m_rawstream.size())
		{
			numberField.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return false;
	}
	uint32_t functionNameLength = *reinterpret_cast<uint32_t *>(numberField.data());
	if (functionNameLength == 0)
		return false;
	std::string functionName;
	functionName.reserve(256);
	while (functionNameLength--)
	{
		if (m_rawstream.size())
		{
			functionName.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return false;
	}
	//return, argCount field
	uint32_t retFieldLength = 8;
	std::vector<uint8_t> metaV;
	metaV.reserve(retFieldLength);
	while (retFieldLength--)
	{
		if (m_rawstream.size())
		{
			metaV.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return false;
	}
	m_postdata.lock()->AppendScrFunction(functionName);
	std::shared_ptr<PostScrFunction> func = m_postdata.lock()->LastFunction();
	uint32_t *metaStream = reinterpret_cast<uint32_t *>(metaV.data());

	func->SetMetaData(functionName, metaStream[0], metaStream[1]);
	return true;
}

bool PostStream::ReadFunctionSymbSingle(uint32_t count)
{
	std::vector<uint8_t> nField;
	uint32_t fieldLength = 4;
	std::list<uint32_t> syList;

	nField.reserve(fieldLength);
	while (count)
	{
		while (fieldLength--)
		{
			if (m_rawstream.size())
			{
				nField.push_back(m_rawstream.front());
				m_rawstream.pop_front();
			}
			else
				return false;
		}
		syList.push_back(*reinterpret_cast<uint32_t *>(nField.data()));
		nField.clear();
		fieldLength = 4;
		--count;
	}
	std::shared_ptr<PostScrFunction> last = m_postdata.lock()->LastFunction();

	if (last)
		last->SetSymbolList(std::move(syList));
	else
		return false;
	return true;
}

bool PostStream::ReadFunctionSymbField()
{
	uint32_t headerLength = 4;
	std::string symbHeader;

	symbHeader.reserve(headerLength);
	while (headerLength--)
	{
		if (m_rawstream.size())
		{
			symbHeader.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return false;
	}
	if (symbHeader != "SYMB")
		return false;
	uint32_t lengthFieldLength = 8;
	std::vector<uint8_t> lengthV;

	lengthV.reserve(lengthFieldLength);
	while (lengthFieldLength--)
	{
		if (m_rawstream.size())
		{
			lengthV.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return false;
	}
	uint32_t elementCount = *reinterpret_cast<uint32_t *>(lengthV.data());
	if (elementCount)
		return ReadFunctionSymbSingle(elementCount);

	return true;
}

bool PostStream::ReadFunctionDataSingle(uint32_t count)
{
	std::vector<uint8_t> nField;
	std::list<uint32_t> codeList;
	uint32_t readLength = 4;

	nField.reserve(readLength);
	while (count)
	{
		while (readLength--)
		{
			if (m_rawstream.size())
			{
				nField.push_back(m_rawstream.front());
				m_rawstream.pop_front();
			}
			else
				return false;
		}
		codeList.push_back(*reinterpret_cast<uint32_t *>(nField.data()));
		nField.clear();
		readLength = 4;
		count--;
	}
	std::shared_ptr<PostScrFunction> last = m_postdata.lock()->LastFunction();

	if (last)
		last->SetCodedataList(std::move(codeList));
	else
		return false;
	return true;
}

bool PostStream::ReadFunctionDataField()
{
	uint32_t dataHeaderLength = 4;
	std::string dataHeader;

	dataHeader.reserve(dataHeaderLength);
	while (dataHeaderLength--)
	{
		if (m_rawstream.size())
		{
			dataHeader.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return false;
	}
	if (dataHeader != "DATA")
		return false;
	std::vector<uint8_t> lengthFieldV;
	uint32_t nfieldLength = 4;

	lengthFieldV.reserve(nfieldLength);
	while (nfieldLength--)
	{
		if (m_rawstream.size())
		{
			lengthFieldV.push_back(m_rawstream.front());
			m_rawstream.pop_front();
		}
		else
			return false;
	}
	uint32_t codelength = *reinterpret_cast<uint32_t *>(lengthFieldV.data());
	uint32_t dwordsize = 4;

	if (codelength)
		return ReadFunctionDataSingle(codelength / dwordsize);
	return true;
}

bool PostStream::ReadFunctionSingle(uint32_t functionCount)
{
	while (functionCount--)
	{
		if (!ReadFunctionMetaData())
			return false;
		if (!ReadFunctionSymbField())
			return false;
		if (!ReadFunctionDataField())
			return false;
	}
	return true;
}

bool PostStream::ReadFunctionSection()
{
	while (ReadCodeHeader())
	{
		uint32_t functionCount = ReadFunctionCount();

		if (functionCount == -1)
			break;
		if (ReadFunctionSingle(functionCount))
			return true;
		break;
	}
	return false;
}

bool PostStream::ReadObjFormat()
{
	if (m_postdata.expired())
		return false;
	if (!ReadHeader())
		return false;
	if (!ReadStrSection())
		return false;
	if (!ReadFunctionSection())
		return false;
	return true;
}

void PostStream::ReportReadComplete(std::vector<uint8_t> *fstream)
{
	std::transform(fstream->begin(), fstream->end(), std::insert_iterator<std::list<uint8_t>>(m_rawstream, m_rawstream.begin()), [](uint8_t u) { return u; });
	
	m_endofproc = ReadObjFormat();
}