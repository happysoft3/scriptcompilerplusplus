
#ifndef SCRIPT_PROCESS_H__
#define SCRIPT_PROCESS_H__

#include <string>
#include <iostream>

class ScriptProcess
{
public:
	explicit ScriptProcess();
	virtual ~ScriptProcess();

	virtual void Initialize() = 0;
	virtual bool Process() = 0;
	virtual void Deinitialize(bool) {}

	virtual std::string ProcessName();

protected:
	template <class T>
	void ErrorReport(T arg) { std::cout << arg << std::endl; }
	template <class T, class... Types>
	void ErrorReport(T arg, Types... args)
	{
		std::cout << arg << static_cast<char>(0x20);
		ErrorReport(args...);
	}

	//todo. 오류 발생 시 알림메서드 추가
};

#endif