
#ifndef MAKE_FUNCTION_H__
#define MAKE_FUNCTION_H__

#include "scriptProcess.h"
#include <memory>
#include <list>
#include <string>

class ScriptStream;
class FunctionPool;
class ScriptTypes;
class ScriptFunction;
class TokenGroup;
class ScriptUnit;

//@brief. 함수와 메타 데이터 (함수 리턴타입+인자 정보)를 여기에서 만듭니다
class MakeFunction : public ScriptProcess
{
private:
	std::weak_ptr<ScriptStream> m_streamRef;
	std::weak_ptr<FunctionPool> m_functionPoolRef;

	std::weak_ptr<ScriptFunction> m_globalFunction;
	std::weak_ptr<ScriptFunction> m_currentFunction;
	std::unique_ptr<ScriptTypes> m_typeChecker;

	std::weak_ptr<TokenGroup> m_rootGroup;

	using macroErrorTypes = std::tuple<std::string, std::weak_ptr<ScriptUnit>>;
	std::list<macroErrorTypes> m_errorList;

	const std::string m_externKey;
	const std::string m_overrideKey;

public:
	explicit MakeFunction(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<FunctionPool> functionPoolRef);
	virtual ~MakeFunction() override;

private:
	virtual void Initialize() override;
	bool MakeArgs(std::shared_ptr<ScriptUnit> argGroup, const std::string &functionName);
	bool Make(std::shared_ptr<ScriptUnit> currentGroup);
	virtual bool Process() override;
	void PrintErrorList();
	virtual void Deinitialize(bool pResult);

	virtual std::string ProcessName() override { return "makeFunction"; }
};

#endif