
#ifndef SCR_FILE_WATCHER_H__
#define SCR_FILE_WATCHER_H__

#define _CRT_SECURE_NO_WARNINGS

#include <atomic>
#include <vector>
#include <memory>
#include <thread>

class AbstractFileListener;

class ScrFileWatcher
{
private:
	std::atomic<bool> m_bRunning;
	std::wstring m_sDir;
	std::vector<AbstractFileListener*> m_Listeners;
	std::unique_ptr<std::thread> m_pFileWatcherThread;

public:
	explicit ScrFileWatcher(const std::string &directory);
	~ScrFileWatcher();

	std::string GetWatchDirectory();
	void AddFileChangeListener(AbstractFileListener *listener);
	bool Start();
	void Stop();
	bool RunningState() const;

private:
	void OnFileChange(const std::wstring &sFile);
	void OnFileAdded(const std::wstring &sFile);
	void OnFileRemoved(const std::wstring &sFile);
	void OnFileRenamed(const std::wstring &sFile);

	void Monitoring();
};

#endif