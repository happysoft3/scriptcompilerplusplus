
#ifndef BUILTINS_LOADER_H__
#define BUILTINS_LOADER_H__

#include <memory>
#include <string>

class FunctionPool;
class Builtins;

//@brief. ��Ʈ�� ������
class BuiltinsLoader
{
private:
	std::weak_ptr<FunctionPool> m_functionPool;
	std::unique_ptr<Builtins> m_builtins;

public:
	explicit BuiltinsLoader(std::shared_ptr<FunctionPool> functionPool);
	~BuiltinsLoader();

	void SetBuiltinsPath(const std::string &path);
	bool Load();
};

#endif