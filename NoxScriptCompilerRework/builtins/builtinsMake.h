
#ifndef BUILTINS_MAKE_H__
#define BUILTINS_MAKE_H__

#include "preprocess\scriptProcess.h"

#include <memory>
#include <list>

class ScriptStream;
class FunctionPool;
class ScriptTypes;
class TokenGroup;
class ScriptUnit;

//std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<FunctionPool> functionPoolRef

class BuiltinsMake : public ScriptProcess
{
private:
	std::weak_ptr<ScriptStream> m_streamRef;
	std::weak_ptr<FunctionPool> m_functionPoolRef;
	std::unique_ptr<ScriptTypes> m_typeChecker;

	std::weak_ptr<TokenGroup> m_rootGroup;

public:
	explicit BuiltinsMake(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<FunctionPool> functionPoolRef);
	virtual ~BuiltinsMake() override;

private:
	virtual void Initialize() override;
	bool FunctionDetector(const std::list<std::shared_ptr<ScriptUnit>> &fqueue);
	bool Make(std::shared_ptr<ScriptUnit> symbols);
	virtual bool Process() override;
	virtual void Deinitialize(bool pResult) override;
};

#endif