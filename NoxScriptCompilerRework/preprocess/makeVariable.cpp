
#include "makeVariable.h"
#include "scriptStream.h"
#include "functionPool.h"
#include "scriptFunction.h"
#include "scriptVariable.h"
#include "tokenGroup.h"
#include "scriptToken.h"
#include "scriptTypes.h"

#include <sstream>

//#define DEBUG_VAR_DECLARE		//변수 생성정보 실시간 확인하려면 코멘트 해제

MakeVariable::MakeVariable(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<FunctionPool> functionPoolRef)
	: ScriptProcess()
{
	m_typeChecker = std::make_unique<ScriptTypes>();
	m_streamRef = streamRef;
	m_functionPoolRef = functionPoolRef;
	m_latestTypeNumber = 0;
}

MakeVariable::~MakeVariable()
{ }

void MakeVariable::Initialize()
{
	std::shared_ptr<FunctionPool> functionPool(m_functionPoolRef.lock());

	m_globalFunction = functionPool->GetGlobalFunction();

	functionPool->ResetListCursorPosition();

	std::shared_ptr<ScriptStream> streamRef(m_streamRef.lock());

	m_rootGroup = streamRef->GetTokenRoot();
}

bool MakeVariable::PushStacklist(std::shared_ptr<ScriptUnit> unit, std::shared_ptr<ScriptFunction> currentFunction)
{
	if (m_varStack.size())
	{
		m_varStack.push_back(unit);
		if (unit->UnitType() == ScriptUnit::SINGLE)
		{
			ScriptToken *scrToken = dynamic_cast<ScriptToken *>(unit.get());
			//타입+(포인터연산자)+키워드
			if (scrToken->Type() == ScriptToken::KEYWORD)	//일반변수 생성처리
			{
				ScriptVariable *var = currentFunction->CreateVariable();

				//m_varStack.front().lock()->SetEliminate();	//타입 제거
				ScriptToken *typeHelper = dynamic_cast<ScriptToken*>(m_varStack.front().lock().get());
				m_varStack.pop_front();
				std::shared_ptr<ScriptUnit> voper = m_varStack.front().lock();
				bool isPointer = dynamic_cast<ScriptToken *>(voper.get())->Type() == ScriptToken::OPERATOR;
				if (currentFunction->AppendVariable(unit->UnitName()))
				{
					var->SetType(m_latestTypeNumber);
					if (isPointer)
					{
						var->SetPointer();
						voper->SetEliminate();	//포인터 연산자 제거
					}
					if (typeHelper->Type() == ScriptToken::OPERATOR)
					{
						if (typeHelper->FirstChar() == ',')
							typeHelper->SetName(std::string(1, ';'));
					}
					else
						typeHelper->SetEliminate();	////타입 제거
					unit->SetDeclare();
					m_varStack.clear();
#ifdef DEBUG_VAR_DECLARE
					std::cout << "Def Variable::" << unit->UnitName() << ", pointer?=" << isPointer << " in function: " << currentFunction->FunctionName() << std::endl;
#endif
					return true;
				}
				else
				{
					m_errorList.emplace_back("variable name has been duplicated", unit, currentFunction);
					return false;
				}
			}
		}
		else	//배열처리
		{
			/* 배열 선언 예시**
			int arr[3];
			int arr[] = { 3, 4 };
			int arr[3] = { 5,4,1 };
			*/
			//m_varStack.front().lock()->SetEliminate();	//변수 타입 제거
			ScriptToken *typeHelper = dynamic_cast<ScriptToken*>(m_varStack.front().lock().get());
			m_varStack.pop_front();
			if (unit->Length() > 1)	// int arr[41] or int arr[]
			{
				m_errorList.emplace_back("array size declare have only number", unit, currentFunction);
				return false;
			}
			uint32_t arrsize = 0;
			if (unit->Length() == 1)
			{
				std::shared_ptr<ScriptUnit> temptemp = dynamic_cast<TokenGroup *>(unit.get())->Front();
				ScriptToken *arrsizeToken = dynamic_cast<ScriptToken *>(temptemp.get());

				if (arrsizeToken->Type() != ScriptToken::NUMBER)
				{
					m_errorList.emplace_back("array size declare have only number", unit, currentFunction);
					return false;
				}
				std::stringstream ss;
				ss << arrsizeToken->Name();
				ss >> arrsize;
			}
			if (arrsize == 1)
			{
				m_errorList.emplace_back("array size > 1", unit, currentFunction);
			}
			else if (m_varStack.size() == 1)
			{
				ScriptVariable *arrvar = currentFunction->CreateVariable();
				if (currentFunction->AppendVariable(unit->UnitName()))
				{
					unit->SetDeclare();
					arrvar->SetType(m_latestTypeNumber);
					arrvar->SetSize(arrsize);
					m_varStack.clear();
					if (typeHelper->Type() == ScriptToken::OPERATOR)
					{
						if (typeHelper->FirstChar() == ',')
							typeHelper->SetName(std::string(1, ';'));
					}
					else
						typeHelper->SetEliminate();	////타입 제거
#ifdef DEBUG_VAR_DECLARE
					std::cout << "Def Array Variable::" << unit->UnitName() << ", size?=" << arrsize << " in function: " << currentFunction->FunctionName() << std::endl;
#endif
					return true;
				}
				m_errorList.emplace_back("array variable name has been duplicated", unit, currentFunction);
			}
			else
			{
				m_errorList.emplace_back("wrong array declared", unit, currentFunction);
			}
		}
	}
	return false;
}

uint32_t MakeVariable::GetArrayInitSize(TokenGroup *initList, ScriptVariable *var)	//here
{
	std::list<std::shared_ptr<ScriptUnit>> tokens = initList->GetList(), destlist;	//added: destlist 를 추가
	uint32_t resultsize = 0;
	bool count = true;

	while (tokens.size())
	{
		if (count)
		{
			std::shared_ptr<ScriptUnit> assign(new TokenGroup), arrindex(new ScriptToken(std::to_string(resultsize), tokens.front()->Linenumber(), ScriptToken::NUMBER));
			TokenGroup *scope = dynamic_cast<TokenGroup*>(assign.get());

			scope->SetGroupType(TokenGroup::ARRAY_TYPE);
			scope->SetGroupName(var->Name());
			scope->Append(arrindex);

			std::shared_ptr<ScriptUnit> assignop(new ScriptToken(std::string(1, '='), tokens.front()->Linenumber(), ScriptToken::OPERATOR));

			destlist.push_back(assign);
			destlist.push_back(assignop);
			++resultsize;
			count = false;
		}
		else
		{
			if (tokens.front()->UnitType() == ScriptUnit::SINGLE)
			{
				ScriptToken *scrToken = dynamic_cast<ScriptToken*>(tokens.front().get());

				if (scrToken->Type() == ScriptToken::OPERATOR)
				{
					if (scrToken->FirstChar() == ',')
					{
						count = true;
						scrToken->SetFirstChar(';');
						//scrToken->SetEliminate();	//added
					}
				}
			}
			
		}
		destlist.push_back(tokens.front());
		tokens.pop_front();
	}
	if (destlist.size())
	{
		destlist.push_back(std::make_shared<ScriptToken>(std::string(1, ';'), destlist.back()->Linenumber(), ScriptToken::OPERATOR));
		initList->SetList(destlist);
	}
	return resultsize;
}

bool MakeVariable::PushArrayStacklist(std::shared_ptr<ScriptUnit> unit, std::shared_ptr<ScriptFunction> currentFunction)
{
	//배열 초기화리스트 스택: 배열변수+ '=' + 초기화 그룹
	if (unit != m_arrInitStack.front())
		m_arrInitStack.push_back(unit);
	while (m_arrInitStack.size() == 3)
	{
		std::shared_ptr<ScriptUnit> varunit = m_arrInitStack.front();	//변수유닛
		m_arrInitStack.pop_front();
		std::shared_ptr<ScriptUnit> assignop = m_arrInitStack.front();	//대입연산자 - 무조건
		TokenGroup *node = nullptr;
		ScriptVariable *var = nullptr;

		if (assignop->UnitType() != ScriptUnit::SINGLE)
		{
			/*std::cout << varunit->UnitName() << std::endl;
			std::cout << assignop->UnitName() << std::endl;
			std::cout << m_arrInitStack.back()->UnitName() << std::endl;*/
			m_errorList.emplace_back("token error", assignop, currentFunction);
			break;
		}
		if (unit->UnitType() == ScriptUnit::GROUP)
		{
			if (!(dynamic_cast<TokenGroup*>(unit.get())->GroupType() == TokenGroup::MID_TYPE))
			{
				m_errorList.emplace_back("error", varunit, currentFunction);
				break;
			}

			var = currentFunction->GetVariable(varunit->UnitName());

			if (var == nullptr)
			{
				m_errorList.emplace_back("error", varunit, currentFunction);
				break;
			}
			node = dynamic_cast<TokenGroup*>(unit.get());
			uint32_t initsize = GetArrayInitSize(node, var/*->Name()*/);

			if (var->Size() == 0)
			{
				if (initsize > 1)
					var->SetSize(initsize);
				else
				{
					m_errorList.emplace_back("fail to array size deduction", unit, currentFunction);
					break;
				}
				//assignop->SetEliminate();	//comment
				assignop->SetUnitName(std::string(1, ';'));	//added
				std::cout << "here\n";
				assignop->SetAssignop(false);	//added
			}
			else if (var->Size() != initsize)
			{
				m_errorList.emplace_back("error", unit, currentFunction);
			}
			else
			{
				//assignop->SetEliminate();	//comment
				assignop->SetUnitName(std::string(1, ';'));	//added
				std::cout << "here\n";
				assignop->SetAssignop(false);	//added
				//std::cout << "array OK!!" << varunit->UnitName() << std::endl;
			}
			
		}
		m_arrInitStack.clear();
		break;
	}
	return false;
}

bool MakeVariable::Make(std::shared_ptr<ScriptUnit> currentGroup, std::shared_ptr<ScriptFunction> currentFunction)
{
	std::shared_ptr<FunctionPool> functionPool(m_functionPoolRef.lock());
	std::list<std::shared_ptr<ScriptUnit>> tokens = dynamic_cast<TokenGroup *>(currentGroup.get())->GetList();

	//bool typecame = false;	//added

	if (!currentFunction)
		return false;

	while (tokens.size() > 0)
	{
		//**변수 생성규칙**//
		//타입+키워드= 일반변수
		//타입+'*'+키워드= 포인터변수
		//타입+그룹[= 배열변수
		if (tokens.front()->FunctionEntry())
		{
			if (!Make(tokens.front(), functionPool->NextFunction()))		///!함수노드 그룹인 경우 재귀호출!///
				return false;
		}
		else if (tokens.front()->UnitType() == ScriptUnit::GROUP)
		{
			//그룹의 경우  3가지로 나뉩니다. 배열의 경우 [그룹, 함수호출 (그룹, 배열 값 {그룹
			if (m_varStack.size())
			{
				if (dynamic_cast<TokenGroup *>(tokens.front().get())->GroupType() == TokenGroup::ARRAY_TYPE)
				{
					if (PushStacklist(tokens.front(), currentFunction))
					{
						//get last array variable
						m_arrInitStack.push_back(tokens.front());
					}
				}
				else
				{
					m_errorList.emplace_back("is array?", tokens.front(), currentFunction);
					return false;	//error todo
				}
			}
			////added
			//else
			//{
			//	if (dynamic_cast<TokenGroup *>(tokens.front().get())->GroupType() == TokenGroup::MID_TYPE)
			//	{
			//		if (!Make(tokens.front(), currentFunction))
			//			return false;
			//	}
			//}
			////added/
		}
		else
		{
			ScriptToken *scrToken = dynamic_cast<ScriptToken *>(tokens.front().get());

			if (scrToken->Type() == ScriptToken::KEYWORD)
			{
				if (m_typeChecker->IsValid(scrToken->Name()))	//타입이 왔습니다
				{
					m_latestTypeNumber = m_typeChecker->GetTypeId(scrToken->Name());
					if (!m_varStack.size())
					{
						m_varStack.push_back(tokens.front());
						//typecame = true;	//added
					}
					else
					{
						m_errorList.emplace_back("syntax error::type+type??", tokens.front(), currentFunction);
						return false;
					}
				}
				else if (m_varStack.size()) //그냥 키워드. 원래대로라면 변수명이 와야합니다
				{
					PushStacklist(tokens.front(), currentFunction);
				}
			}
			else if (scrToken->Type() == ScriptToken::OPERATOR)
			{
				if (m_varStack.size())
				{
					if (scrToken->FirstChar() == '*')	//포인터 타입
						PushStacklist(tokens.front(), currentFunction);
					else
					{
						m_errorList.emplace_back("undefined type operator", tokens.front(), currentFunction);
					}
				}
				else
				{
					switch (scrToken->FirstChar())
					{
					case ',':	//타입 이어서 // , 으로 이어진 타입입니다
						//if (typecame)	//added
						{
							m_varStack.push_back(tokens.front());
							
						}	//added
						//m_arrInitStack.clear();	//added
						//break;	//added
					case ';':	//줄 끝이면 배열 스택 종료
						m_arrInitStack.clear();
						//typecame = false;	//added
						break;
					default:
						break;
					}
				}
			}
		}
		if (m_errorList.size())	//오류 누적 시 중단
			return false;

		if (m_arrInitStack.size())
			PushArrayStacklist(tokens.front(), currentFunction);

		tokens.pop_front();
	}
	return true;
}


bool MakeVariable::AddVariable(std::shared_ptr<ScriptUnit> typekey, std::shared_ptr<ScriptUnit> varunit, std::shared_ptr<ScriptFunction> cFunction, bool setpointer)
{
	if (m_typeChecker->IsValid(varunit->UnitName()))
	{
		m_errorList.emplace_back("can not variable name with typename", varunit, cFunction);
		return false;
	}
	ScriptVariable *var = cFunction->CreateVariable();

	if (cFunction != m_globalFunction.lock())	//전역노드에서 변수명 충돌체크
	{
		if (m_globalFunction.lock()->GetVariable(varunit->UnitName()) != nullptr)
		{
			m_errorList.emplace_back("varname has been duplicate with global", varunit, cFunction);
			return false;
		}
	}
	if (!cFunction->AppendVariable(varunit->UnitName()))
	{
		m_errorList.emplace_back("varname has been duplicate", varunit, cFunction);
		return false;
	}
	var->SetType(m_typeChecker->GetTypeId(typekey->UnitName()));
	if (setpointer)
		var->SetPointer();
	if (varunit->UnitType() == ScriptUnit::GROUP)	//is array
	{
		if (setpointer)
		{
			m_errorList.emplace_back("array is not be pointer", varunit, cFunction);
			return false;
		}
		
		TokenGroup *arrscope = static_cast<TokenGroup*>(varunit.get());
		if (arrscope->Length())
		{
			std::shared_ptr<ScriptUnit> arrsizeunit = arrscope->Front();
			if (arrsizeunit->UnitTokenType() == ScriptToken::NUMBER)
			{
				std::stringstream ss;
				uint32_t arrsizevalue = 0;
				ss << arrsizeunit->UnitName();
				ss >> arrsizevalue;
				if (arrsizevalue > 1)
					var->SetSize(arrsizevalue);
				else
				{
					m_errorList.emplace_back("array size must be greater than 1", varunit, cFunction);
					return false;
				}
			}
			else
			{
				m_errorList.emplace_back("array size must have only const number", varunit, cFunction);
				return false;
			}
		}
		else
			var->SetSize(0);		//need array size deduction
	}
	varunit->SetDeclare();
	return true;
}

bool MakeVariable::ArrayInitList(std::shared_ptr<ScriptUnit> arr, std::shared_ptr<ScriptUnit> initlist, std::shared_ptr<ScriptFunction> cFunction)
{
	TokenGroup *arrinit = static_cast<TokenGroup*>(initlist.get());	//here

	if (arrinit->GroupType() != TokenGroup::MID_TYPE)
	{
		m_errorList.emplace_back("error::usage:: int arr[]={a,b,c};", initlist, cFunction);
		return false;
	}
	ScriptVariable *var = cFunction->GetVariable(arr->UnitName());
	if (var == nullptr)
		return false;

	std::list<std::shared_ptr<ScriptUnit>> tokens = arrinit->GetList(), destlist;	//added: destlist 를 추가
	uint32_t resultsize = 0;
	bool count = true;

	while (tokens.size())
	{
		if (count)
		{
			std::shared_ptr<ScriptUnit> assign(new TokenGroup), arrindex(new ScriptToken(std::to_string(resultsize), tokens.front()->Linenumber(), ScriptToken::NUMBER));
			TokenGroup *scope = dynamic_cast<TokenGroup*>(assign.get());

			scope->SetGroupType(TokenGroup::ARRAY_TYPE);
			scope->SetGroupName(var->Name());
			scope->Append(arrindex);

			std::shared_ptr<ScriptUnit> assignop(new ScriptToken(std::string(1, '='), tokens.front()->Linenumber(), ScriptToken::OPERATOR));

			destlist.push_back(assign);
			destlist.push_back(assignop);
			++resultsize;
			count = false;
		}
		else
		{
			if (tokens.front()->UnitType() == ScriptUnit::SINGLE)
			{
				ScriptToken *scrToken = dynamic_cast<ScriptToken*>(tokens.front().get());

				if (scrToken->Type() == ScriptToken::OPERATOR)
				{
					if (scrToken->FirstChar() == ',')
					{
						count = true;
						scrToken->SetFirstChar(';');
					}
				}
			}

		}
		destlist.push_back(tokens.front());
		tokens.pop_front();
	}
	if (destlist.size())
	{
		destlist.push_back(std::make_shared<ScriptToken>(std::string(1, ';'), destlist.back()->Linenumber(), ScriptToken::OPERATOR));
		arrinit->SetList(destlist);
	}

	if (resultsize > 1)
	{
		if (var->Size() == 0)
			var->SetSize(resultsize);
		else if (resultsize != var->Size())
		{
			m_errorList.emplace_back("mismatch array size " + std::to_string(resultsize) + "vs" + std::to_string(var->Size()), initlist, cFunction);
			return false;
		}
	}
	else
	{
		m_errorList.emplace_back("array size >= 2", initlist, cFunction);
		return false;
	}

	return true;
}

bool MakeVariable::DeclareArray(std::shared_ptr<ScriptUnit> typekey, std::shared_ptr<ScriptUnit> arr, std::shared_ptr<ScriptFunction> cFunction)
{
	if (AddVariable(typekey, arr, cFunction))
	{
		return true;
	}
	return false;
}

bool MakeVariable::DeclareVariable(std::shared_ptr<ScriptUnit> typekey, std::shared_ptr<ScriptUnit> front, std::shared_ptr<ScriptUnit> back, std::shared_ptr<ScriptFunction> cFunction)
{
	if (front->UnitTokenType() == ScriptToken::KEYWORD)	// int var0
	{
		if (back->UnitTokenType() == ScriptToken::OPERATOR)
		{
			if (AddVariable(typekey, front, cFunction))
				return true;
		}
	}
	else if (front->UnitTokenType() == ScriptToken::OPERATOR)	// int *var0
	{
		if (front->UnitName() == "*")	// int */var0
		{
			if (back->UnitTokenType() == ScriptToken::KEYWORD)	//int *var0
			{
				if (AddVariable(typekey, back, cFunction, true))
				{
					front->SetEliminate();		//포인터 연산자 제거
					return true;
				}
			}
		}
		else
			m_errorList.emplace_back("undefined declare operator", front, cFunction);
	}
	m_errorList.emplace_back("syntax error:: wrong declare", front, cFunction);
	return false;
}

bool MakeVariable::Make2(std::shared_ptr<ScriptUnit> currentGroup, std::shared_ptr<ScriptFunction> cFunction)
{
	std::shared_ptr<FunctionPool> functionPool(m_functionPoolRef.lock());
	std::list<std::shared_ptr<ScriptUnit>> tokens = dynamic_cast<TokenGroup *>(currentGroup.get())->GetList();
	std::shared_ptr<ScriptUnit> prev, typekey, typekeytemp, arrkey;
	bool pointerkey = false;

	if (!cFunction)
		return false;

	tokens.emplace_back(new ScriptToken);
	while (tokens.size())
	{
		do
		{
			if (!prev || prev->Eliminated()) break;
			if (prev->FunctionEntry())	//!함수진입!//
			{
				if (typekey)
				{
					m_errorList.emplace_back("syntax error:: type+function?", prev, cFunction);
					return false;
				}
				if (!Make2(prev, functionPool->NextFunction()))
					return false;
			}
			else if (prev->UnitType() == ScriptUnit::GROUP)
			{
				if (typekey)
				{
					if (static_cast<TokenGroup*>(prev.get())->GroupType() == TokenGroup::ARRAY_TYPE)
					{
						if (DeclareArray(typekey, prev, cFunction))
						{
							arrkey = prev;
							typekeytemp = typekey;
							typekey.reset();
						}
						else
							return false;
					}
					else
					{
						m_errorList.emplace_back("syntax error:: type+() or {}", prev, cFunction);
						return false;
					}
				}
				else if (!Make2(prev, cFunction))
					return false;
			}
			else if (typekey)
			{
				if (DeclareVariable(typekey, prev, tokens.front(), cFunction))
				{
					typekeytemp = typekey;
					typekey.reset();
				}
				else
					return false;
			}
			else
			{
				if (prev->UnitTokenType() == ScriptToken::KEYWORD)
				{
					//타입 또는 변수명
					if (m_typeChecker->IsValid(prev->UnitName()))	//타입입니다
					{
						if (!typekey)
						{
							typekey = prev;
							typekey->SetEliminate();
							break;
						}
						else
						{
							m_errorList.emplace_back("syntax error:: type+type", prev, cFunction);
							return false;
						}
					}
				}
				else if (prev->UnitTokenType() == ScriptToken::OPERATOR)
				{
					//, ;
					ScriptToken *symbol = static_cast<ScriptToken*>(prev.get());
					if (symbol->Name().length() == 1)
					{
						if (symbol->FirstChar() == ',')
						{
							if (typekeytemp)
							{
								typekey = typekeytemp;
								symbol->SetName(std::string(1, ';'));
								arrkey.reset();	//added
								break;
							}
						}
						else if (symbol->FirstChar() == ';')
						{
							typekey.reset();
							typekeytemp.reset();
							arrkey.reset();
						}
						else if (symbol->FirstChar() == '=')
						{
							if (arrkey)
							{
								if (tokens.front()->UnitType() == ScriptUnit::GROUP)
								{
									if (ArrayInitList(arrkey, tokens.front(), cFunction))
									{
										symbol->SetName(std::string(1, ';'));	//here
										arrkey.reset();
									}
									else
									{
										m_errorList.emplace_back("array initializer list error2", prev, cFunction);
										return false;
									}
								}
								else
								{
									m_errorList.emplace_back("array initializer list error", tokens.front(), cFunction);
									return false;
								}
							}
						}
					}
				}
			}
			if (typekey)
			{
				m_errorList.emplace_back("syntax error:: type+nothing?", typekey, cFunction);
				return false;
			}
		} while (false);

		prev = tokens.front();
		tokens.pop_front();
	}
	return true;
}

bool MakeVariable::Process()
{
	std::shared_ptr<FunctionPool> functionPool = m_functionPoolRef.lock();

    functionPool->AddThisFunctionIdVar();

	bool pResult = Make2(m_rootGroup.lock(), m_globalFunction.lock());

	if (pResult)
	{

		pResult = functionPool->FunctionRefreshAll();
		//functionPool->PrintAll();
	}
	else
		PrintErrorList();
	return pResult;
}

void MakeVariable::Deinitialize(bool pResult)
{
	if (pResult)
	{
		std::shared_ptr<TokenGroup> root(m_rootGroup.lock());

		root->Refresh();
		//root->PrintAll();

		
	}
	m_arrInitStack.clear();
}

void MakeVariable::PrintErrorList()
{
	while (m_errorList.size() != 0)
	{
		std::string errorMessage = std::get<0>(m_errorList.front());
		std::shared_ptr<ScriptUnit> scrUnit = std::get<1>(m_errorList.front()).lock();
		std::shared_ptr<ScriptFunction> errFunction = std::get<2>(m_errorList.front()).lock();

		if (errFunction)
			ErrorReport("error::", scrUnit->UnitName(), " :: ", errorMessage, " in function: ", errFunction->FunctionName(), "::line:", scrUnit->Linenumber());
		else
			ErrorReport("error::", scrUnit->UnitName(), " :: ", errorMessage, "::line:", scrUnit->Linenumber());
		m_errorList.pop_front();
	}
}