
#include "premoduleProc.h"
#include "premodule.h"
#include "scriptStream.h"
#include "tokenGroup.h"
#include "functionPool.h"
#include "scriptFunction.h"

#include "utils/stringHelper.h"

using namespace _StringHelper;

PremoduleProc::PremoduleProc(std::shared_ptr<FunctionPool> fpool, std::shared_ptr<ScriptStream> stream)
	: ScriptProcess()
{
	m_streamRef = stream;
	m_functionPoolRef = fpool;

    if (stream)
	    m_additionalPath = stream->HeaderPath();
}

PremoduleProc::~PremoduleProc()
{ }

void PremoduleProc::Initialize()
{
    //std::shared_ptr<ScriptStream> stream = m_streamRef.lock();

    //if (!stream)
    //    return;

    //while (1)
    //{
    //    auto mod = stream->PopModuleObject();

    //    if (!mod)
    //        break;

    //    m_premods.emplace_back(mod);
    //}
}

bool PremoduleProc::FetchExternFunctions()
{
	std::map<std::string, std::shared_ptr<ScriptFunction>> ftable;
	auto fpool = m_functionPoolRef.lock();
    auto stream = m_streamRef.lock();

    stream->PreorderModule([&ftable](PreModule &mod) { mod.CollectExternalMethod(ftable); });
    //for (const auto &module : m_premods)
    //{
    //    module->CollectExternalMethod(ftable);
    //    stream->AppendModuleObject(module); //added
    //}
	
	auto &&root = m_streamRef.lock()->GetTokenRoot();
	for (auto const &element : ftable)
	{
        element.second->SetExtern();    //added
		if (!fpool->CopyFunction(element.second))
		{
			auto &&f = fpool->GetFunctionNode(element.first);

			if (f->IsOverride())	//메인소스에서 재정의
				continue;
			//std::cout << "the function was conflicted with main function. " << element.first << std::endl;
			//return false;
			m_errorStrings.push_back(stringFormat("%s function was conflicted with main function", element.first));
			continue;
		}
		TokenGroup *exg(new TokenGroup(root));

		exg->SetGroupName(element.first);
		exg->SetGroupType(TokenGroup::GroupTypes::MID_TYPE);
		exg->SetFunctionEntry();
		root->Append(std::shared_ptr<TokenGroup>(exg));
	}
	return true;
}

bool PremoduleProc::Process()
{
	FetchExternFunctions();

	for (const auto &s : m_errorStrings)
		std::cout << stringFormat("error:: %s\n", s);

	return m_errorStrings.empty();
}