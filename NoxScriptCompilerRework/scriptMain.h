
#ifndef SCRIPT_MAIN_H__
#define SCRIPT_MAIN_H__

#include <memory>
#include <string>
#include <map>

class PathReader;
class ScriptCompile;
class ScrFileWatcher;
class ScrFileListener;
class SystemEnv;

class ScriptMain
{
private:
	std::unique_ptr<PathReader> m_pathReader;
	std::unique_ptr<ScriptCompile> m_compile;
	std::unique_ptr<ScrFileWatcher> m_fileWatcher;
	std::unique_ptr<ScrFileListener> m_fileListener;
	std::string m_scrpath;
	std::string m_lasterrorPath;
	std::map<std::string, std::string> m_urlPack;
	std::shared_ptr<SystemEnv> m_systemEnv;

public:
	explicit ScriptMain(std::shared_ptr<SystemEnv> sysEnv);
	~ScriptMain();

	bool Initialize();

private:
	int DirExists(const std::string &path);
	bool PushURL(const std::string &key);
	bool FetchPaths();
	bool InitBeforeCompile();
	bool InitFileWatcher();
};

#endif