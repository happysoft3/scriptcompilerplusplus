
#ifndef POST_MODULE_H__
#define POST_MODULE_H__

#include <string>
#include <memory>
#include <list>
#include <functional>

class PostScrData;
class PostStream;
class FunctionPool;
class PreModule;

class PostModule
{
private:
	std::shared_ptr<PostScrData> m_postData;
	std::unique_ptr<PostStream> m_postStream;
	std::string m_fullname;
	std::list<std::string> m_exportList;
	std::list<std::string> m_needInit;

    std::shared_ptr<FunctionPool> m_preprocFnMap;

public:
	explicit PostModule(const std::string &fullname);
	~PostModule();

private:
    void findExternSymbol(PreModule &mod);

public:
	bool Load(std::shared_ptr<FunctionPool> preprocFnMap, PreModule &mod);
	bool Insert(std::shared_ptr<PostScrData> mainData, const std::function<uint32_t(const std::string&)> &findExternFunc);
	std::list<std::string> NeedInitList() const { return m_needInit; }

private:
	bool FindExportSymbol();
	void PrintError();
};

#endif