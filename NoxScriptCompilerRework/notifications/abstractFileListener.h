
#ifndef ABSTRACT_FILE_LISTENER_H__
#define ABSTRACT_FILE_LISTENER_H__

#include <string>

class AbstractFileListener
{
public:
	enum actionTypes
	{
		ACTION_NONE,
		ACTION_CHANGED,
		ACTION_ADDED,
		ACTION_DELETED,
		ACTION_RENAMED
	};

public:
	explicit AbstractFileListener();
	virtual ~AbstractFileListener();

	virtual void OnFileChange(const std::wstring& path) = 0;
	virtual void OnFileAdded(const std::wstring& path) = 0;
	virtual void OnFileRemoved(const std::wstring& path) = 0;
	virtual void OnFileRenamed(const std::wstring& path) = 0;
};

#endif