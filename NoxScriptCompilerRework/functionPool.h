
#ifndef FUNCTION_POOL_H__
#define FUNCTION_POOL_H__

#include <map>
#include <list>
#include <memory>
#include <string>
#include <set>
#include <functional>

class ScriptFunction;
class ScriptTypes;

//@brief. 함수 목록을 가지고있는 객체
class FunctionPool
{
private:
	std::map<std::string, std::shared_ptr<ScriptFunction>> m_functionMap;
	std::list<std::weak_ptr<ScriptFunction>> m_functionList;
	std::list<std::weak_ptr<ScriptFunction>>::iterator m_functionListCursor;
	uint32_t m_orderedId;
	const std::string m_globalFunctionName;

	std::shared_ptr<ScriptTypes> m_typeChecker;
	std::weak_ptr<ScriptFunction> m_globalFunction;

public:
	explicit FunctionPool();
	~FunctionPool();

	void MakeGlobal();
	bool Append(const std::string &functionName, const std::string &retTypekey = {});
	bool CopyFunction(const std::shared_ptr<ScriptFunction> &scrmethod);
	std::shared_ptr<ScriptFunction> GetFunctionNode(const std::string &functionKey);
	std::string GlobalFunctionName() const { return m_globalFunctionName; }
	std::shared_ptr<ScriptFunction> GetGlobalFunction();
	void ResetListCursorPosition();
	std::shared_ptr<ScriptFunction> NextFunction();
	std::shared_ptr<ScriptFunction> GetLatestFunction();

	bool FunctionRefreshAll();

	std::map<std::string, uint32_t> GetSpecialFunctions();

	void PrintAll();
    void AddThisFunctionIdVar();
    void Preorder(std::function<void(std::shared_ptr<ScriptFunction>&)>&&f);
};

#endif