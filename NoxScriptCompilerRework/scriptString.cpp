
#include "scriptString.h"
#include "utils\stringUtils.h"
#include <algorithm>
#include <iostream>
#include <sstream>

ScriptString::ScriptString()
	: m_strIdentifier("#STR")
{
	m_strCount = 1;
}

ScriptString::~ScriptString()
{ }

uint32_t ScriptString::Append(const std::string &str)
{
	auto keyIterator = m_idMap.find(str);

	if (keyIterator == m_idMap.end())
	{
		m_idMap.emplace(str, m_strCount);
		m_strMap.emplace(m_strCount, str);
		return m_strCount++;
	}
	else
		return keyIterator->second;
}

std::string ScriptString::FindString(uint32_t strId)
{
	auto keyIterator = m_strMap.find(strId);

	if (keyIterator != m_strMap.end())
		return keyIterator->second;
	else
		return{};
}

std::string ScriptString::FindStringRaw(uint32_t strId)
{
	std::string keyvalue = FindString(strId);

	if (keyvalue.length() > 2)
	{
		if (keyvalue.front() == '"' && keyvalue.back() == '"')
			return keyvalue.substr(1, keyvalue.length() - 2);
	}
	return{};
}

std::string ScriptString::FindStringWithKey(const std::string &keyId)
{
	if (std::equal(m_strIdentifier.begin(), m_strIdentifier.end(), keyId.begin()))
	{
		std::stringstream ss;
		std::string keyvalue = keyId.substr(m_strIdentifier.length());

		if (keyvalue.length() > 0)
		{
			uint32_t keyNumber = 0;

			ss << keyvalue;
			ss >> keyNumber;

			return FindString(keyNumber);
		}

	}
	return{};
}

std::string ScriptString::FindStringWithKeyRaw(const std::string &keyId)
{
	std::string keyvalue = FindStringWithKey(keyId);

	if (keyvalue.length() > 2)
	{
		if (keyvalue.front() == '"' && keyvalue.back() == '"')
			return keyvalue.substr(1, keyvalue.length() - 2);
	}
	return{};
}

void ScriptString::PrintAll()
{
	std::cout << "Strings::" << m_strMap.size() << std::endl;
	std::for_each(m_strMap.begin(), m_strMap.end(), [](const auto &node)
	{
		std::string encordedStr = node.second;
		std::wstring tmpstr;
		std::string dest;

		StringUtils::Utf8ToUnicode(encordedStr, tmpstr);
		StringUtils::UnicodeToAnsi(tmpstr, dest);
		std::cout << dest << std::endl;
	});
}