
#ifndef PRE_MODULE_H__
#define PRE_MODULE_H__

#include <list>
#include <memory>
#include <string>
#include <map>

class ScriptProcess;
class FunctionPool;
class ScriptString;
class ScriptStream;
class ScriptFunction;
class ScriptTypes;

class PreModule
{
private:
	std::string m_name;
	std::shared_ptr<ScriptStream> m_scrStream;
    std::weak_ptr<ScriptStream> m_mainStreamRef;
	std::list<std::unique_ptr<ScriptProcess>> m_processes;
	std::shared_ptr<FunctionPool> m_functionPool;
	std::shared_ptr<ScriptString> m_strManager;

    using scr_func_list_ty = std::list<std::shared_ptr<ScriptFunction>>;
	scr_func_list_ty m_exflist;
    std::map<std::string, scr_func_list_ty::iterator> m_extFnMap;

	std::unique_ptr<ScriptTypes> m_typeChecker;

public:
	explicit PreModule(const std::string &name, std::shared_ptr<ScriptStream> mainStream);
	~PreModule();

private:
    std::string AdditionalPath();

public:
    std::list<std::string> GetExternList() const;
	bool Load();
	bool External(std::shared_ptr<FunctionPool> modf);
	void CollectExternalMethod(std::map<std::string, std::shared_ptr<ScriptFunction>> &functionTable);

private:
	std::string GetExternalFunctionName(const std::string &raw);
    void pushExternFunction(std::shared_ptr<ScriptFunction> &fn);
    void eraseExternFunction(const std::string &id);
	bool MakeExternalFunction();
};

#endif