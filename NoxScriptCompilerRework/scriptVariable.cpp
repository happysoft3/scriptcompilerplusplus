
#include "scriptVariable.h"
#include "scriptFunction.h"
#include "scriptTypes.h"

ScriptVariable::ScriptVariable(ScriptFunction *parentFunction)
{
	m_parentFunction = parentFunction;
	m_offset = 0;
	m_size = 1;
}

ScriptVariable::~ScriptVariable()
{ }

uint32_t ScriptVariable::AlignedSize() const
{
    if (!m_size) return m_size;
    if (m_isArgument || m_isPointer)
        return 1;
    if (m_type == ScriptTypes::SCR_CHAR)
    {
        size_t byteAlign = ((m_size - 1) >> 2) + 1;

        return (m_size > 1 && byteAlign == 1) ? byteAlign + 1 : byteAlign;
        //return byteAlign;   //�Ƹ� //FIXME//
    }
    else if (m_type == ScriptTypes::SCR_SHORT)
    {
        size_t wordAlign = ((m_size - 1) >> 1) + 1;

        return (m_size > 1 && wordAlign == 1) ? wordAlign + 1 : wordAlign;
        //return wordAlign;
    }
    else return m_size;
}

