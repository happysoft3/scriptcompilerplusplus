
#include "functionPool.h"
#include "scriptFunction.h"
#include "scriptVariable.h"
#include "scriptTypes.h"

#include <algorithm>
#include <iostream>

FunctionPool::FunctionPool()
	: m_globalFunctionName("GLOBAL")
{
	m_orderedId = 1;
	m_typeChecker = std::make_shared<ScriptTypes>();
}

FunctionPool::~FunctionPool()
{ }

void FunctionPool::MakeGlobal()
{
	if (m_globalFunction.expired())
	{
		Append(m_globalFunctionName, "void");

		m_globalFunction = m_functionList.front();
		std::shared_ptr<ScriptFunction> global(m_globalFunction.lock());

		ScriptVariable *var = global->CreateVariable();
		var->SetType(m_typeChecker->GetTypeId("int"));
		global->AppendVariable("self");
		var = global->CreateVariable();
		var->SetType(m_typeChecker->GetTypeId("int"));
		global->AppendVariable("other");
		var = global->CreateVariable();
		var->SetType(m_typeChecker->GetTypeId("int"));
		global->AppendVariable("true");
		var = global->CreateVariable();
		var->SetType(m_typeChecker->GetTypeId("int"));
		global->AppendVariable("false");

		m_functionListCursor = m_functionList.begin();
	}
}

bool FunctionPool::Append(const std::string &functionName, const std::string &retTypekey)
{
	auto keyIterator = m_functionMap.find(functionName);

	if (keyIterator == m_functionMap.end())
	{
		std::shared_ptr<ScriptFunction> functionNode(new ScriptFunction(functionName, m_orderedId++));

		functionNode->RegistTypeChecker(m_typeChecker);
		functionNode->SetReturnType(retTypekey);
		m_functionMap.emplace(functionName, functionNode);
		m_functionList.push_back(functionNode);

		return true;
	}
	return false;	//already exist
}

bool FunctionPool::CopyFunction(const std::shared_ptr<ScriptFunction> &scrmethod)
{
	auto keyIterator = m_functionMap.find(scrmethod->FunctionName());

	if (keyIterator == m_functionMap.end())
	{
		scrmethod->ChangeFunctionId(m_orderedId++);
		m_functionMap.emplace(scrmethod->FunctionName(), scrmethod);
		m_functionList.push_back(scrmethod);
		return true;
	}
	return false;
}

std::shared_ptr<ScriptFunction> FunctionPool::GetFunctionNode(const std::string &functionKey)
{
	auto keyIterator = m_functionMap.find(functionKey);

	if (keyIterator != m_functionMap.end())
	{
		return keyIterator->second;
	}
	return{};
}

std::shared_ptr<ScriptFunction> FunctionPool::GetGlobalFunction()
{
	return m_globalFunction.lock();
}

void FunctionPool::ResetListCursorPosition()
{
	m_functionListCursor = m_functionList.begin();
}

std::shared_ptr<ScriptFunction> FunctionPool::NextFunction()
{
	if (m_functionListCursor != m_functionList.end())
	{
		++m_functionListCursor;
		return m_functionListCursor->lock();
	}
	return nullptr;
}

std::shared_ptr<ScriptFunction> FunctionPool::GetLatestFunction()
{
	if (m_functionList.size())
	{
		std::shared_ptr<ScriptFunction> currentFunction(m_functionList.back().lock());

		if (currentFunction)
			return currentFunction;
	}
	return nullptr;
}

bool FunctionPool::FunctionRefreshAll()
{
	std::list<std::weak_ptr<ScriptFunction>> flist = m_functionList;

	while (flist.size())
	{
		std::shared_ptr<ScriptFunction> function(flist.front().lock());

		if (!function->VarRefresh())
			return false;
		flist.pop_front();
	}
	return true;
}

std::map<std::string, uint32_t> FunctionPool::GetSpecialFunctions()
{
	std::map<std::string, uint32_t> specials;
	uint32_t flags;

	for (const auto &element : m_functionList)
	{
		auto scrFunction = element.lock();
		flags = 0;

		if (scrFunction)
		{
			if (scrFunction->IsExtern())
				flags |= 1;
			if (scrFunction->IsOverride())
				flags |= 2;
			if (flags != 0)
				specials.emplace(scrFunction->FunctionName(), flags);
		}
	}
	return specials;
}

void FunctionPool::PrintAll()
{
	std::for_each(m_functionList.begin(), m_functionList.end(), [](const std::weak_ptr<ScriptFunction> &funcRef)
	{
		std::shared_ptr<ScriptFunction> node = funcRef.lock();

		if (node)
		{
			std::cout << "DefFunction: " << node->FunctionName() << std::endl;
			node->PrintAll();
			//node->ShowCodeTokens();
			//std::cout << std::endl << std::endl;;
		}
	});
}

void FunctionPool::AddThisFunctionIdVar()
{
    auto globalFn = m_globalFunction.lock();

    for (auto &pr : m_functionMap)
    {
        if (pr.second != globalFn)
            pr.second->AddFunctionNumberMarkerVariable();
    }
}

void FunctionPool::Preorder(std::function<void(std::shared_ptr<ScriptFunction>&)>&&f)
{
    for (auto &pr : m_functionMap)
    {
        f(pr.second);
    }
}
