
#include "optypes.h"

static OptypeChecker *s_checkerInstance;

OptypeChecker::OptypeChecker()
{
	s_checkerInstance = this;
}

OptypeChecker::~OptypeChecker()
{
	s_checkerInstance = nullptr;
}

OptypeChecker *OptypeChecker::GetInstance()
{
	return s_checkerInstance;
}

bool OptypeChecker::IsAssignOpcode(OpTypes optype)
{
	switch (optype)
	{
	case OpTypes::OP_A_ADD: case OpTypes::OP_A_AND: case OpTypes::OP_A_DIV: case OpTypes::OP_A_MOD:
	case OpTypes::OP_A_MUL: case OpTypes::OP_A_OR: case OpTypes::OP_A_SAL: case OpTypes::OP_A_SAR:
	case OpTypes::OP_A_SUB: case OpTypes::OP_A_XOR:/* case OpTypes::OP_ASIGN:*/
		return true;
	}
	return false;
}

std::string OptypeChecker::GetOpcodeIdent(OpTypes optype)
{
	switch (optype)
	{
	case OpTypes::OP_ARROW: return "->";
	case OpTypes::OP_A_ADD: return "+=";
	case OpTypes::OP_A_AND: return "&=";
	case OpTypes::OP_A_DIV: return "/=";
	case OpTypes::OP_A_MOD: return "%=";
	case OpTypes::OP_A_MUL: return "*=";
	case OpTypes::OP_A_OR: return "|=";
	case OpTypes::OP_A_SAL: return "<<=";
	case OpTypes::OP_A_SAR: return ">>=";
	case OpTypes::OP_A_SUB: return "-=";
	case OpTypes::OP_A_XOR: return "^=";
	case OpTypes::OP_DEC: return "--";
	case OpTypes::OP_EQ: return "==";
	case OpTypes::OP_GE: return ">=";
	case OpTypes::OP_INC: return "++";
	case OpTypes::OP_LE: return "<=";
	case OpTypes::OP_LOGAND: return "&&";
	case OpTypes::OP_LOGOR: return "||";
	case OpTypes::OP_NE: return "!=";
	case OpTypes::OP_SAL: return "<<";
	case OpTypes::OP_SAR: return ">>";
	case OpTypes::OP_MULPLY: return "*";
	case OpTypes::OP_XOR: return "^";
	case OpTypes::OP_NOT: return "!";
	case OpTypes::OP_ASIGN: return "=";
	case OpTypes::OP_MOD: return "%";
	case OpTypes::OP_DIV: return "/";
	case OpTypes::OP_MINUS: return "-";
	case OpTypes::OP_LEFT: return ">";
	case OpTypes::OP_RIGHT: return "<";
	}
	return{};
}