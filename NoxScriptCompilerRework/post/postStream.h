
#ifndef POST_STREAM_H__
#define POST_STREAM_H__

#include "utils\fileStream.h"
#include <list>
#include <memory>

class PostScrData;

class PostStream : public FileStream
{
private:
	std::string m_filename;
	std::list<uint8_t> m_rawstream;
	std::weak_ptr<PostScrData> m_postdata;
	bool m_endofproc;

public:
	explicit PostStream(std::shared_ptr<PostScrData> postdata, const std::string &filename);
	virtual ~PostStream() override;

private:
	bool ReadStrHeader();
	uint32_t ReadStrCount();
	bool ReadStrSingle(uint32_t strCount);
	bool ReadStrSection();
	bool ReadHeader();
	bool ReadCodeHeader();
	uint32_t ReadFunctionCount();
	bool ReadFunctionMetaData();
	bool ReadFunctionSymbSingle(uint32_t count);
	bool ReadFunctionSymbField();
	bool ReadFunctionDataSingle(uint32_t count);
	bool ReadFunctionDataField();
	bool ReadFunctionSingle(uint32_t functionCount);
	bool ReadFunctionSection();
	bool ReadObjFormat();
	void ReportReadComplete(std::vector<uint8_t> *fstream) override;

public:
	bool Completed() const { return m_endofproc; }
};

#endif