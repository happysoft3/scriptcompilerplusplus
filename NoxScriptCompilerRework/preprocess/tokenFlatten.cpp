
#include "tokenFlatten.h"
#include "scriptStream.h"
#include "functionPool.h"
#include "scriptFunction.h"
#include "scriptVariable.h"
#include "tokenGroup.h"
#include "scriptToken.h"
#include "scriptTypes.h"
#include "optypes.h"
#include "preprocess_generic.h"

TokenFlatten::TokenFlatten(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<FunctionPool> functionPoolRef)
	: ScriptProcess()
{
	m_streamRef = streamRef;
	m_functionPoolRef = functionPoolRef;
	m_typeChecker = std::make_unique<ScriptTypes>();
}

TokenFlatten::~TokenFlatten()
{ }

void TokenFlatten::Initialize()
{
	std::shared_ptr<FunctionPool> functionPool(m_functionPoolRef.lock());

	m_globalFunction = functionPool->GetGlobalFunction();

	functionPool->ResetListCursorPosition();

	std::shared_ptr<ScriptStream> streamRef(m_streamRef.lock());

	m_rootGroup = streamRef->GetTokenRoot();
}

bool TokenFlatten::CheckPlusminus(const std::shared_ptr<ScriptUnit> &scr)
{
	if (scr->UnitTokenType() == ScriptToken::OPERATOR)	//here
	{
		auto token = static_cast<ScriptToken *>(scr.get());
		return (token->GetTokenOpType() == OpTypes::OP_INC) || (token->GetTokenOpType() == OpTypes::OP_DEC);
	}
	return false;
}

bool TokenFlatten::IsIncOp(const std::shared_ptr<ScriptUnit> &unit)
{
	auto token = static_cast<ScriptToken *>(unit.get());

	return token->GetTokenOpType() == OpTypes::OP_INC;
}

bool TokenFlatten::CheckKeyword(const std::shared_ptr<ScriptUnit> &scr)
{
	if (scr->UnitTokenType() == ScriptToken::KEYWORD)
	{
		//todo. 제외 else, return
		return true;
	}
	return false;
}

std::string TokenFlatten::varSetOperator(std::shared_ptr<ScriptUnit> &tok)
{
    auto currentFn = m_currentFunction.lock();

    if (currentFn)
    {
        auto var = currentFn->GetVariable(tok->UnitName());

        if (!var)
            var = m_globalFunction.lock()->GetVariable(tok->UnitName());

        if (var)
        {
            if (!var->IsPointer())
            {
                std::string ty = m_typeChecker->GetTypeName(var->Type());

                if (ty == "char")
                    return "OperatorSetByteVar";
                else if (ty == "short")
                    return "OperatorSetWordVar";
            }
            else
            {
                if (tok->UnitType() == ScriptUnit::GROUP)
                {
                    if (static_cast<TokenGroup *>(tok.get())->GroupType() == TokenGroup::GroupTypes::ARRAY_TYPE)
                    {
                        std::string ty = m_typeChecker->GetTypeName(var->Type());

                        if (ty == "char")
                            return "OperatorSetByteVar";
                        else if (ty == "short")
                            return "OperatorSetWordVar";
                    }
                }
            }
        }
        else
        {
            printf("debug::what%s %d\n", __FILE__, __LINE__);
        }
    }
    return "OperatorSetVar";
}

bool TokenFlatten::ArrayConvert(std::shared_ptr<ScriptUnit> arr, std::shared_ptr<ScriptFunction> cFunction)
{
	ScriptVariable *var = cFunction->GetVariable(arr->UnitName());
	std::shared_ptr<ScriptFunction> varf = cFunction;

	while (var == nullptr)
	{
		if (m_globalFunction.lock() != cFunction)
		{
			var = m_globalFunction.lock()->GetVariable(arr->UnitName());
			if (var != nullptr)
			{
				varf = m_globalFunction.lock();
				break;
			}
		}
		m_errorList.emplace_back("unknown keyword", arr, cFunction);
		return false;
	}
	TokenGroup *symbols = static_cast<TokenGroup*>(arr.get());	// funcIndx, voffset+ ...

	symbols->AppendFront(std::make_shared<ScriptToken>(std::string(1, ','), arr->Linenumber(), ScriptToken::OPERATOR));
	//symbols->AppendFront(std::make_shared<ScriptToken>(std::string(1, var->IsPointer() ? ',' : '+'), arr->Linenumber(), ScriptToken::OPERATOR));
	symbols->AppendFront(std::make_shared<ScriptToken>(std::to_string(var->Offset()), arr->Linenumber(), ScriptToken::OPERATOR));
	symbols->AppendFront(std::make_shared<ScriptToken>(std::string(1, ','), arr->Linenumber(), ScriptToken::OPERATOR));
	//symbols->AppendFront(std::make_shared<ScriptToken>(std::to_string(varf->FunctionNumber()), arr->Linenumber(), ScriptToken::OPERATOR));
    symbols->AppendFront(std::make_shared<ScriptToken>((varf!=cFunction) ? std::to_string(varf->FunctionNumber()) : s_this_function_id, arr->Linenumber(), ScriptToken::OPERATOR));

	symbols->SetArrayType(var->Type());
	if (var->IsPointer())
		symbols->SetPointerArray();

	return true;
}

void TokenFlatten::referenceSymb(ScriptUnit &unit, bool &scan)
{
    if (unit.UnitType() == ScriptUnit::UnitTypes::GROUP)
    {
        TokenGroup *g = static_cast<TokenGroup *>(&unit);

        if (g->GroupType() == TokenGroup::ARRAY_TYPE)
        {
            unit.SetSpecialProperty(ScriptUnit::OP_ARR_REF);
            scan = false;
            return;
        }
    }
    if (unit.UnitTokenType() == ScriptToken::KEYWORD)
    {
        unit.SetSpecialProperty(ScriptUnit::OP_RIGHT_REF);
        scan= false;
    }
}

bool TokenFlatten::Some(std::shared_ptr<ScriptUnit> currentGroup, std::shared_ptr<ScriptFunction> currentFunction)
{
	std::shared_ptr<FunctionPool> functionPool(m_functionPoolRef.lock());
	std::list<std::shared_ptr<ScriptUnit>> tokens = dynamic_cast<TokenGroup*>(currentGroup.get())->GetList();
	std::shared_ptr<ScriptUnit> prev;
	bool opscan = true;

	tokens.emplace_back(new ScriptToken);
	while (tokens.size())
	{
		do
		{
			if (!prev || prev->Eliminated()) break;
			if (prev->FunctionEntry())
			{
				if (!Some(prev, functionPool->NextFunction()))
					return false;
			}
			else if (prev->UnitType() == TokenGroup::GROUP)
			{
				if (dynamic_cast<TokenGroup*>(prev.get())->GroupType() == TokenGroup::ARRAY_TYPE)
				{
					if (!ArrayConvert(prev, currentFunction))
						return false;
					if (CheckPlusminus(tokens.front()))
					{
						prev->SetSpecialProperty(IsIncOp(tokens.front()) ? ScriptUnit::OP_AFTER_PLUS : ScriptUnit::OP_AFTER_MINUS);
						tokens.front()->SetEliminate();
					}
				}
				if (!Some(prev, currentFunction))
					return false;
			}
			else
			{
				if (prev->UnitTokenType() == ScriptToken::OPERATOR)
				{
					if (CheckPlusminus(prev))
					{
						if (CheckKeyword(tokens.front()))	// ++k
						{
							tokens.front()->SetSpecialProperty(IsIncOp(prev) ? ScriptUnit::OP_BEFORE_PLUS : ScriptUnit::OP_BEFORE_MINUS);
							prev->SetEliminate();
							break;
						}
						else if (tokens.front()->UnitType() == TokenGroup::GROUP)	// ++arr[]
						{
							if (dynamic_cast<TokenGroup*>(tokens.front().get())->GroupType() == TokenGroup::ARRAY_TYPE)
							{
								tokens.front()->SetSpecialProperty(IsIncOp(prev) ? ScriptUnit::OP_BEFORE_PLUS : ScriptUnit::OP_BEFORE_MINUS);
								prev->SetEliminate();
								break;
							}
						}
					}
					if (opscan)
					{
						if (prev->UnitName().length() == 1)
						{
							switch (prev->UnitName()[0])
							{
							case '&':
                                referenceSymb(*tokens.front(), opscan);
								break;
							case '*':
								if (tokens.front()->UnitTokenType() == ScriptToken::KEYWORD)
								{
									tokens.front()->SetSpecialProperty(ScriptUnit::OP_RIGHT_REVREF);
									opscan = false;
								}
								break;
							default:
								break;
							}
						}
						if (!opscan)
						{
							prev->SetEliminate();
							break;
						}
					}
					opscan = true;
					break;
				}
				else if (prev->UnitTokenType() == ScriptToken::KEYWORD)
				{
					if (CheckPlusminus(tokens.front()))
					{
						if (CheckKeyword(prev))		// k++
						{
							prev->SetSpecialProperty(IsIncOp(tokens.front()) ? ScriptUnit::OP_AFTER_PLUS : ScriptUnit::OP_AFTER_MINUS);
							tokens.front()->SetEliminate();
						}
					}
				}
				else if (prev->UnitTokenType() == ScriptToken::SCR_COMMIT)
				{
					opscan = true;
					break;
				}
			}
			opscan = false;
		} while (false);
		prev = tokens.front();
		tokens.pop_front();
	}
	return true;
}

//@brief. 대입연산자 처리루틴
bool TokenFlatten::Make(std::shared_ptr<ScriptUnit> currentGroup, std::shared_ptr<ScriptUnit> destGroup)
{
	std::list<std::shared_ptr<ScriptUnit>> tokens = static_cast<TokenGroup*>(currentGroup.get())->GetList();
	std::shared_ptr<ScriptUnit> prev, lvalue;

	tokens.emplace_back(new ScriptToken);

	static_cast<TokenGroup*>(currentGroup.get())->Clear();
	while (tokens.size())
	{
		do
		{
			if (!prev || prev->Eliminated()) break;
			if (tokens.front()->AssignOp() || tokens.front()->UnitName() == "=")
			{
				if (lvalue)
				{
					m_errorList.emplace_back("fatal error", prev, m_currentFunction);
					return false;
				}

				std::shared_ptr<ScriptUnit> scope(new TokenGroup);
				TokenGroup *createdNode = dynamic_cast<TokenGroup*>(scope.get());
				createdNode->SetGroupName(varSetOperator(prev));        //FIXME. 무조건, 이런식으로 줄 수 없음, char *p; p[0] = 30; 꼴이 있음.
				createdNode->SetGroupType(TokenGroup::TINY_TYPE);
                //std::cout << __FILE__ << __LINE__ << prev->UnitName() << " in " << m_currentFunction.lock()->FunctionName() << std::endl;         //여기에서, 처리하면 될듯//

				if (prev->Declared())
				{
					std::shared_ptr<ScriptUnit> declarevar(new ScriptToken(prev->UnitName(), prev->Linenumber(), ScriptToken::KEYWORD));

					declarevar->SetDeclare();
					prev->UnsetDeclare();
					static_cast<TokenGroup*>(destGroup.get())->Append(declarevar);
					static_cast<TokenGroup*>(destGroup.get())->Append(std::make_shared<ScriptToken>(std::string(1, ';'), prev->Linenumber(), ScriptToken::OPERATOR));
				}

				std::shared_ptr<ScriptUnit> lexpr(prev->Clone());	//added
				lexpr->SetLeftAssign();	//added
				createdNode->Append(lexpr);
				createdNode->Append(std::make_shared<ScriptToken>(std::string(1, ','), prev->Linenumber(), ScriptToken::OPERATOR));
				if (tokens.front()->UnitName().length() > 1)
				{
					createdNode->Append(prev);
					std::string opname = tokens.front()->UnitName();
					opname.pop_back();
					createdNode->Append(std::make_shared<ScriptToken>(opname, prev->Linenumber(), ScriptToken::OPERATOR));
				}
				tokens.front()->SetEliminate();
				static_cast<TokenGroup*>(lvalue ? lvalue.get() : destGroup.get())->Append(scope);

				lvalue = scope;
			}
			else if (prev->UnitType() == TokenGroup::GROUP)
			{
				if (prev->FunctionEntry())
				{
					m_errorList.emplace_back("why declare function in function?", prev, m_currentFunction);
					return false;
				}
				std::shared_ptr<ScriptUnit> scope(new TokenGroup);
				TokenGroup *createdNode = dynamic_cast<TokenGroup*>(scope.get());
				createdNode->SetGroupName(prev->UnitName());
				createdNode->SetGroupType(static_cast<TokenGroup*>(prev.get())->GroupType());
				createdNode->SetLinenumber(prev->Linenumber());
				if (prev->Declared())
					createdNode->SetDeclare();
				createdNode->SetSpecialProperty(prev->GetSpecialProperty());
				
				createdNode->SetArrayType(static_cast<TokenGroup*>(prev.get())->ArrayType());	//added
				createdNode->SetPointerArray(static_cast<TokenGroup *>(prev.get())->PointerArray());
				static_cast<TokenGroup*>(lvalue ? lvalue.get() : destGroup.get())->Append(scope);
				if (!Make(prev, scope))
					return false;
			}
			else
			{
				if (prev->UnitName() == ";" && lvalue)
				{
					lvalue.reset();
				}
				static_cast<TokenGroup*>(lvalue ? lvalue.get() : destGroup.get())->Append(prev);
			}
			
		} while (false);
		prev = tokens.front();
		tokens.pop_front();
	}
	return true;
}

//@brief. 실제 컴파일러 특성에 따라 전역노드는 처리 안합니다
bool TokenFlatten::FunctionOrder(std::shared_ptr<ScriptUnit> rootUnit)
{
	std::shared_ptr<FunctionPool> functionPool(m_functionPoolRef.lock());
	TokenGroup *root = dynamic_cast<TokenGroup *>(rootUnit.get());
	std::list<std::shared_ptr<ScriptUnit>> tokens = root->GetList();

	root->Clear();	//@brief. 다 비우고 다시 만듭니다
	while (tokens.size())
	{
		if (tokens.front()->FunctionEntry())
		{
			m_currentFunction = functionPool->NextFunction();
			if (!Make(tokens.front(), tokens.front()))
				return false;
			m_currentFunction = m_globalFunction;
		}
		root->Append(tokens.front());	//@brief. 합류
		tokens.pop_front();
	}
	return true;
}

bool TokenFlatten::Process()
{
	bool pResult = Some(m_rootGroup.lock(), m_globalFunction.lock());

	if (pResult)
	{
		m_rootGroup.lock()->Refresh();
		m_functionPoolRef.lock()->ResetListCursorPosition();
		if (!FunctionOrder(m_rootGroup.lock()))
			return false;
		m_rootGroup.lock()->Refresh();
	}

	if (m_errorList.size())
	{
		PrintErrorList();
		return false;
	}
	return pResult;
	
	//return false;
}

void TokenFlatten::Deinitialize(bool pResult)
{ }

void TokenFlatten::PrintErrorList()
{
	while (m_errorList.size() != 0)
	{
		std::string errorMessage = std::get<0>(m_errorList.front());
		std::shared_ptr<ScriptUnit> scrUnit = std::get<1>(m_errorList.front()).lock();
		std::shared_ptr<ScriptFunction> errFunction = std::get<2>(m_errorList.front()).lock();

		if (errFunction)
			ErrorReport("error::", scrUnit->UnitName(), " :: ", errorMessage, " in function: ", errFunction->FunctionName(), "::line:", scrUnit->Linenumber());
		else
			ErrorReport("error::", scrUnit->UnitName(), " :: ", errorMessage, "::line:", scrUnit->Linenumber());
		m_errorList.pop_front();
	}
}