
#ifndef FILE_STREAM_H__
#define FILE_STREAM_H__

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <tuple>

enum class FileMode
{
	FILE_MODE_READ,
	FILE_MODE_WRITE,
	FILE_MODE_OVERWRITE,
	FILE_MODE_READ_RANGE		//영역 단위 읽기모드
};

class FileStream
{
public:
	enum
	{
		PREVIOUS_FILENAME = 0,
		PREVIOUS_FILEMODE = 1
	};
private:
	std::vector<uint8_t> m_fstream;
	std::string m_curFileName;
	FileMode m_mode;
	uint32_t m_startReadPos;
	uint32_t m_maxReadAmount;
	std::tuple<std::string, FileMode> m_previousMode;

public:
	explicit FileStream(const std::string &_fileName, FileMode _mode = FileMode::FILE_MODE_READ);	//파일명, 파일처리 타입(읽기, 쓰기)
	FileStream(const std::string &_fileName, std::vector<uint8_t> &_cpyStream, FileMode _mode = FileMode::FILE_MODE_READ);
	explicit FileStream(const FileStream &cpy);		//복사생성자: 스트림 간 깊은복사 수행
	virtual ~FileStream();		//소멸자

private:
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream);		//파일 읽기 완료보고
	virtual void ReportReadFailed() { }
	virtual void ReportWriteReady(std::vector<uint8_t> *fstream);	//파일 쓰기 준비완료
	virtual bool CheckBeforeRead();	//파일 읽기 전 확인
	uint32_t Read();			//파일 읽기(스트림에 저장):	읽은 파일의 스트림 크기 반환
	uint32_t Write();			//파일 쓰기(스트림으로 부터 저장): 스트림 내용을 파일로 쓰기
	uint32_t ReadRange();

public:
	virtual uint32_t FileProcess();			//파일 처리(모드에 따라 읽기 또는 쓰기로 분기됨 (오버라이딩 대비)
	uint32_t GetStreamLength() const;	//파일 스트림 크기 반환
	std::vector<uint8_t> &GetFileStream();
	bool ChangeFileMode(FileMode _mode);
	void ChangeFileName(const std::string &_fn);
	void ChangeDirectory(const std::string &dirPath);
	void operator<<(std::vector<uint8_t> &srcStream);
	void operator<<(const std::string &srcStrStream);
	void SetRange(int offset, int amount);

	bool Exist()
	{
		struct stat buffer;
		return (stat(m_curFileName.c_str(), &buffer) == 0);
	}
	static bool Exist(const std::string &filename)
	{
		struct stat buffer;
		return (stat(filename.c_str(), &buffer) == 0);
	}
	std::string GetOnlyFilename();
	static std::string GetOnlyFilename(const std::string &filename);
    static std::string GetFilepath(const std::string &fullname);

protected:
	std::string CurrentFileName() const;
	void SavePreviousMode();
	void BackPreviousMode();
};

#endif