
#ifndef TOKENIZER_H__
#define TOKENIZER_H__

#include "preprocess\scriptProcess.h"
#include "optypes.h"
#include <memory>
#include <list>

class ScriptStream;
class ScriptString;
class TokenGroup;
class ScriptUnit;

class Tokenizer : public ScriptProcess
{
private:
	std::weak_ptr<ScriptStream> m_streamRef;
	std::weak_ptr<ScriptString> m_strManager;
	std::list<char> m_tokens;
	std::list<uint8_t> m_destTokens;
	uint32_t m_linenumber;
	std::shared_ptr<TokenGroup> m_rootbase;
	std::shared_ptr<TokenGroup> m_rootnode;

	std::string m_strToken;
	std::list<ScriptUnit*> m_scopeStack;

public:
	explicit Tokenizer(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<ScriptString> strManager);
	virtual ~Tokenizer() override;

private:
	int Readc();
	void Storec(int c);
	void Unreadc(int c);
	bool Next(int c);
	int Peek();

private:
	bool SkipBlockComment();
	void SkipLineComment();
	bool IsWhiteSpace(int c);
	bool DoSkipSpace();
	bool SkipSpace();
	bool Nextoct();
	int ReadOctalChar(int c);
	std::list<char> ReadEscapedChar();
	int ReadEscapedCharReal();
	std::shared_ptr<ScriptUnit> ReadString();
	std::shared_ptr<ScriptUnit> ReadChar();
	std::shared_ptr<ScriptUnit> ReadNumber(char c);

	std::shared_ptr<ScriptUnit> ReadIdent(char c);

	std::shared_ptr<ScriptUnit> MakeOperator(OpTypes id, char n = 0);
	std::shared_ptr<ScriptUnit> ReadRepeat(char expect, OpTypes id, OpTypes els);
	std::shared_ptr<ScriptUnit> ReadRepeatBinaryOper(char expect1, OpTypes id1, char expect2, OpTypes id2, char els);

	std::shared_ptr<ScriptUnit> MakeNewscope(char c);
	std::shared_ptr<ScriptUnit> MakeEndscope(char c);
	bool PushScopeStack(std::shared_ptr<ScriptUnit> scope);
	std::shared_ptr<ScriptUnit> MakeInvalid(const std::string &name);

private:
	virtual void Initialize() override;
	std::shared_ptr<ScriptUnit> DoReadToken();
	virtual bool Process() override;
	virtual void Deinitialize(bool pResult) override;

	virtual std::string ProcessName() override;
};

#endif