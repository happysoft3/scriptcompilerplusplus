
#include "scriptStream.h"
#include "scriptUnit.h"
#include "tokenGroup.h"
#include <algorithm>
#include <iterator>

ScriptStream::ScriptStream(const std::string &filename)
	: FileStream(filename)
{
	m_tokenRoot = std::make_shared<TokenGroup>();

	m_commonKey.emplace("return");
	m_commonKey.emplace("else");
	m_commonKey.emplace("break");
	m_commonKey.emplace("continue");
	m_commonKey.emplace("goto");

	//added 210622
	m_commonKey.emplace("for");
	m_commonKey.emplace("if");
	m_commonKey.emplace("else if");
	m_commonKey.emplace("while");
	//added end

	m_commonKey.emplace("static");

	m_filename = filename;

	m_modulePath = "lib\\";
	//AppendModuleName("noxmemlib");
}

ScriptStream::~ScriptStream()
{ }

void ScriptStream::SetRootToken(std::shared_ptr<TokenGroup> &&root)
{
	m_tokenRoot = std::forward<std::shared_ptr<TokenGroup>>(root);
}

bool ScriptStream::FindCommonkey(const std::string &key)
{
	auto keyIterator = m_commonKey.find(key);

	return keyIterator != m_commonKey.end();
}

std::string ScriptStream::ScriptFileName()
{
	uint32_t loff = m_filename.find_last_of('\\');
	uint32_t dotOff = m_filename.find_last_of('.');

	if (loff == std::string::npos)
		return m_filename;
	return m_filename.substr(loff + 1, dotOff - 1 - loff);
}

bool ScriptStream::AppendModuleName(const std::string &module)
{
    std::string moduleId = FileStream::GetOnlyFilename(module);
	auto keyIterator = m_moduleNameCheck.find(moduleId);

	if (moduleId == "builtins")	//�ӽ�ó��
		return false;

	if (keyIterator == m_moduleNameCheck.end())
	{
		m_moduleNameCheck.emplace(moduleId);
		m_moduleNameList.emplace_back(module);
		return true;
	}
	return false;
}

std::list<std::string> ScriptStream::GetModuleList() const
{
	if (m_moduleNameList.size())
	{
		return m_moduleNameList;
	}
	return{};
}

void ScriptStream::ClearModuleList()
{
    if (m_moduleNameList.size())
    {
        m_moduleNameList.clear();
        m_moduleNameCheck.clear();
    }
}

void ScriptStream::PreorderModule(std::function<void(PreModule &mod)> &&fn)
{
    for (auto &m : m_moduleObjList)
        fn(*m);
}

void ScriptStream::AppendModuleObject(const std::string &key, std::shared_ptr<PreModule> modobj)
{
    m_moduleObjList.push_back(modobj);
    m_moduleObjMap[key] = std::prev(m_moduleObjList.end());
}

std::shared_ptr<PreModule> ScriptStream::ReleaseModuleObject(const std::string &key)
{
    auto iter = m_moduleObjMap.find(key);

    if (iter == m_moduleObjMap.cend())
        return{ };

    auto ret = *iter->second;
    m_moduleObjList.erase(iter->second);
    m_moduleObjMap.erase(iter);
    return ret;
}

std::shared_ptr<PreModule> ScriptStream::PopModuleObject()
{
    if (m_moduleObjList.empty())
        return nullptr;

    auto ret = m_moduleObjList.front();

    m_moduleObjList.pop_front();
    return ret;
}

bool ScriptStream::PushExternDefine(const std::string &key, std::shared_ptr<ScriptUnit> value)
{
    auto keyIterator = m_externDefines.find(key);

    if (keyIterator != m_externDefines.end())
        return false;

    m_externDefines.emplace(key, value);
    return true;
}

void ScriptStream::RemoveExternDefine(const std::string &key)
{
    auto keyIterator = m_externDefines.find(key);

    if (keyIterator != m_externDefines.end())
        m_externDefines.erase(keyIterator);
}

void ScriptStream::ReportReadComplete(std::vector<uint8_t> *fstream)
{
	std::transform(fstream->begin(), fstream->end(), std::insert_iterator<std::string>(m_stream, m_stream.begin()), [](uint8_t chk) { return chk; });
}