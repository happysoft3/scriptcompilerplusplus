
#ifndef POST_SCR_DATA_H__
#define POST_SCR_DATA_H__

#include "utils\fileStream.h"

#include <string>
#include <list>
#include <memory>
#include <map>

class PostScrFunction;

class PostScrData : public FileStream
{
    struct PostStringData;
private:
    uint32_t m_strCounter;
    using post_str_list_ty = std::list<std::unique_ptr<PostStringData>>;
	post_str_list_ty m_scrStrings;
    std::map<uint32_t, post_str_list_ty::iterator> m_strMap;
    std::map<std::string, uint32_t> m_strIndexMap;
	std::multimap<std::string, std::shared_ptr<PostScrFunction>> m_funcMap;
	std::map<uint32_t, std::weak_ptr<PostScrFunction>> m_funcIndexMap;
	std::list<std::weak_ptr<PostScrFunction>> m_funcList;

	std::vector<uint8_t> m_outstream;
	uint32_t m_order;

	std::string m_libraryEntryFunctionName;

public:
	explicit PostScrData();
	virtual ~PostScrData() override;

	uint32_t AppendScrString(const std::string &str);
	//void AppendScrString(std::string &&str);
	void AppendScrFunction(const std::string &functionName);
	void AppendScrFunction(const std::shared_ptr<PostScrFunction> &function);
	std::shared_ptr<PostScrFunction> LastFunction();
	std::shared_ptr<PostScrFunction> FindFunction(const std::string &name);
	std::shared_ptr<PostScrFunction> FindFunctionWithIndex(uint32_t index);
	std::shared_ptr<PostScrFunction> GetGlobalFunction();
	std::shared_ptr<PostScrFunction> FindLibraryEntryFunction();

	void CheckingAllFunctions();
	void CommitOutlineSize(uint32_t commit);

    std::list<std::string> GetStringList() const;
    bool FindString(uint32_t index, std::string &found) const;
    bool FindStringIndex(const std::string &s, uint32_t &index) const;

	bool AppendExternFunctionCall(const std::string &functionName, const std::shared_ptr<PostScrFunction> &entryFunction);

	uint32_t GetScrStringCount() const { return m_scrStrings.size(); }
	uint32_t GetScrFunctionCount() const { return m_funcList.size(); }

	uint32_t GetFunctionOrder() const { return m_order; }

private:
	void InsertNumber(uint32_t number);
	void MakeScriptStringSection();
	void MakeFunctionSection();
	bool MakeOutput();
	virtual void ReportWriteReady(std::vector<uint8_t> *fstream);
};

#endif