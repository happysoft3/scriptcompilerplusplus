
#ifndef SCRIPT_TOKEN_H__
#define SCRIPT_TOKEN_H__

#include "scriptUnit.h"
#include "preprocess\optypes.h"

class ScriptToken : public ScriptUnit
{
public:
	enum TokenType
	{
		NONE,
		NUMBER,
		KEYWORD,
		OPERATOR,
		DEFINE,
		SCR_STRING,
		SCR_COMMIT,
		POINTER_COMMIT,
		SPACE_TOKEN,
		NEW_LINE,
		END_OF_FILE,
		END_SCOPE,
		INVALID
	};


private:
	uint32_t m_lineNumber;
	std::string m_token;
	TokenType m_type;

	OpTypes m_optype;

public:
	explicit ScriptToken();
	explicit ScriptToken(const std::string &name, uint32_t lineNumber, TokenType type);
	virtual ~ScriptToken() override;

	virtual uint32_t Linenumber() const override { return m_lineNumber; }
	void SetLinenumber(uint32_t lineNumber);
	char FirstChar() const { return (m_token.length() > 0) ? m_token[0] : 0; }
	void SetFirstChar(char c);
	std::string Name() const { return m_token; }
	void SetName(const std::string &name);
	TokenType Type() const { return m_type; }
	void SetType(TokenType type);

	virtual int UnitType() const override;
	virtual int UnitTokenType() const override;
	virtual std::string UnitName() const override { return Name(); }
	virtual void SetUnitName(const std::string &name) override { SetName(name); }	//added

	virtual uint32_t Length() const override { return 1; }
	virtual ScriptUnit *Clone() override;

	void SetTokenOpType(OpTypes type) { m_optype = type; }
	OpTypes GetTokenOpType() const { return m_optype; }
};

#endif