
#include "pathReader.h"

#include "utils\stringUtils.h"
#include <algorithm>
#include <regex>

PathReader::PathReader()
	: FileStream("systempath.txt")
{ }

PathReader::~PathReader()
{ }

void PathReader::MakePathMap()
{
	std::regex expr(R"(([\w]*)[\t=\s\n]*("[^"]*"))");
	std::map<std::string, std::string> &pathMapRef = m_pathMap;
	std::for_each(std::sregex_iterator(m_stream.begin(), m_stream.end(), expr), std::sregex_iterator(), [&pathMapRef](std::smatch mList)
	{
		pathMapRef.emplace(std::move(*(++mList.begin())), std::move(*(--mList.end())));
	});
}

void PathReader::ReportReadComplete(std::vector<uint8_t> *fstream)
{
	m_stream.resize(fstream->size());
	std::transform(fstream->begin(), fstream->end(), m_stream.begin(), [](char c) { return c; });

	MakePathMap();
}

std::string PathReader::ToAnsi(const std::string &utf)
{
	std::wstring wtmp;
	std::string dest;

	StringUtils::Utf8ToUnicode(utf, wtmp);
	StringUtils::UnicodeToAnsi(wtmp, dest);
	return dest;
}

std::string PathReader::FindPath(const std::string &key, bool to_ansi)
{
	auto keyIterator = m_pathMap.find(key);

	if (keyIterator != m_pathMap.end())
	{
		if (keyIterator->second.length() > 2)
		{
			if (to_ansi)
				return ToAnsi(keyIterator->second.substr(1, keyIterator->second.length() - 2));
			return keyIterator->second.substr(1, keyIterator->second.length() - 2);
		}
	}
	return{};
}
