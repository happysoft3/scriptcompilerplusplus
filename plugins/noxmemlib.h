

void EnableMemoryIO()
{
    int target = 0x979744;
    int exec[14];
    int arr[55];

    exec[0] = 0xBE515756; exec[1] = 0x0097977C; exec[2] = 0x750D80BF; exec[3] = 0x0037B900; exec[4] = 0xA5F30000; exec[5] = 0x5C33D8BE; exec[6] = 0xBC06C700;
	exec[7] = 0xC700750D; exec[8] = 0x0DD40446; exec[9] = 0x46C70075; exec[10] = 0x750DEC08; exec[11] = 0x0C46C700; exec[12] = 0x00750E44; exec[13] = 0xC35E5F59;
	
	arr[0] = 0x68515156; arr[1] = 0x00507250; arr[2] = 0x892414FF; arr[3] = 0xFF042444; arr[4] = 0xFF502414; arr[5] = 0x8B042454; arr[6] = 0x01C901C8; arr[7] = 0x04E1C1C1; arr[8] = 0xAE28358B; 
	arr[9] = 0x748B0075; arr[10] = 0x4C8B1C31; arr[11] = 0x8D580824; arr[12] = 0x048D0804; arr[13] = 0x59595986; arr[14] = 0x9090C35E; arr[15] = 0xFFFFBFE8; arr[16] = 0x723068FF; arr[17] = 0xFF500050; 
	arr[18] = 0x83042454; arr[19] = 0xC03108C4; arr[20] = 0x909090C3; arr[21] = 0x72506851; arr[22] = 0x14FF0050; arr[23] = 0x54FF5024; arr[24] = 0x89590424; arr[25] = 0xC0315808; arr[26] = 0x9090C359; 
	arr[27] = 0x50725068; arr[28] = 0x2414FF00; arr[29] = 0x6804C483; arr[30] = 0x00507230; arr[31] = 0x54FF30FF; arr[32] = 0xC4830424; arr[33] = 0xC3C03108; arr[34] = 0x68515156; arr[35] = 0x00507250; 
	arr[36] = 0x892414FF; arr[37] = 0xFF042444; arr[38] = 0xFF502414; arr[39] = 0x8B042454; arr[40] = 0x01C901C8; arr[41] = 0x04E1C1C1; arr[42] = 0xAE28358B; arr[43] = 0x748B0075; arr[44] = 0x8D581C31; 
	arr[45] = 0x008B8604; arr[46] = 0x04244C8B; arr[47] = 0x5988048D; arr[48] = 0xC35E5959; arr[49] = 0xFFFFBFE8; arr[50] = 0x723068FF; arr[51] = 0xFF500050; arr[52] = 0x83042454; arr[53] = 0xC03108C4; 
	arr[54] = 0x909090C3; 

    UnBlind();
	UOperatorInitNon4ByteHandler();
}

void SetMemory(int addr, int value)
{
    OperatorSetVar(addr, value);
}

int GetMemory(int addr)
{
    return OperatorGetMemory(addr);
}

int VOffsetToPtrchar(int functionId, int varId, int adder)
{
	return VOffsetToPtr(functionId, varId, 0) + adder;
}

int VOffsetToPtrshort(int functionId, int varId, int adder)
{
	return VOffsetToPtr(functionId, varId, 0) + (adder*2);
}

int VPointerOffsetToPtrchar(int functionId, int varId, int adder)
{
	return VPointerOffsetToPtr(functionId, varId, 0)+adder;
}

int VPointerOffsetToPtrshort(int functionId, int varId, int adder)
{
//	return OperatorGetWordMemory( VPointerOffsetToPtr(functionId, varId, 0)+(adder*2) );
	return VPointerOffsetToPtr(functionId, varId, 0)+(adder*2) ;
}

int UOperatorAfterMinus(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtr(functionId, varId, adder);
	int res = OperatorGetMemory(vptr);
	
	SetMemory(vptr, res - 1);
	return res;
}

int UOperatorAfterMinuschar(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtrchar(functionId, varId, adder);
	int res = OperatorGetByteMemory(vptr);
	
	OperatorSetByteVar(vptr, res-1);
	return res;
}

int UOperatorAfterMinusshort(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtrshort(functionId, varId, adder);
	int res = OperatorGetWordMemory(vptr);
	
	OperatorSetWordVar(vptr, res-1);
	return res;
}

int UOperatorAfterPlus(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtr(functionId, varId, adder);
	int res = OperatorGetMemory(vptr);
	
	SetMemory(vptr, res + 1);
	return res;
}

int UOperatorAfterPluschar(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtrchar(functionId, varId, adder);
	int res = OperatorGetByteMemory(vptr);
	
	OperatorSetByteVar(vptr, res + 1);
	return res;
}

int UOperatorAfterPlusshort(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtrshort(functionId, varId, adder);
	int res = OperatorGetWordMemory(vptr);
	
	OperatorSetWordVar(vptr, res + 1);
	return res;
}

int UOperatorBeforeMinus(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtr(functionId, varId, adder);
	int res = OperatorGetMemory(vptr) - 1;
	
	SetMemory(vptr, res);
	return res;
}

int UOperatorBeforeMinuschar(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtrchar(functionId, varId, adder);
	int res = OperatorGetByteMemory(vptr) - 1;
	
	OperatorSetByteVar(vptr, res);
	return res;
}

int UOperatorBeforeMinusshort(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtrshort(functionId, varId, adder);
	int res = OperatorGetWordMemory(vptr) - 1;
	
	OperatorSetWordVar(vptr, res);
	return res;
}

int UOperatorBeforePlus(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtr(functionId, varId, adder);
	int res = OperatorGetMemory(vptr) + 1;
	
	SetMemory(vptr, res);
	return res;
}

int UOperatorBeforePluschar(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtrchar(functionId, varId, adder);
	int res = OperatorGetByteMemory(vptr) + 1;
	
	OperatorSetByteVar(vptr, res);
	return res;
}

int UOperatorBeforePlusshort(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtrshort(functionId, varId, adder);
	int res = OperatorGetWordMemory(vptr) + 1;
	
	OperatorSetWordVar(vptr, res);
	return res;
}

int UOperatorAfterMinusPointer(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtr(functionId, varId, adder);
	int res = OperatorGetMemory(vptr);
	
	SetMemory(vptr, res - 1);
	return res;
}

int UOperatorAfterMinusPointerchar(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtrchar(functionId, varId, adder);
	int res = OperatorGetByteMemory(vptr);
	
	OperatorSetByteVar(vptr, res - 1);
	return res;
}

int UOperatorAfterMinusPointershort(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtrshort(functionId, varId, adder);
	int res = OperatorGetWordMemory(vptr);
	
	OperatorSetWordVar(vptr, res - 1);
	return res;
}

int UOperatorAfterPlusPointer(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtr(functionId, varId, adder);
	int res = OperatorGetMemory(vptr);
	
	SetMemory(vptr, res + 1);
	return res;
}

int UOperatorAfterPlusPointerchar(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtrchar(functionId, varId, adder);
	int res = OperatorGetByteMemory(vptr);
	
	OperatorSetByteVar(vptr, res + 1);
	return res;
}

int UOperatorAfterPlusPointershort(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtrshort(functionId, varId, adder);
	int res = OperatorGetWordMemory(vptr);
	
	OperatorSetWordVar(vptr, res + 1);
	return res;
}

int UOperatorBeforeMinusPointer(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtr(functionId, varId, adder);
	int res = OperatorGetMemory(vptr) - 1;
	
	SetMemory(vptr, res);
	return res;
}

int UOperatorBeforeMinusPointerchar(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtrchar(functionId, varId, adder);
	int res = OperatorGetByteMemory(vptr) - 1;
	
	OperatorSetByteVar(vptr, res);
	return res;
}

int UOperatorBeforeMinusPointershort(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtrshort(functionId, varId, adder);
	int res = OperatorGetWordMemory(vptr) - 1;
	
	OperatorSetWordVar(vptr, res);
	return res;
}

int UOperatorBeforePlusPointer(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtr(functionId, varId, adder);
	int res = OperatorGetMemory(vptr) + 1;
	
	SetMemory(vptr, res);
	return res;
}

int UOperatorBeforePlusPointerchar(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtrchar(functionId, varId, adder);
	int res = OperatorGetByteMemory(vptr) + 1;
	
	OperatorSetByteVar(vptr, res);
	return res;
}

int UOperatorBeforePlusPointershort(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtrshort(functionId, varId, adder);
	int res = OperatorGetWordMemory(vptr) + 1;
	
	OperatorSetWordVar(vptr, res);
	return res;
}

int UOperatorGetValueint(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtr(functionId, varId, adder);
	
	return GetMemory(vptr);
}

float UOperatorGetValuefloat(int functionId, int varId, int adder)
{
	StopScript(UOperatorGetValueint(functionId, varId, adder));
}

string UOperatorGetValuestring(int functionId, int varId, int adder)
{
	StopScript(UOperatorGetValueint(functionId, varId, adder));
}

object UOperatorGetValueobject(int functionId, int varId, int adder)
{
	StopScript(UOperatorGetValueint(functionId, varId, adder));
}

int UOperatorGetValuechar(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtrchar(functionId, varId, adder);
	
	return OperatorGetByteMemory(vptr);
}

int UOperatorGetValueshort(int functionId, int varId, int adder)
{
	int vptr = VOffsetToPtrshort(functionId, varId, adder);
	
	return OperatorGetWordMemory(vptr);
}

int UOperatorGetPointerValueint(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtr(functionId, varId, adder);
	
	return GetMemory(vptr);
}

float UOperatorGetPointerValuefloat(int functionId, int varId, int adder)
{
	StopScript(UOperatorGetPointerValueint(functionId, varId, adder));
}
string UOperatorGetPointerValuestring(int functionId, int varId, int adder)
{
	StopScript(UOperatorGetPointerValueint(functionId, varId, adder));
}
object UOperatorGetPointerValueobject(int functionId, int varId, int adder)
{
	StopScript(UOperatorGetPointerValueint(functionId, varId, adder));
}

int UOperatorGetPointerValuechar(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtrchar(functionId, varId, adder);
	
	return OperatorGetByteMemory(vptr);
}

int UOperatorGetPointerValueshort(int functionId, int varId, int adder)
{
	int vptr = VPointerOffsetToPtrshort(functionId, varId, adder);
	
	return OperatorGetWordMemory(vptr);
}

int UOperatorVOffsetToPtrint(int functionId, int varId, int adder)
{
	return VOffsetToPtr(functionId, varId, adder);
}

float UOperatorVOffsetToPtrfloat(int functionId, int varId, int adder)
{
	StopScript(VOffsetToPtr(functionId, varId, adder));
}

string UOperatorVOffsetToPtrstring(int functionId, int varId, int adder)
{
	StopScript(VOffsetToPtr(functionId, varId, adder));
}

object UOperatorVOffsetToPtrobject(int functionId, int varId, int adder)
{
	StopScript(VOffsetToPtr(functionId, varId, adder));
}

int UOperatorVOffsetToPtrchar(int functionId, int varId, int adder)	//Unused
{
	StopScript(VOffsetToPtrchar(functionId, varId, adder));
}

int UOperatorVOffsetToPtrshort(int functionId, int varId, int adder)		//Unused
{
	StopScript(VOffsetToPtrshort(functionId, varId, adder));
}

int UOperatorLoadFunction(int n)
{
	return n;
}

string UOperatorLoadString(string s)
{
	return s;
}

int UOperator_intArrayRef(int functionId, int varId, int adder, int multip)
{
	return VOffsetToPtr(functionId, varId, adder * multip);
}
float UOperator_floatArrayRef(int functionId, int varId, int adder, int multip)
{
	StopScript( UOperator_intArrayRef(functionId, varId, adder, multip) );
}
string UOperator_stringArrayRef(int functionId, int varId, int adder, int multip)
{
	StopScript( UOperator_intArrayRef(functionId, varId, adder, multip) );
}

int UOperator_intPtrArrayRef(int functionId, int varId, int adder, int multip)
{
	return UOperatorGetValueint(functionId, varId, 0) + (adder * multip);
}
int UOperator_floatPtrArrayRef(int functionId, int varId, int adder, int multip)
{
	StopScript(UOperator_intPtrArrayRef(functionId,varId,adder,multip));
}
int UOperator_stringPtrArrayRef(int functionId, int varId, int adder, int multip)
{
	StopScript(UOperator_intPtrArrayRef(functionId,varId,adder,multip));
}

void UOperatorInitNon4ByteHandler()
{
	int arr[29];
	
	arr[0] = 0x72506851; arr[1] = 0x14FF0050; arr[2] = 0x54FF5024; arr[3] = 0x88590424; arr[4] = 0xC0315808; arr[5] = 0x9090C359; arr[6] = 0x50725068; 
	arr[7] = 0x2414FF00; arr[8] = 0x6804C483; arr[9] = 0x00507230; arr[10] = 0x8AC93151; arr[11] = 0x54FF5108; arr[12] = 0x59590824; arr[13] = 0xC3C03158; 
	arr[14] = 0x72506851; arr[15] = 0x14FF0050; arr[16] = 0x54FF5024; arr[17] = 0x66590424; arr[18] = 0x31580889; arr[19] = 0x90C359C0; arr[20] = 0x50725068; 
	arr[21] = 0x2414FF00; arr[22] = 0x6804C483; arr[23] = 0x00507230; arr[24] = 0x66C93151; arr[25] = 0xFF51088B; arr[26] = 0x59082454; arr[27] = 0xC0315859; arr[28] = 0x909090C3;
	int i = 0, off=0x750e5c;
	
	for (i = 0 ; i < 29 ; i ++)
	{
		SetMemory(off + (i*4), arr[i]);
	}
	SetMemory(0x5c33e8, off);
	SetMemory(0x5c33e8+4, off+0x18);
	SetMemory(0x5c33e8+8, off+0x38);
	SetMemory(0x5c33e8+12, off+0x50);
}

void NOXLibraryEntryPointFunction()
{
    "export needinit EnableMemoryIO";
    "export SetMemory";
    "export GetMemory";
	"export VOffsetToPtrchar";
	"export VOffsetToPtrshort";
"export VPointerOffsetToPtrchar";
"export VPointerOffsetToPtrshort";
	"export UOperatorAfterMinus";
	"export UOperatorAfterMinuschar";
	"export UOperatorAfterMinusshort";
	"export UOperatorAfterPlus";
	"export UOperatorAfterPluschar";
	"export UOperatorAfterPlusshort";
	"export UOperatorBeforeMinus";
	"export UOperatorBeforeMinuschar";
	"export UOperatorBeforeMinusshort";
	"export UOperatorBeforePlus";
	"export UOperatorBeforePluschar";
	"export UOperatorBeforePlusshort";
	"export UOperatorAfterMinusPointer";
	"export UOperatorAfterMinusPointerchar";
	"export UOperatorAfterMinusPointershort";
	"export UOperatorAfterPlusPointer";
	"export UOperatorAfterPlusPointerchar";
	"export UOperatorAfterPlusPointershort";
	"export UOperatorBeforeMinusPointer";
	"export UOperatorBeforeMinusPointerchar";
	"export UOperatorBeforeMinusPointershort";
	"export UOperatorBeforePlusPointer";
	"export UOperatorBeforePlusPointerchar";
	"export UOperatorBeforePlusPointershort";
	"export UOperatorGetPointerValueint";
	"export UOperatorGetPointerValuefloat";
	"export UOperatorGetPointerValueobject";
	"export UOperatorGetPointerValueshort";
	"export UOperatorGetPointerValuechar";	
	"export UOperatorGetPointerValuestring";
	"export UOperatorGetValueint";
	"export UOperatorGetValuefloat";
	"export UOperatorGetValueobject";
	"export UOperatorGetValuestring";	
	"export UOperatorGetValuechar";
	"export UOperatorGetValueshort";
	"export UOperatorVOffsetToPtrint";
	"export UOperatorVOffsetToPtrfloat";
	"export UOperatorVOffsetToPtrstring";
	"export UOperatorVOffsetToPtrobject";
	//"export UOperatorVOffsetToPtrchar";
	//"export UOperatorVOffsetToPtrshort";
	"export UOperatorLoadFunction";
	"export UOperatorLoadString";
	"export UOperatorInitNon4ByteHandler";
	
    EnableMemoryIO();
}