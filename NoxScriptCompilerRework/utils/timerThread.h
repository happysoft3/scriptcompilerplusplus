
#ifndef TIMER_THREAD_H__
#define TIMER_THREAD_H__

#include <type_traits>
#include <functional>
#include <chrono>
#include <future>
#include <cstdio>
#include <thread>
#include <iostream>

class TimerThread
{
public:
	template <class callable, class... arguments>
	TimerThread(int after, bool async, callable&& f, arguments&&... args)
	{
		//std::function<typename std::result_of<callable(arguments...)>::type> task(std::bind(std::forward<callable>(f), std::forward<arguments>(args)...));
		//using timer_function_type = std::function<typename std::invoke_result<callable(arguments...)>::type(arguments...)>;

		auto task(std::bind(std::forward<callable>(f), std::forward<arguments>(args)...));

		if (async)
		{
			std::thread th([after, task]() {
				std::this_thread::sleep_for(std::chrono::milliseconds(after));
				task();
			});
			th.detach();
		}
		else
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(after));
			task();
		}
	}

};

#endif