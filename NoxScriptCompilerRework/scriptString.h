
#ifndef SCRIPT_STRING_H__
#define SCRIPT_STRING_H__

#include <string>
#include <map>

class ScriptString
{
private:	//문자열로 숫자찾기, 숫자로 문자열 찾기
	std::map<uint32_t, std::string> m_strMap;
	std::map<std::string, uint32_t> m_idMap;
	uint32_t m_strCount;
	const std::string m_strIdentifier;

public:
	explicit ScriptString();
	~ScriptString();

	uint32_t Append(const std::string &str);
	std::string FindString(uint32_t strId);
	std::string FindStringRaw(uint32_t strId);
	std::string FindStringWithKey(const std::string &keyId);
	std::string FindStringWithKeyRaw(const std::string &keyId);
	std::string StringSymbol() const { return m_strIdentifier; }

	void PrintAll();

	uint32_t Count() const { return m_strCount; }
};

#endif