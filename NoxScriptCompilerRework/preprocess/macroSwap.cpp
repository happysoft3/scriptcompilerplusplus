
#include "macroSwap.h"
#include "scriptStream.h"
#include "tokenGroup.h"
#include "scriptToken.h"
#include "utils\stringHelper.h"
#include <algorithm>
#include <iterator>

using namespace _StringHelper;

MacroSwap::MacroSwap(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<ScriptStream> mainstream)
	: ScriptProcess(), m_macroKey("#define")
{
	m_streamRef = streamRef;
    m_mainstreamRef = mainstream;
}

MacroSwap::~MacroSwap()
{ }

bool MacroSwap::MacroAppend(const std::string &macrokey, std::shared_ptr<ScriptUnit> macrovalue)
{
    std::shared_ptr<ScriptStream> mainstream = m_mainstreamRef.lock();

    if (mainstream)
        mainstream->PushExternDefine(macrokey, macrovalue);         //todo. exception catch

	auto macroIterator = m_macroMap.find(macrokey);

	if (macroIterator == m_macroMap.end())
	{
		m_macroMap.emplace(macrokey, macrovalue);
		return true;
	}
	return false;
}

void MacroSwap::MacroErase(const std::string &macroKey)
{
    std::shared_ptr<ScriptStream> mainstream = m_mainstreamRef.lock();

    if (mainstream)
        mainstream->RemoveExternDefine(macroKey);

    auto macroIterator = m_macroMap.find(macroKey);

    if (macroIterator != m_macroMap.cend())
        m_macroMap.erase(macroIterator);
}

bool MacroSwap::MacroAssign(std::shared_ptr<ScriptUnit> node)
{
	ScriptToken *scrToken = dynamic_cast<ScriptToken *>(node.get());
	auto macroIterator = m_macroMap.find(scrToken->Name());

	if (macroIterator != m_macroMap.end())
	{
		std::shared_ptr<ScriptUnit> scrUnit = macroIterator->second.lock();
		ScriptToken *value = dynamic_cast<ScriptToken *>(scrUnit.get());

		scrToken->SetName(value->Name());
		scrToken->SetType(value->Type());
		return true;
	}
	return false;
}

bool MacroSwap::MacroSS(std::list<std::shared_ptr<ScriptUnit>> &l)
{
	l.front()->SetEliminate();
	l.pop_front();	//�����ζ�

	if (l.front()->UnitTokenType() != ScriptToken::KEYWORD)
		return false;
	auto sss = l.front();
	std::string key = l.front()->UnitName();	//���ڿ�
	l.front()->SetEliminate();
	l.pop_front();
	bool minus = false;
	if (l.front()->UnitName() == "-")	//������
	{
		l.front()->SetEliminate();
		l.pop_front();
		minus = true;
	}
	if (l.front()->UnitTokenType() == ScriptToken::NUMBER)	//���ڿ�
	{
		if (minus)
			l.front()->SetUnitName('-' + l.front()->UnitName());
	}
	else if (l.front()->UnitTokenType() != ScriptToken::SCR_STRING)	//�����̰ų�
		return false;
	if (!MacroAppend(key, l.front()))
	{
		m_errorList.emplace_back("this macro key is already exist", sss);
		return false;
	}
	l.front()->SetEliminate();
	return true;
}

bool MacroSwap::macroUndef(std::list<std::shared_ptr<ScriptUnit>> &l)
{
    l.front()->SetEliminate();
    l.pop_front();	//�����ζ�

    if (l.front()->UnitTokenType() != ScriptToken::KEYWORD)
    {
        m_errorList.emplace_back(stringFormat("expect keyword but got %s", l.front()->UnitName()), l.front());
        return false;
    }
    std::string key = l.front()->UnitName();
    l.front()->SetEliminate();
    l.pop_front();
    MacroErase(key);
    /*if (l.front()->UnitTokenType() != ScriptToken::NEW_LINE)
    {
        m_errorList.emplace_back(stringFormat("expect newline but got %s", l.front()->UnitName()), l.front());
        return false;
    }*/
    return true;
}

bool MacroSwap::Substitution(std::shared_ptr<ScriptUnit> currentGroup)
{
	std::list<std::shared_ptr<ScriptUnit>> tokens = dynamic_cast<TokenGroup *>(currentGroup.get())->GetList();
	std::list<std::shared_ptr<ScriptUnit>> macroStack;

	while (tokens.size() > 0)
	{
		if (tokens.front()->UnitType() == ScriptUnit::GROUP)
			Substitution(tokens.front());
		else
		{
			ScriptToken *token = dynamic_cast<ScriptToken *>(tokens.front().get());

			switch (token->Type())
			{
			case ScriptToken::DEFINE:
				if (m_macroKey == token->Name())	//'#define'
				{
					if (m_rootGroup.lock() == currentGroup)
					{
						if (!MacroSS(tokens))
							m_errorList.emplace_back("fail to match", tokens.front());
							//return false;
						else
							continue;
					}
					else
					{
						m_errorList.emplace_back("macro can not declare in local scope", tokens.front());
					}
				}
                else if ("#undef" == token->Name())
                {
                    if (!macroUndef(tokens))
                        return false;
                    continue;
                }
				break;
			case ScriptToken::KEYWORD:
				if (MacroAssign(tokens.front()))
					break;
			case ScriptToken::OPERATOR:
			case ScriptToken::NUMBER:
			case ScriptToken::SCR_STRING:
				break;
			}
		}
		tokens.pop_front();
	}
	return m_errorList.empty();
}

void MacroSwap::Initialize()
{
	std::shared_ptr<ScriptStream> streamRef = m_streamRef.lock();

	m_rootGroup = streamRef->GetTokenRoot();

	m_pointerKey = std::shared_ptr<ScriptUnit>(new ScriptToken(std::string(1, '*'), 0, ScriptToken::OPERATOR));
	MacroAppend("__pointer", m_pointerKey);

    auto prevmap = streamRef->GetExternDefine();

    for (const auto &item : prevmap)
        MacroAppend(item.first, item.second);
}

bool MacroSwap::Process()
{
	bool pResult = Substitution(m_rootGroup.lock());

	PrintErrorList();

	return pResult;
}

void MacroSwap::Deinitialize(bool pResult)
{
	if (pResult)
	{
		std::shared_ptr<TokenGroup> root(m_rootGroup.lock());

		root->Refresh();
	}
}

void MacroSwap::PrintErrorList()
{
	while (m_errorList.size() != 0)
	{
		std::string errorMessage = std::get<0>(m_errorList.front());
		std::shared_ptr<ScriptUnit> scrUnit = std::get<1>(m_errorList.front()).lock();

		if (scrUnit->UnitType() == ScriptUnit::SINGLE)
		{
			ScriptToken *scrToken = dynamic_cast<ScriptToken *>(scrUnit.get());

			ErrorReport("error::", scrToken->Name(), " :: ", errorMessage, "::line:", scrToken->Linenumber());
		}
		m_errorList.pop_front();
	}
}