
#include "postModule.h"
#include "postStream.h"
#include "postScrData.h"
#include "postScrFunction.h"
#include "preprocess/premodule.h"

#include "utils/stringHelper.h"

using namespace _StringHelper;

PostModule::PostModule(const std::string &fullname)
{
	m_postData = std::make_shared<PostScrData>();
	m_fullname = fullname;
}

PostModule::~PostModule()
{ }

void PostModule::findExternSymbol(PreModule &mod)
{
    static constexpr char needinit_key[] = "export needinit";
    m_exportList = mod.GetExternList();
    std::list<std::string> scrStrings = m_postData->GetStringList();

    for (const auto &s : scrStrings)
    {
        if (s.find("export needinit") != std::string::npos)
            m_needInit.push_back(s.substr(sizeof(needinit_key)));
    }
}

bool PostModule::Load(std::shared_ptr<FunctionPool> preprocFnMap, PreModule &mod)
{
	m_postStream = std::make_unique<PostStream>(m_postData, m_fullname);
    m_preprocFnMap = preprocFnMap;

	while (m_postStream->FileProcess())
	{
		if (m_postStream->Completed())
		{
			//if (!FindExportSymbol())
			//	return false;
            findExternSymbol(mod);

			std::shared_ptr<PostScrFunction> libEntryFunction(m_postData->FindLibraryEntryFunction());
			if (libEntryFunction)
				libEntryFunction->CheckGlobal();
			//else
			//	return false; //없어도 오류가 아님// 2023-09-26-16-13

			//for (const auto &name : m_exportList)	//print export functions
			//	std::cout << "external function:: " << name << std::endl;
			return true;
		}
		break;
	}
	PrintError();
	return false;
}

bool PostModule::Insert(std::shared_ptr<PostScrData> mainData, const std::function<uint32_t(const std::string&)> &findExternFunc)
{
	std::shared_ptr<PostScrFunction> moduleFunction, mainFunction;

	for (const std::string &functionName : m_exportList)
	{
		moduleFunction = m_postData->FindFunction(functionName);

        if (!moduleFunction)		//모듈파일에서 해당 함수 없으면 오류
        {
            std::cout << stringFormat("error::%s::%s missing", __FUNCSIG__, functionName) << std::endl;
            return false;
        }
		mainFunction = mainData->FindFunction(functionName);
		
        moduleFunction->SetRealFnId(m_preprocFnMap);
		if (!moduleFunction->FixCallLink(mainData))	//링킹처리
		{
			std::cout << m_fullname << " module " << functionName << " function linking error\n";
			return false;
		}
		/*else
			std::cout << "linking " << functionName << " function... ok\n";*/
		if (mainFunction)	//메인코드에 함수가 있음, 
		{
			if (!mainFunction->Replaced())	//이미 교체된 함수가 아니어야 한다
			{
				uint32_t flags = findExternFunc(functionName);

				if (flags & 2)	//override: 메인소스에서 재정의 했으므로 투입 무시
					mainFunction->ReplaceComplete();
				else
					mainFunction->ReplaceContent(moduleFunction);	//일치하면 교체 수행	//추가 2021.7.5 19:57
				//if (findExternFunc(functionName))	//메인코드가 해당 함수를 외부의 것으로 정의했는가?
				//	mainFunction->ReplaceContent(moduleFunction);	//일치하면 교체 수행
				//else
				//{
				//	std::cout << m_fullname << " module " << functionName << " function name conflicted with main source\n";
				//	return false;
				//}
			}
		}
		else	//메인소스에 함수 없으면 강제투입
			mainData->AppendScrFunction(moduleFunction);
	}
	return true;
}

bool PostModule::FindExportSymbol()
{
	std::list<std::string> scrStrings = m_postData->GetStringList();
	std::string exportKey = "export ", check, functionName;
	std::string needInit = "needinit ";

	functionName.reserve(64);
	scrStrings.pop_front();
	while (scrStrings.size())
	{
		if (std::equal(exportKey.begin(), exportKey.end(), scrStrings.front().begin()))
		{
			check = scrStrings.front().substr(exportKey.length());

			if (std::equal(needInit.begin(), needInit.end(), check.begin()))	//needinit keyword
			{
				functionName = check.substr(needInit.length());
				m_needInit.emplace_back(functionName);
			}
			else
				functionName = check;

			if (functionName.length())
			{
				if (m_postData->FindFunction(functionName))
					m_exportList.emplace_back(functionName);
				else
					std::cout << "Warning:: " << functionName << " can not find\n";
			}
			else
				return false;
		}
		//else
		//	return false;
		scrStrings.pop_front();
	}
	return m_exportList.size() > 0;
}

void PostModule::PrintError()
{
	std::cout << "module error::" << m_fullname << std::endl;
}