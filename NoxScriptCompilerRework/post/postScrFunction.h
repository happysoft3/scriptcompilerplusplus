
#ifndef POST_SCR_FUNCTION_H__
#define POST_SCR_FUNCTION_H__

#include <string>
#include <list>
#include <vector>
#include <memory>
#include <map>
#include <functional>

class PostScrData;
class FunctionPool;

class PostScrFunction
{
private:
	PostScrData *m_parentNode;
	std::string m_name;
	uint32_t m_retType;
	uint32_t m_argCount;
	std::list<uint32_t> m_symbList;
	std::list<uint32_t> m_codeList;

	uint32_t m_lastOp;
	bool m_replaced;

	uint32_t m_index;

	std::list<uint32_t> m_globalTempStore;
	std::map<uint32_t, uint32_t> m_globalData;

	std::weak_ptr<PostScrData> m_mainData;

    uint32_t m_realFnId;

public:
	explicit PostScrFunction(PostScrData *parentNode);
	~PostScrFunction();

	std::string Name() const { return m_name; }
	uint32_t ReturnType() const { return m_retType; }
	uint32_t ArgumentCount() const { return m_argCount; }
	std::list<uint32_t> SymbolList() const { return m_symbList; }
	void SetMetaData(const std::string &name, uint32_t retType, uint32_t argCount);
	void SetSymbolList(std::list<uint32_t> &&symbols);
	void SetCodedataList(std::list<uint32_t> &&data);
	void ExtractDataList(std::list<uint32_t> &destDatalist);
	bool ReplaceContent(std::shared_ptr<PostScrFunction> replaceFunction);

	void SetFunctionIndex(uint32_t index) { m_index = index; }
	uint32_t FunctionIndex() const { return m_index; }

	void AppendCodeFront(const std::vector<uint32_t> &codes);

private:
	uint32_t ReadOpcodeIndex(uint32_t opIndex);
	bool OpcodeHandler(std::vector<uint32_t> &opcodes);
	bool OpcodeGlobalProc(std::vector<uint32_t> &opcodes);
	bool OpcodeFixLink(std::vector<uint32_t> &opcodes);

public:
	using dataCodeFunctionType = std::function<bool(std::vector<uint32_t>&)>;
	bool DataCodeAnalysis(dataCodeFunctionType proc);

public:
	void CheckOpcode();
	void CheckGlobal();

private:
    bool FixThisLink();

public:
	bool FixCallLink(std::shared_ptr<PostScrData> mainData);

private:
	void AppendGlobalData();
    void fixLoadFunctionId(uint32_t index);
    bool fixLoadStringId(uint32_t index);
    bool tryLoadFunctionPtr(const std::string &id, uint32_t invokeTarget);
	bool FixLinkOpcode(uint32_t index, uint32_t opcode, uint32_t adder = 0);

public:
	bool Replaced() const { return m_replaced; }
	void ReplaceComplete() { m_replaced = true; }
	uint32_t CodeSectionLength() const;

public:
	bool FixCodeSectionJumpOffset(uint32_t prevLength);
    void SetRealFnId(std::shared_ptr<FunctionPool> preprocFnMap);
};

#endif