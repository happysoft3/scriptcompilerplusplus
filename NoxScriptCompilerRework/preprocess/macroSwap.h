
#ifndef MACRO_SWAP_H__
#define MACRO_SWAP_H__

#include "scriptProcess.h"

#include <list>
#include <memory>
#include <map>

class ScriptStream;
class ScriptToken;
class TokenGroup;
class ScriptUnit;

//@brief. 전처리기 치환하는 노드
class MacroSwap : public ScriptProcess
{
private:
	std::map<std::string, std::weak_ptr<ScriptUnit>> m_macroMap;
	std::weak_ptr<ScriptStream> m_streamRef;
    std::weak_ptr<ScriptStream> m_mainstreamRef;

	const std::string m_macroKey;

	std::weak_ptr<TokenGroup> m_rootGroup;
	
	using macroErrorTypes = std::tuple<std::string, std::weak_ptr<ScriptUnit>>;
	std::list<macroErrorTypes> m_errorList;

	std::shared_ptr<ScriptUnit> m_pointerKey;

public:
	explicit MacroSwap(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<ScriptStream> mainstream = nullptr);
	virtual ~MacroSwap() override;

private:
	bool MacroAppend(const std::string &macrokey, std::shared_ptr<ScriptUnit> macrovalue);
    void MacroErase(const std::string &macroKey);
	bool MacroAssign(std::shared_ptr<ScriptUnit> node);
	bool MacroSS(std::list<std::shared_ptr<ScriptUnit>> &l);
    bool macroUndef(std::list<std::shared_ptr<ScriptUnit>> &l);
	bool Substitution(std::shared_ptr<ScriptUnit> currentGroup);
	virtual void Initialize() override;
	virtual bool Process() override;
	virtual void Deinitialize(bool pResult) override;
	void PrintErrorList();
};

#endif