
#include "postScrFunction.h"
#include "postScrData.h"
#include "functionPool.h"
#include "scriptFunction.h"

#include "preprocess\preprocess_generic.h"
#include "global/builtinsDef.h"
#include <iostream>
#include <iterator>
#include <algorithm>

PostScrFunction::PostScrFunction(PostScrData *parentNode)
{
	m_parentNode = parentNode;
	m_replaced = false;
	m_index = 0;
    m_realFnId = 0;
}

PostScrFunction::~PostScrFunction()
{ }

void PostScrFunction::SetMetaData(const std::string &name, uint32_t retType, uint32_t argCount)
{
	m_name = name;
	m_retType = retType;
	m_argCount = argCount;
}

void PostScrFunction::SetSymbolList(std::list<uint32_t> &&symbols)
{
	m_symbList = std::forward<std::list<uint32_t>>(symbols);
}

void PostScrFunction::SetCodedataList(std::list<uint32_t> &&data)
{
	m_codeList = std::forward<std::list<uint32_t>>(data);
}

void PostScrFunction::ExtractDataList(std::list<uint32_t> &destDatalist)
{
	if (m_codeList.size())
	{
		destDatalist = m_codeList;
	}
}

bool PostScrFunction::ReplaceContent(std::shared_ptr<PostScrFunction> replaceFunction)
{
	if (!replaceFunction)
		return false;
	if (m_replaced)
		return false;
	//if (m_codeList.size() > 1)	//it doesn't work the function has atleast 2 code field
	//{
	//	m_replaced = true;
	//	return false;
	//}

	if (replaceFunction->m_name != m_name)
		m_name = replaceFunction->m_name;
	m_retType = replaceFunction->m_retType;
	m_argCount = replaceFunction->m_argCount;
	m_symbList = replaceFunction->m_symbList;
	m_codeList = replaceFunction->m_codeList;
	m_replaced = true;

	return true;
}

void PostScrFunction::AppendCodeFront(const std::vector<uint32_t> &codes)
{
	std::vector<uint32_t>::const_reverse_iterator rb = codes.rbegin();

	while (rb != codes.rend())
	{
		m_codeList.push_front(*rb);
		++rb;
	}
}

uint32_t PostScrFunction::ReadOpcodeIndex(uint32_t opIndex)
{
	switch (opIndex)
	{
	case 0: case 1: case 2: case 3:
		return 2;
	case 4: case 5: case 6:
	case 19: case 20: case 21:	//jump
	case 69: //builtin call
	case 70: //user define call
		return 1;
	}
	return 0;
}

bool PostScrFunction::OpcodeHandler(std::vector<uint32_t> &opcodes)
{
	return true;
}

bool PostScrFunction::OpcodeGlobalProc(std::vector<uint32_t> &opcodes)
{
	switch (m_lastOp)
	{
	case 2:
	case 4:
		if (opcodes.size())
			m_globalTempStore.push_back(opcodes.back());
		break;
	case 0x16:
		AppendGlobalData();
	default:
		if (m_globalTempStore.size())
			m_globalTempStore.clear();
	}
	return true;
}

bool PostScrFunction::OpcodeFixLink(std::vector<uint32_t> &opcodes)
{
    if (m_parentNode == nullptr)
        return false;

	const uint32_t globalScope = 1;
	uint32_t index = (opcodes.size() > 0) ? opcodes.back() : 0;
	decltype(m_globalData.begin()) valueIterator;

	switch (m_lastOp)
	{
	case 0: //0 1 varIndex	-> 4 var_index
		if (opcodes.front() == globalScope)
		{
            auto entryFunction = m_parentNode->FindLibraryEntryFunction();
            //if (!entryFunction)   //2023-09-26-1612 없어도 오류가 아님//
            //    return false;

			valueIterator = entryFunction->m_globalData.find(index);			//added

			if (valueIterator == entryFunction->m_globalData.end())
			{
				break;
			}
			
			if (!FixLinkOpcode(valueIterator->second, 4, 0x40))
				return false;
			opcodes.clear();
		}
		break;

    //case 4: //load function, load string
    //    switch (m_linkMode)
    //    {
    //    case LinkMode::LOAD_FUNCTION:
    //        fixLoadFunctionId(index);
    //        opcodes.clear();
    //        break;

    //    //case LinkMode::LOAD_STRING:
    //    //    if (!fixLoadStringId(index))
    //    //        return false;
    //    }
    //    break;

    case 6:
        if (!fixLoadStringId(index))
            return false;
        opcodes.clear();
        break;

	case 0x46:	//call user defined function
        if (!FixLinkOpcode(index, m_lastOp))
            return false;

		opcodes.clear();
		break;
	}
	return true;
}

bool PostScrFunction::DataCodeAnalysis(PostScrFunction::dataCodeFunctionType proc)
{
	//code 단락 따라읽기
	uint32_t codeLength = m_codeList.size();
	uint32_t oplength = 0;
	std::vector<uint32_t> opcode;

	opcode.reserve(16);
	while (codeLength--)
	{
		if (oplength)
		{
			opcode.push_back(m_codeList.front());
			oplength--;
			if (oplength == 0)
			{
                if (!proc(opcode))
                    return false;

				if (opcode.size())
				{
					m_codeList.push_back(m_lastOp);
					std::copy(opcode.begin(), opcode.end(), std::insert_iterator<std::list<uint32_t>>(m_codeList, m_codeList.end()));
					opcode.clear();
				}
			}
		}
		else
		{
			m_lastOp = m_codeList.front();
			oplength = ReadOpcodeIndex(m_codeList.front());
			if (!oplength)
			{
                if (!proc(opcode))
                    return false;

				m_codeList.push_back(m_lastOp);
			}
		}
		m_codeList.pop_front();
	}
	return opcode.size() == 0;
}

void PostScrFunction::CheckOpcode()	//todo. ret 타입 bool 으로 바꾸어서 false 일 경우 처리 중단 하기
{
	if (!DataCodeAnalysis([this](std::vector<uint32_t> &codes) { return this->OpcodeHandler(codes); }))
		return;
	if (m_lastOp != 0x48)
	{
		m_codeList.push_back(0x48);
		std::cout << m_name << " function fix return\n";
	}
}

void PostScrFunction::CheckGlobal()
{
	if (!DataCodeAnalysis([this](std::vector<uint32_t> &codes) { return this->OpcodeGlobalProc(codes); }))
		return;
}

bool PostScrFunction::FixThisLink()
{
    if (m_codeList.size() < 8)
        return false;

    std::vector<std::list<uint32_t>::iterator> opIteratorVec(8);
    std::list<uint32_t>::iterator srcIterator = m_codeList.begin();

    for (auto &dest : opIteratorVec)
        dest = srcIterator++;

    if (*opIteratorVec[0] == 4 &&
        *opIteratorVec[1] == s_uniq_ident_num &&
        *opIteratorVec[2] == 2 && //opcode ref local var
        *opIteratorVec[5] == 4 && //opcdoe push int
        *opIteratorVec[7] == 0x16) //opcode assignment value
    {
        *opIteratorVec[6] = m_realFnId;
        //std::cout << m_name << std::endl;
        return true;
    }
    return false;
}

bool PostScrFunction::FixCallLink(std::shared_ptr<PostScrData> mainData)
{
	m_mainData = mainData;

    FixThisLink();  //added
	return DataCodeAnalysis([this](std::vector<uint32_t> &codes) { return this->OpcodeFixLink(codes); });
}

void PostScrFunction::AppendGlobalData()
{
	if (m_globalTempStore.size() > 1)
	{
		uint32_t value = m_globalTempStore.back();

		m_globalTempStore.pop_back();
		uint32_t vindex = m_globalTempStore.back();

		if (m_globalData.find(vindex) == m_globalData.end())
		{
			m_globalData.emplace(vindex, value);
		}
	}
}

void PostScrFunction::fixLoadFunctionId(uint32_t index)
{
    std::shared_ptr<PostScrFunction> moduleFunction = m_parentNode->FindFunctionWithIndex(index);
    auto mainData = m_mainData.lock();

    auto mainFunc = mainData->FindFunction(moduleFunction->Name());
    m_codeList.push_back(mainFunc->FunctionIndex());

    m_codeList.push_back(0x46);
    auto loadFunc = mainData->FindFunction(BUILTINS_LOAD_USER_FUNCTION);

    m_codeList.push_back(loadFunc->FunctionIndex());
}

bool PostScrFunction::fixLoadStringId(uint32_t index)
{
    std::string sId;

    if (!m_parentNode->FindString(index, sId))
    {
        std::cout << "error::" << __FUNCSIG__ << std::endl;
        return false;
    }

    auto mainData = m_mainData.lock();

    if (!mainData->FindStringIndex(sId, index))
        index = mainData->AppendScrString(sId);
    m_codeList.push_back(6);
    m_codeList.push_back(index);
    //m_codeList.push_back(0x46);
    //m_codeList.push_back(m_linkInvoke);
    return true;
}

bool PostScrFunction::tryLoadFunctionPtr(const std::string &id, uint32_t invokeTarget)
{
    if (id == BUILTINS_LOAD_USER_FUNCTION)
    {
        uint32_t index = m_codeList.back();

        m_codeList.pop_back();
        fixLoadFunctionId(index);
        return true;
    }
    return false;
}

bool PostScrFunction::FixLinkOpcode(uint32_t index, uint32_t opcode, uint32_t adder)
{
	if (m_parentNode == nullptr)
		return false;

	std::shared_ptr<PostScrFunction> moduleFunction = m_parentNode->FindFunctionWithIndex(index);

    if (tryLoadFunctionPtr(moduleFunction->Name(), index))
    {
        return true;
    }

	std::shared_ptr<PostScrData> mainData(m_mainData.lock());

	if (moduleFunction && mainData)
	{
		std::shared_ptr<PostScrFunction> mainFunction = mainData->FindFunction(moduleFunction->Name());

		if (mainFunction)
		{
			m_codeList.push_back(opcode);
			m_codeList.push_back(mainFunction->FunctionIndex());
			if (adder != 0)
				m_codeList.push_back(adder);
			return true;
		}
		else	//main 소스에 함수가 없는 경우-- 현재 함수여야 한다!, 아니면 오류임
		{
			if (moduleFunction->Name() == m_name)
			{
				m_codeList.push_back(opcode);
				m_codeList.push_back(mainData->GetFunctionOrder());
				if (adder != 0)
					m_codeList.push_back(adder);
				return true;
			}
			else
				std::cout << "link error::" << m_name << std::endl;
		}
	}
	return false;
}

uint32_t PostScrFunction::CodeSectionLength() const
{
	if (m_codeList.size() == 0)
		return 0;

	return m_codeList.size();
}

bool PostScrFunction::FixCodeSectionJumpOffset(uint32_t prevLength)
{
	uint32_t currentLength = m_codeList.size();
	
	if (prevLength < currentLength)
	{
		uint32_t basePlus = currentLength - prevLength;
		auto codeIterator = m_codeList.begin();
		uint32_t opcode = -1, length = 0;
		std::list<decltype(codeIterator)> tempList;

		while (codeIterator != m_codeList.end())
		{
			if (opcode == -1)
			{
				opcode = *codeIterator;
				++codeIterator;
				continue;
			}
			else
			{
				length = ReadOpcodeIndex(opcode);

				while (length > 0)
				{
					if (codeIterator != m_codeList.end())
					{
						tempList.push_back(codeIterator);
						--length;
					}
					else
						return false;
					++codeIterator;
				}
				switch (opcode)
				{
				case 19: case 20: case 21:
					*tempList.front() += basePlus;
				default:
					break;
				}
				tempList.clear();
				opcode = -1;
			}
		}
	}
	return true;
}

void PostScrFunction::SetRealFnId(std::shared_ptr<FunctionPool> preprocFnPool)
{
    auto preprocFn = preprocFnPool->GetFunctionNode(m_name);

    if (!preprocFn)
        return;

    m_realFnId = preprocFn->FunctionNumber();
}

