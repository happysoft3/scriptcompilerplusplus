
#include "builtinsLoader.h"
#include "builtins.h"
#include "functionPool.h"
#include <iostream>

BuiltinsLoader::BuiltinsLoader(std::shared_ptr<FunctionPool> functionPool)
{
	m_functionPool = functionPool;
}

BuiltinsLoader::~BuiltinsLoader()
{ }

void BuiltinsLoader::SetBuiltinsPath(const std::string &path)
{
	if (!m_builtins)
	{
		m_builtins = std::make_unique<Builtins>(m_functionPool.lock(), path);
	}
}

bool BuiltinsLoader::Load()
{
	if (m_builtins)
		return m_builtins->Fetch();
	else
	{
		std::cout << "builtins not loaded\n";
		return false;
	}
}