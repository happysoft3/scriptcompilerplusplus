
#ifndef SCRIPT_TYPES_H__
#define SCRIPT_TYPES_H__

#include <map>
#include <string>
#include <vector>

class ScriptTypes
{
public:
	enum ScrType
	{
		SCR_VOID,
		SCR_INT,
		SCR_STRING,
		SCR_OBJECT,
		SCR_FLOAT,
        SCR_CHAR,
        SCR_SHORT,
		SCR_INVALID = 0xff
	};

private:
	std::map<std::string, uint32_t> m_typeMap;
	std::vector<std::string> m_typeV;
    std::map<int, std::string> m_realTypeMap;

public:
	explicit ScriptTypes();
	~ScriptTypes();
	bool IsValid(const std::string &typeKey);
	uint32_t GetTypeId(const std::string &typeKey);
	std::string GetTypeName(uint32_t typeNumber) const;
    std::string GetRealTypeName(uint32_t typeId) const;
    std::string GetUnDwordTypeName(uint32_t typeId) const;
    uint32_t TypeSize(uint32_t typeId) const;
};

#endif