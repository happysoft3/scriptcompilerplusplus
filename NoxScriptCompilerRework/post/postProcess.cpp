
#include "postProcess.h"
#include "postModule.h"
#include "postStream.h"
#include "postScrData.h"

#include "postScrFunction.h"
#include "utils/stringHelper.h"
#include "utils/incFilesystem.h"

using namespace _StringHelper;

PostProcess::PostProcess(const std::string &filename)
{
	m_postData = std::make_shared<PostScrData>();
	m_filename = filename;
}

PostProcess::~PostProcess()
{ }

bool PostProcess::SetMapPath(const std::string &mapname, const std::string &noxpath, const std::string &alt)
{
	m_mapname = mapname;
    auto url = stringFormat("%s/maps/%s", noxpath, mapname);

    std::cout << url << std::endl;
    if (!NAMESPACE_FILESYSTEM::exists(url))
    {
        //m_noxpath = alt;
        m_altpath = alt;
        return false;
    }
	m_noxpath = noxpath;
    return true;
}

bool PostProcess::Process()
{
	//if (!CheckMapDirectory())
	//	return false;

	m_postStream = std::make_unique<PostStream>(m_postData, m_filename);
	while (m_postStream->FileProcess())
	{
		if (!m_postStream->Completed())
		{
			std::cout << m_filename << "::binary read fail\n";
			break;
		}
		m_postData->CheckingAllFunctions();
		if (!InsertModule())
			return false;
		std::cout << "linking... ok\n";
		if (!InsertCallModuleCode())
			return false;
		m_postData->ChangeFileName(GetOutputObjectFileName());
		m_postData->CommitOutlineSize(m_postStream->GetStreamLength());
        std::cout << GetOutputObjectFileName() << std::endl;
		if (m_postData->FileProcess() != 0)
		{
			return true;
		}
        std::cout << "postprocess::process()" << std::endl;
		break;
	}
	return false;
}

bool PostProcess::AppendModule(const std::string &modulePath, std::shared_ptr<PreModule> mod, std::shared_ptr<FunctionPool> preprocFnMap)
{
	std::unique_ptr<PostModule> module(new PostModule(modulePath + ".obj"));

	if (module->Load(preprocFnMap, *mod))
	{
		m_modules.emplace_back(std::move(module));
		return true;
	}
	else
		std::cout << modulePath << "fail to load\n";
	return false;
}

std::list<std::string> PostProcess::ReportResult()
{
	std::list<std::string> reportList;
	uint32_t scrstrCount = m_postData->GetScrStringCount();
	uint32_t scrFuncCount = m_postData->GetScrFunctionCount();

	reportList.emplace_back("report::");
	reportList.emplace_back(std::to_string(scrstrCount));
	reportList.emplace_back(" strings, ");
	reportList.emplace_back(std::to_string(scrFuncCount));
	reportList.emplace_back(" functions\n");
	return reportList;
}

void PostProcess::RegistSpecialFunctions(std::map<std::string, uint32_t> &&specials)
{
	m_specialFuncs = std::forward<std::map<std::string, uint32_t>>(specials);
}

bool PostProcess::InsertCallModuleCode()
{
	auto const entryFunction = m_postData->FindFunction("MapEntry");
	uint32_t prevEntryLength = 0;

	if (entryFunction)
		prevEntryLength = entryFunction->CodeSectionLength();

	for (const auto &func : m_extCallFunctions)
	{
		if (!m_postData->AppendExternFunctionCall(func, entryFunction))
			return false;
	}

	if (entryFunction)
	{
		if (!entryFunction->FixCodeSectionJumpOffset(prevEntryLength))
			return false;
	}
	return true;
}

bool PostProcess::InsertModule()
{
	std::list<std::string> needinit;

	for (auto &module : m_modules)
	{
		needinit = module->NeedInitList();
		for (const auto &func : needinit)
		{
			RegistNeedinitFunction(func);
		}
        if (!module->Insert(m_postData, [this](const std::string &funcName) { return this->FindSpecialFunction(funcName); })) {
            return false;
        }
	}
	return true;
}

bool PostProcess::CheckMapDirectory()
{
	std::string mapFolder = m_noxpath + "\\maps";
	std::string mapFullName = mapFolder + '\\' + m_mapname + '\\' + m_mapname + ".map";

	return FileStream(mapFullName).Exist();
}

std::string PostProcess::GetOutputObjectFileName()
{
    if (!m_altpath.empty())
    {
        return stringFormat("%s/%s.obj", m_altpath, m_mapname);
    }
	std::string mapFolder = m_noxpath + "\\maps";
	std::string mapFolderDetail = mapFolder + '\\' + m_mapname + '\\';

	return mapFolderDetail + m_mapname + "out.obj";
}

bool PostProcess::RegistNeedinitFunction(const std::string &name)
{
	auto check = m_extDupCheck.find(name);

	if (check == m_extDupCheck.end())
	{
		m_extCallFunctions.emplace_front(name);
		m_extDupCheck.emplace(name);
		return true;
	}
	return false;
}

uint32_t PostProcess::FindSpecialFunction(const std::string &key)
{
	auto keyIterator = m_specialFuncs.find(key);

	if (keyIterator != m_specialFuncs.end())
		return keyIterator->second;
	return 0;
}