
#include "builtins.h"
#include "preprocess\scriptProcess.h"
#include "preprocess\tokenizer.h"
#include "builtinsMake.h"
#include "scriptString.h"
#include "functionPool.h"
#include "scriptStream.h"

Builtins::Builtins(std::shared_ptr<FunctionPool> functionPool, const std::string &compilerPath)
{
	m_functionPool = functionPool;
	m_builtinsFilename = compilerPath + "\\builtins.h";
	m_strManager = std::make_shared<ScriptString>();
}

Builtins::~Builtins()
{ }

bool Builtins::Fetch()
{
	m_builtinsStream = std::make_shared<ScriptStream>(m_builtinsFilename);
	uint32_t res = m_builtinsStream->FileProcess();

	if (res != 0)
	{
		m_processes.emplace_back(std::make_unique<Tokenizer>(m_builtinsStream, m_strManager));
		m_processes.emplace_back(std::make_unique<BuiltinsMake>(m_builtinsStream, m_functionPool.lock()));
		if (Progress())
			return true;
		else
		{
			std::cout << "get an error\n";
		}
	}
	else
		std::cout << "builtins.h file is missing\n";
	return false;
}

void Builtins::NotifyError(ScriptProcess *errProcess)
{
	std::cout << "builtins load error:: in " << errProcess->ProcessName() << std::endl;
}

bool Builtins::Progress()
{
	bool pResult = true;

	while (m_processes.size())
	{
		m_processes.front()->Initialize();
		pResult = m_processes.front()->Process();
		m_processes.front()->Deinitialize(pResult);
		if (!pResult)
			break;
		m_processes.pop_front();
	}
	if (!pResult)
		NotifyError(m_processes.front().get());
	return pResult;
}