
#include "tokenGroup.h"
#include "scriptToken.h"

#include <algorithm>
#include <iostream>

TokenGroup::TokenGroup(std::shared_ptr<TokenGroup> parent)
	: ScriptUnit()
{
	m_type = GroupTypes::NONE;
	m_parent = parent;
	m_functionEntry = false;
	m_linenumber = 0;	//added
	m_arrtype = -1;

	m_pointerArray = false;
}

TokenGroup::~TokenGroup()
{ }

int TokenGroup::UnitType() const
{
	return UnitTypes::GROUP;
}

void TokenGroup::Append(std::shared_ptr<ScriptUnit> scrToken)
{
	if (m_tokens.size())
	{
		std::shared_ptr<ScriptUnit> prev(m_tokens.back());	//else

		if (prev->UnitTokenType() == ScriptToken::SCR_COMMIT)
		{
			if (prev->UnitName() == "else")
			{
				if (scrToken->UnitName() == "if")
				{
					m_tokens.pop_back();
					scrToken->SetUnitName("else if");
				}
			}
		}
	}
	m_tokens.push_back(scrToken);
}

void TokenGroup::AppendFront(std::shared_ptr<ScriptUnit> scrunit)
{
	m_tokens.push_front(scrunit);
}

void TokenGroup::SetGroupTypeFromChar(char c)
{
	switch (c)
	{
	case '(':
		m_type = GroupTypes::TINY_TYPE;
		break;
	case '[':
		m_type = GroupTypes::ARRAY_TYPE;
		break;
	case '{':
		m_type = GroupTypes::MID_TYPE;
		break;
	default:
		m_type = GroupTypes::NONE;
	}
}

std::shared_ptr<ScriptUnit> TokenGroup::Front() const
{
	if (m_tokens.size())
		return m_tokens.front();
	else
		return nullptr;
}

std::shared_ptr<ScriptUnit> TokenGroup::Back() const
{
	if (m_tokens.size())
		return m_tokens.back();
	else
		return nullptr;
}

void TokenGroup::PrintAll()
{
	uint32_t itemcount = m_tokens.size();

	while (itemcount != 0)
	{
		if (m_tokens.front()->UnitType() == ScriptUnit::GROUP)
		{
			TokenGroup *node = dynamic_cast<TokenGroup *>(m_tokens.front().get());
			if (m_tokens.front()->Declared())
				std::cout << "  def:" << m_tokens.front()->UnitName();
			else
				std::cout << "  " << m_tokens.front()->UnitName();
			std::cout << node->GetScopeName(true);
			dynamic_cast<TokenGroup *>(m_tokens.front().get())->PrintAll();
			std::cout << node->GetScopeName();
			std::cout << "  ";
		}
		else
		{
			if (m_tokens.front()->Declared())
				std::cout << " def:\'" << m_tokens.front()->UnitName() << "\'  ";
			else
				std::cout << "  \'" << m_tokens.front()->UnitName() << "\'  ";
		}
		m_tokens.push_back(m_tokens.front());
		m_tokens.pop_front();
		--itemcount;
	}
	
}

void TokenGroup::Refresh()
{
	uint32_t itemcount = m_tokens.size();

	while (itemcount != 0)
	{
		if (!m_tokens.front()->Eliminated())
		{
			if (m_tokens.front()->UnitType() == ScriptUnit::GROUP)
				dynamic_cast<TokenGroup *>(m_tokens.front().get())->Refresh();
			m_tokens.push_back(m_tokens.front());
		}
		m_tokens.pop_front();
		--itemcount;
	}
}

char TokenGroup::GetScopeName(bool open)
{
	switch (m_type)
	{
	case GroupTypes::NONE:
		return 0;
	case GroupTypes::TINY_TYPE:
		return open ? '(' : ')';
	case GroupTypes::ARRAY_TYPE:
		return open ? '[' : ']';
	case GroupTypes::MID_TYPE:
		return open ? '{' : '}';
	default:
		break;
	}
	return 0;
}

uint32_t TokenGroup::Length() const
{
	return m_tokens.size();
}

uint32_t TokenGroup::Linenumber() const
{
	if (m_linenumber)
		return m_linenumber;
	else if (m_tokens.size() > 0)
		return m_tokens.front()->Linenumber();
	return 0;
	/*if (m_tokens.size() > 0)
		return m_tokens.front()->Linenumber();
	else
		return m_linenumber;*/
}

ScriptUnit *TokenGroup::Clone()
{
	return new TokenGroup(*this);
}