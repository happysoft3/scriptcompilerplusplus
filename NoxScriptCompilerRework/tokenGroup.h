
#ifndef TOKEN_GROUP_H__
#define TOKEN_GROUP_H__

#include "scriptUnit.h"

#include <list>
#include <memory>

class ScriptToken;

class TokenGroup : public ScriptUnit
{
public:
	enum GroupTypes
	{
		NONE,
		ARRAY_TYPE,	// '['
		MID_TYPE,	// '{'
		TINY_TYPE   // '('
	};

private:
	std::list<std::shared_ptr<ScriptUnit>> m_tokens;
	std::weak_ptr<TokenGroup> m_parent;
	std::string m_groupName;
	GroupTypes m_type;
	bool m_functionEntry;

	uint32_t m_linenumber;

	uint32_t m_arrtype;
	bool m_pointerArray;

public:
	explicit TokenGroup(std::shared_ptr<TokenGroup> parent = nullptr);
	virtual ~TokenGroup() override;

	virtual int UnitType() const override;
	virtual int UnitTokenType() const override { return -1; }

	void Append(std::shared_ptr<ScriptUnit> scrUnit);
	void AppendFront(std::shared_ptr<ScriptUnit> scrunit);
	void Clear() { m_tokens.clear(); };

	std::shared_ptr<TokenGroup> GetParent() const { return m_parent.lock(); }
	void SetGroupType(GroupTypes type) { m_type = type; }
	void SetGroupTypeFromChar(char c);
	GroupTypes GroupType() const { return m_type; }
	void SetGroupName(const std::string &name) { m_groupName = name; }
	std::string GroupName() const { return m_groupName; }
	virtual void SetFunctionEntry() override { m_functionEntry = true; }
	virtual bool FunctionEntry() const override { return m_functionEntry; }

	std::list<std::shared_ptr<ScriptUnit>> GetList() const { return m_tokens; }
	void SetList(std::list<std::shared_ptr<ScriptUnit>> &change) { m_tokens = change; }
	std::shared_ptr<ScriptUnit> Front() const;
	std::shared_ptr<ScriptUnit> Back() const;
	void Popback() { m_tokens.pop_back(); }

	void PrintAll();
	void Refresh();
	char GetScopeName(bool open = false);

	virtual std::string UnitName() const override { return GroupName(); }
	virtual void SetUnitName(const std::string &name) override { SetGroupName(name); }

	virtual uint32_t Length() const override;

	void SetLinenumber(uint32_t linenumber) { m_linenumber = linenumber; }
	virtual uint32_t Linenumber() const override;

	virtual ScriptUnit *Clone() override;

	uint32_t ArrayType() const { return m_arrtype; }
	void SetArrayType(uint32_t set) { m_arrtype = set; }

	bool PointerArray() const { return m_pointerArray; }
	void SetPointerArray(bool set = true) { m_pointerArray = set; }
};

#endif