
#include "builtinsMake.h"
#include "functionPool.h"
#include "scriptStream.h"
#include "scriptTypes.h"
#include "tokenGroup.h"
#include "scriptToken.h"

BuiltinsMake::BuiltinsMake(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<FunctionPool> functionPoolRef)
	: ScriptProcess()
{
	m_streamRef = streamRef;
	m_functionPoolRef = functionPoolRef;
	m_typeChecker = std::make_unique<ScriptTypes>();
}

BuiltinsMake::~BuiltinsMake()
{ }

void BuiltinsMake::Initialize()
{
	std::shared_ptr<ScriptStream> streamRef = m_streamRef.lock();

	if (streamRef)
		m_rootGroup = streamRef->GetTokenRoot();
}

bool BuiltinsMake::FunctionDetector(const std::list<std::shared_ptr<ScriptUnit>> &fqueue)
{
	while (fqueue.back()->UnitType() == ScriptUnit::GROUP)
	{
		if (static_cast<TokenGroup *>(fqueue.back().get())->GroupType() == TokenGroup::TINY_TYPE)
		{
			if (m_typeChecker->IsValid(fqueue.back()->UnitName()))
				break;
			return m_functionPoolRef.lock()->Append(fqueue.back()->UnitName(), fqueue.front()->UnitName());
		}
		break;
	}
	return false;
}

bool BuiltinsMake::Make(std::shared_ptr<ScriptUnit> symbols)
{
	std::shared_ptr<FunctionPool> functionPool = m_functionPoolRef.lock();
	std::list<std::shared_ptr<ScriptUnit>> tokens = dynamic_cast<TokenGroup *>(symbols.get())->GetList();
	std::shared_ptr<ScriptUnit> dummy = std::make_shared<ScriptToken>("dummy", 0, ScriptToken::NONE);
	std::list<std::shared_ptr<ScriptUnit>> fqueue({dummy});
	
	while (tokens.size())
	{
		fqueue.push_back(tokens.front());
		if (fqueue.front()->UnitTokenType() == ScriptToken::KEYWORD)	//type
		{
			if (m_typeChecker->IsValid(fqueue.front()->UnitName()))		//typeOk
				FunctionDetector(fqueue);
		}
		fqueue.pop_front();
		tokens.pop_front();
	}
	return true;
}

bool BuiltinsMake::Process()
{
	bool pResult = Make(m_rootGroup.lock());

	if (!pResult)
		std::cout << "builtins make process error\n";
	/*else
		m_functionPoolRef.lock()->PrintAll();*/
	return pResult;
}

void BuiltinsMake::Deinitialize(bool pResult)
{
	m_rootGroup.reset();
}

