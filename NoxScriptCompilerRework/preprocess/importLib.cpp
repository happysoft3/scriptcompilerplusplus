
#include "importLib.h"
#include "premodule.h"
#include "scriptStream.h"
#include "scriptString.h"
#include "tokenGroup.h"
#include "scriptToken.h"
#include "utils/stringHelper.h"

#include <algorithm>

using namespace _StringHelper;

ImportLib::ImportLib(std::shared_ptr<ScriptStream> selfStreamRef, std::shared_ptr<ScriptString> strManagerRef, std::shared_ptr<ScriptStream> mainStreamRef)
    : ScriptProcess(), m_importkey("#include")
{
    m_mainstreamRef = (mainStreamRef == nullptr) ? selfStreamRef : mainStreamRef;
    m_isMain = selfStreamRef == m_mainstreamRef.lock();
    m_selfstreamRef = selfStreamRef;
    m_strManagerRef = strManagerRef;
    m_additionPath = selfStreamRef->HeaderPath();
}

ImportLib::~ImportLib()
{ }

bool ImportLib::ImportModule(const std::string &modfullname, std::shared_ptr<ScriptUnit> cToken)
{
    std::shared_ptr<ScriptStream> mainstream = m_mainstreamRef.lock();

    if (!mainstream)
        return false;

    if (!mainstream->AppendModuleName(modfullname))
        return false;

    std::unique_ptr<PreModule> modul(new PreModule(m_additionPath + '\\' + modfullname, mainstream));
    bool loaded = modul->Load();

    if (loaded)
        mainstream->AppendModuleObject(modfullname, std::move(modul));
    else
        m_errorList.emplace_back("fail to load " + modfullname, cToken);

    return loaded;
}

std::string ImportLib::FixPathToken(const std::string &src)
{
    std::string dest = src;

    std::transform(src.begin(), src.end(), dest.begin(), [](auto c)
    {
        return (c == '/') ? '\\' : c;
    });
    return dest;
}

bool ImportLib::Traversal(std::shared_ptr<ScriptUnit> root)
{
    std::list<std::shared_ptr<ScriptUnit>> tokens = static_cast<TokenGroup *>(root.get())->GetList();
    std::shared_ptr<ScriptUnit> prev;
    std::shared_ptr<ScriptStream> selfStream = m_selfstreamRef.lock();
    std::shared_ptr<ScriptString> strManager = m_strManagerRef.lock();

    if (!selfStream || !strManager)
        return false;

    tokens.emplace_back(new ScriptToken);

    while (tokens.size())
    {
        do
        {
            if (!prev || prev->Eliminated()) break;
            if (prev->UnitType() == ScriptUnit::SINGLE)
            {
                if (prev->UnitTokenType() == ScriptToken::DEFINE)
                {
                    if (prev->UnitName() != m_importkey)
                        break;

                    if (tokens.front()->UnitTokenType() != ScriptToken::SCR_STRING)
                    {
                        m_errorList.emplace_back("import keyword syntax error::", tokens.front());
                        return false;
                    }
                    std::string modulename = strManager->FindStringWithKeyRaw(tokens.front()->UnitName());
                    
                    if (modulename.length() > 0)
                        ImportModule(FixPathToken(modulename), prev);
                    else
                        m_errorList.emplace_back("module url error::", tokens.front());
                    prev->SetEliminate();
                    tokens.front()->SetEliminate();
                }
            }
        }
        while (false);
        prev = tokens.front();
        tokens.pop_front();
    }
    return m_errorList.empty();
}

void ImportLib::Initialize()
{
    std::shared_ptr<ScriptStream> selfStream = m_selfstreamRef.lock();

    if (!selfStream)
        return;

    m_rootGroup = selfStream->GetTokenRoot();

    auto top = m_rootGroup.lock();

    if (!top)
        return;

    if (!m_isMain)
        return;

    std::list<std::string> mdlist = selfStream->GetModuleList();

    selfStream->ClearModuleList();

    for (const auto &mname : mdlist)
        ImportModule(mname, top);
}

bool ImportLib::Process()
{
    if (m_rootGroup.expired())
        return false;

    bool pResult = Traversal(m_rootGroup.lock());

    PrintErrorList();

    return pResult;
}

void ImportLib::Deinitialize(bool pResult)
{
    if (pResult)
    {
        std::shared_ptr<TokenGroup> root(m_rootGroup.lock());

        if (root)
            root->Refresh();
    }
}

void ImportLib::PrintErrorList()
{
    while (m_errorList.size() != 0)
    {
        std::string errorMessage = std::get<0>(m_errorList.front());
        std::shared_ptr<ScriptUnit> scrUnit = std::get<1>(m_errorList.front()).lock();

        if (scrUnit->UnitType() == ScriptUnit::SINGLE)
        {
            ScriptToken *scrToken = dynamic_cast<ScriptToken *>(scrUnit.get());

            ErrorReport("error::", scrToken->Name(), " :: ", errorMessage, "::line:", scrToken->Linenumber());
        }
        m_errorList.pop_front();
    }
}
