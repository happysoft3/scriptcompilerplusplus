
#include "scriptMain.h"
#include "systemEnv.h"

#include <iostream>


int main(int argc, char **argv)
{
	std::shared_ptr<SystemEnv> env(SystemEnv::GetInstance());

	ScriptMain scrmain(env);

	if (!scrmain.Initialize())
		std::cout << "Error::Fail to initialize\n";

	std::getchar();
	return 0;
}

