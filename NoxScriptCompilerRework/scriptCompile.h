
#ifndef SCRIPT_COMPILE_H__
#define SCRIPT_COMPILE_H__

#include <string>
#include <list>
#include <memory>
#include <atomic>
#include <map>

class ScriptProcess;
class ScriptStream;
class FunctionPool;
class ScriptString;
class PostProcess;
class SystemEnv;

//@brief. 스크립트 컴파일 메인루틴
class ScriptCompile
{
private:
	std::string m_filename;
	std::shared_ptr<ScriptStream> m_scrStream;
	std::list<std::unique_ptr<ScriptProcess>> m_processes;
	std::shared_ptr<FunctionPool> m_functionPool;
	std::shared_ptr<ScriptString> m_strManager;

	std::shared_ptr<FunctionPool> m_builtinsPool;

	std::atomic<bool> m_running;
	std::atomic<bool> m_timerOn;

	std::string m_compilerPath;
	std::string m_compilerExeName;
	std::string m_scriptPath;
	std::string m_noxpath;
	std::string m_repoPath;
	std::string m_modulePath;
	std::string m_headerPath;

	std::list<std::string> m_filenameList;
	std::string m_resultObjFilename;

	std::unique_ptr<PostProcess> m_postProc;
	std::weak_ptr<SystemEnv> m_systemEnv;

public:
	explicit ScriptCompile(std::shared_ptr<SystemEnv> systemEnv);
	~ScriptCompile();

	bool Initialize();
	
	void SetFilename(const std::string &filename) { m_filename = filename; }
	void DoCompile(const std::string &filename);
	void SetSomeUrl(const std::map<std::string, std::string> &urlpack);

private:
	void SoundNotification(bool pResult);
	void ReportCompileEnd(bool pResult);
	bool Compile();
	bool CompilerExist();
	bool CompilerExec();
	bool LoadBuiltins();

	void PopRequest();
	static void EndTimer();
	void PushRequest(const std::string &filename);

public:
	static void RequestCompile(std::string filename, uint32_t actionId);

private:
	bool EntryPost();
	void ReportPost();
};

#endif