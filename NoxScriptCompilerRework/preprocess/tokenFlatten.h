
#ifndef TOKEN_FLATTEN_H__
#define TOKEN_FLATTEN_H__

#include "scriptProcess.h"

#include <memory>
#include <list>

class ScriptStream;
class FunctionPool;
class ScriptTypes;
class ScriptFunction;
class TokenGroup;
class ScriptUnit;

class TokenFlatten : public ScriptProcess
{
private:
	std::weak_ptr<ScriptStream> m_streamRef;
	std::weak_ptr<FunctionPool> m_functionPoolRef;
	std::unique_ptr<ScriptTypes> m_typeChecker;

	std::weak_ptr<ScriptFunction> m_globalFunction;
	std::weak_ptr<ScriptFunction> m_currentFunction;

	std::weak_ptr<TokenGroup> m_rootGroup;

	using nodeErrorTypes = std::tuple<std::string, std::weak_ptr<ScriptUnit>, std::weak_ptr<ScriptFunction>>;
	std::list<nodeErrorTypes> m_errorList;

public:
	explicit TokenFlatten(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<FunctionPool> functionPoolRef);
	virtual ~TokenFlatten() override;

private:
	virtual void Initialize() override;
	bool CheckPlusminus(const std::shared_ptr<ScriptUnit> &scr);
	bool IsIncOp(const std::shared_ptr<ScriptUnit> &unit);
	bool CheckKeyword(const std::shared_ptr<ScriptUnit> &scr);
    std::string varSetOperator(std::shared_ptr<ScriptUnit> &tok);
	
	bool ArrayConvert(std::shared_ptr<ScriptUnit> arr, std::shared_ptr<ScriptFunction> cFunction);

    void referenceSymb(ScriptUnit &unit, bool &scan);
	bool Some(std::shared_ptr<ScriptUnit> currentGroup, std::shared_ptr<ScriptFunction> currentFunction);
	bool Make(std::shared_ptr<ScriptUnit> currentGroup, std::shared_ptr<ScriptUnit> destGroup);

	bool FunctionOrder(std::shared_ptr<ScriptUnit> rootUnit);
	virtual bool Process() override;
	virtual void Deinitialize(bool pResult) override;
	void PrintErrorList();
};

#endif