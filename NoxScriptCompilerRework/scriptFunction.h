
#ifndef SCRIPT_FUNCTION_H__
#define SCRIPT_FUNCTION_H__

#include <list>
#include <memory>
#include <map>
#include <string>

class ScriptTypes;
class ScriptVariable;

//@brief. 함수 노드 클래스
class ScriptFunction
{
private:
	std::string m_functionName;
	uint32_t m_functionNumber;
	uint32_t m_variableOffset;
	std::weak_ptr<ScriptTypes> m_typeChecker;
	
	std::map<std::string, std::shared_ptr<ScriptVariable>> m_variableMap;		//todo. 변수 목록을 가져야 한다 + arguments
	std::list<std::weak_ptr<ScriptVariable>> m_variableList;

	std::list<uint32_t> m_argTypelist;	//인자 타입리스트
	uint32_t m_retType; //리턴타입 

	std::unique_ptr<ScriptVariable> m_latestVariable;

	bool m_externFunction;
	bool m_overrideFunction;

public:
	explicit ScriptFunction(const std::string &functionName, uint32_t functionNumber);
	~ScriptFunction();

	void RegistTypeChecker(std::shared_ptr<ScriptTypes> typeChecker);
	std::string FunctionName() const { return m_functionName; }
	uint32_t FunctionNumber() const { return m_functionNumber; }
	void SetReturnType(const std::string &typekey);
	uint32_t GetReturnType() const { return m_retType; }
	std::list<std::weak_ptr<ScriptVariable>> GetArgumentList();

	ScriptVariable* CreateVariable();
	ScriptVariable *GetLatestVariable() const;
	ScriptVariable *GetVariable(const std::string &varname);

	std::shared_ptr<ScriptVariable> AppendVariable(const std::string &varName);

	void PrintAll();
	bool VarRefresh();

	bool IsExtern() const { return m_externFunction; }
	void SetExtern() { m_externFunction = true; }
	bool IsOverride() const { return m_overrideFunction; }
	void SetOverrideFunction() { m_overrideFunction = true; }

	void ChangeFunctionId(uint32_t id) { m_functionNumber = id; }
    int VariableCount() const
    {
        return m_variableList.size();
    }

private:
	void PrintError(const std::string &message);

public:
    void AddFunctionNumberMarkerVariable();
};

#endif