
#include "scriptToken.h"

ScriptToken::ScriptToken()
	: m_type(TokenType::NONE)
{
	m_lineNumber = 0;
	m_optype = OpTypes::OP_NONE;
}

ScriptToken::ScriptToken(const std::string &name, uint32_t lineNumber, ScriptToken::TokenType type)
	: m_token(name), m_lineNumber(lineNumber), m_type(type)
{
	m_optype = OpTypes::OP_NONE;
}

ScriptToken::~ScriptToken()
{ }

void ScriptToken::SetLinenumber(uint32_t lineNumber)
{
	m_lineNumber = lineNumber;
}

void ScriptToken::SetFirstChar(char c)
{
	if (m_token.length())
		m_token[0] = c;
}

void ScriptToken::SetName(const std::string &name)
{
	m_token = name;
}

void ScriptToken::SetType(ScriptToken::TokenType type)
{
	m_type = type;
}

int ScriptToken::UnitType() const
{
	return UnitTypes::SINGLE;
}

int ScriptToken::UnitTokenType() const
{
	return m_type;
}

ScriptUnit *ScriptToken::Clone()
{
	return new ScriptToken(*this);
}