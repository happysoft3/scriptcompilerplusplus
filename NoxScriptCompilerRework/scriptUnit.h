
#ifndef SCRIPT_UNIT_H__
#define SCRIPT_UNIT_H__

#include <string>

class ScriptUnit
{
private:
	bool m_eliminated;
	bool m_declared;
	bool m_assignop;
	bool m_lassignExpr;
	uint32_t m_specialProperty;

public:
	enum UnitTypes
	{
		SINGLE,
		GROUP
	};

	enum SpecialProperty
	{
		OP_NOTHING,
		OP_BEFORE_PLSHMINUS,
		OP_AFTER_PLSHMINUS,
		OP_LEFT_POINTER_ASSIGN,
		OP_RIGHT_REF,
		OP_RIGHT_REVREF,
		OP_BEFORE_PLUS,
		OP_BEFORE_MINUS,
		OP_AFTER_PLUS,
		OP_AFTER_MINUS,
		OP_FUNCTION_END,
		OP_SCOPE_END,
        OP_ARR_SIZEOF,
        OP_ARR_REF,
	};

public:
	explicit ScriptUnit();
	virtual ~ScriptUnit();

	virtual int UnitType() const = 0;
	virtual int UnitTokenType() const = 0;
	void SetEliminate() { m_eliminated = true; };
	bool Eliminated() const { return m_eliminated; }

	void SetDeclare() { m_declared = true; }
	void UnsetDeclare() { m_declared = false; }
	bool Declared() const { return m_declared; }

	void SetAssignop(bool set = true) { m_assignop = set; }
	bool AssignOp() const { return m_assignop; }

	void SetLeftAssign() { m_lassignExpr = true; }
	bool LeftAssign() const { return m_lassignExpr; }

	int GetSpecialProperty() const { return m_specialProperty; }
	void SetSpecialProperty(int value) { m_specialProperty = value; }

	virtual bool FunctionEntry() const { return false; }
	virtual void SetFunctionEntry() {}

	virtual std::string UnitName() const = 0;
	virtual void SetUnitName(const std::string &name) {};

	virtual uint32_t Length() const = 0;
	virtual uint32_t Linenumber() const = 0;

	virtual ScriptUnit *Clone() = 0;
};

#endif