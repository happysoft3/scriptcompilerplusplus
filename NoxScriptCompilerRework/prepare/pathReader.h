
#ifndef PATH_READER_H__
#define PATH_READER_H__

#include "utils\fileStream.h"

#include <map>

class PathReader : public FileStream
{
private:
	std::string m_stream;
	std::map<std::string, std::string> m_pathMap;

public:
	explicit PathReader();
	virtual ~PathReader() override;

private:
	void MakePathMap();
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream) override;
	std::string ToAnsi(const std::string &utf);

public:
	std::string FindPath(const std::string &key, bool to_ansi = false);
};

#endif