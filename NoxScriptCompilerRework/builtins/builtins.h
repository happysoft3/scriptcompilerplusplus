
#ifndef BUILTINS_H__
#define BUILTINS_H__

#include <memory>
#include <string>
#include <list>

class FunctionPool;
class ScriptStream;
class ScriptProcess;
class ScriptString;

//@brief. 빌트인 함수를 가져오는 객체입니다
class Builtins
{
private:
	std::string m_builtinsFilename;
	std::weak_ptr<FunctionPool> m_functionPool;
	std::shared_ptr<ScriptStream> m_builtinsStream;
	std::list<std::unique_ptr<ScriptProcess>> m_processes;
	std::shared_ptr<ScriptString> m_strManager;

public:
	explicit Builtins(std::shared_ptr<FunctionPool> functionPool, const std::string &compilerPath);
	~Builtins();

	bool Fetch();

private:
	void NotifyError(ScriptProcess *errProcess);
	bool Progress();
};

#endif