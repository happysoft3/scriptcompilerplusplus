
#include "scriptTypes.h"

static const std::vector<int> s_typeSize(
{
    4,
    4,
    4,
    4,
    1,
    2
});

ScriptTypes::ScriptTypes()
{
	m_typeMap["void"] = SCR_VOID;
	m_typeMap["int"] = SCR_INT;
	m_typeMap["string"] = SCR_STRING;
	m_typeMap["object"] = SCR_OBJECT;
	m_typeMap["float"] = SCR_FLOAT;
    m_typeMap["char"] = SCR_CHAR;   //added
    m_typeMap["short"] = SCR_SHORT; //added

	m_typeV.resize(m_typeMap.size());
	for (const auto &item : m_typeMap)
		m_typeV[item.second] = item.first;

	m_typeV[SCR_OBJECT] = m_typeV[SCR_INT];	//임시처리.. 추후 typedef 랑 연계해서 하면 더 깔끔할지싶다

    m_realTypeMap[SCR_VOID] = GetTypeName(SCR_VOID);
    m_realTypeMap[SCR_INT] = GetTypeName(SCR_INT);
    m_realTypeMap[SCR_STRING] = GetTypeName(SCR_STRING);
    m_realTypeMap[SCR_OBJECT] = GetTypeName(SCR_OBJECT);
    m_realTypeMap[SCR_FLOAT] = GetTypeName(SCR_FLOAT);
    m_realTypeMap[SCR_CHAR] = GetTypeName(SCR_INT);
    m_realTypeMap[SCR_SHORT] = GetTypeName(SCR_INT);
}

ScriptTypes::~ScriptTypes()
{ }

bool ScriptTypes::IsValid(const std::string &typeKey)
{
	auto keyIterator = m_typeMap.find(typeKey);

	return (keyIterator != m_typeMap.end()) ? true : false;
}

uint32_t ScriptTypes::GetTypeId(const std::string &typeKey)
{
	auto keyItertor = m_typeMap.find(typeKey);

	if (keyItertor != m_typeMap.end())
		return keyItertor->second;
	return ScrType::SCR_INVALID;
}

std::string ScriptTypes::GetTypeName(uint32_t typeNumber) const
{
	if (typeNumber >= m_typeV.size())
		return{};

	return m_typeV[typeNumber];
}

std::string ScriptTypes::GetRealTypeName(uint32_t typeId) const
{
    auto tyIterator = m_realTypeMap.find(typeId);

    if (tyIterator == m_realTypeMap.cend())
        return GetTypeName(SCR_INT);

    return tyIterator->second;
}

std::string ScriptTypes::GetUnDwordTypeName(uint32_t typeId) const
{
    if (typeId == SCR_CHAR || typeId == SCR_SHORT)
        return GetTypeName(typeId);

    return{ };
}

uint32_t ScriptTypes::TypeSize(uint32_t typeId) const
{
    return s_typeSize[typeId];
}
