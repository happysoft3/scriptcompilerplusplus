
#ifndef SCRIPT_STREAM_H__
#define SCRIPT_STREAM_H__

#include "utils\fileStream.h"
#include <memory>
#include <list>
#include <set>
#include <functional>
#include <map>

class ScriptUnit;
class TokenGroup;
class PreModule;

//Script파일의 I/O를 담당
class ScriptStream : public FileStream
{
private:
	std::string m_stream;
	std::string m_filename;

	std::shared_ptr<TokenGroup> m_tokenRoot;
	std::set<std::string> m_commonKey;

	std::string m_compilerPath;
	std::string m_modulePath;
    std::string m_headerPath;

	std::list<std::string> m_moduleNameList;
	std::set<std::string> m_moduleNameCheck;
    using module_list_ty = std::list<std::shared_ptr<PreModule>>;
    module_list_ty m_moduleObjList;
    std::map<std::string, module_list_ty::iterator> m_moduleObjMap;

    std::map<std::string, std::shared_ptr<ScriptUnit>> m_externDefines;

public:
	explicit ScriptStream(const std::string &filename);
	virtual ~ScriptStream() override;

    std::list<char> GetListStream()
    {
        return std::list<char>(m_stream.begin(), m_stream.end());
    }

    std::shared_ptr<TokenGroup> GetTokenRoot() const
    {
        return m_tokenRoot;
    }
	void SetRootToken(std::shared_ptr<TokenGroup> &&root);

	bool FindCommonkey(const std::string &key);

	std::string ScriptFileName();
    void SetCompilerPath(const std::string path)
    {
        m_compilerPath = path;
    }

    std::string CompilerPath() const
    {
        return m_compilerPath;
    }

	bool AppendModuleName(const std::string &moduleUrl);
	std::list<std::string> GetModuleList() const;
    void ClearModuleList();

    void SetHeaderPath(const std::string &headerpath)
    {
        m_headerPath = headerpath;
    }

    std::string HeaderPath() const
    {
        return m_headerPath;
    }
    void PreorderModule(std::function<void(PreModule &mod)> &&fn);
    void AppendModuleObject(const std::string &key, std::shared_ptr<PreModule> modobj);
    std::shared_ptr<PreModule> ReleaseModuleObject(const std::string &key);
    std::shared_ptr<PreModule> PopModuleObject();

    bool PushExternDefine(const std::string &key, std::shared_ptr<ScriptUnit> value);
    void RemoveExternDefine(const std::string &key);
    std::map<std::string, std::shared_ptr<ScriptUnit>> GetExternDefine() const
    {
        return m_externDefines;
    }

private:
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream) override;
};

#endif