
#include "scriptFunction.h"
#include "scriptTypes.h"
#include "scriptVariable.h"

#include "preprocess\preprocess_generic.h"

#include <algorithm>
#include <iostream>

ScriptFunction::ScriptFunction(const std::string &functionName, uint32_t functionNumber)
{
	m_variableOffset = 0;
	m_functionNumber = functionNumber;
	m_functionName = functionName;
	m_retType = ScriptTypes::SCR_INVALID;
	m_externFunction = false;
}

ScriptFunction::~ScriptFunction()
{ }

void ScriptFunction::RegistTypeChecker(std::shared_ptr<ScriptTypes> typeChecker)
{
	m_typeChecker = typeChecker;
}

void ScriptFunction::SetReturnType(const std::string &typekey)
{
	std::shared_ptr<ScriptTypes> typeChecker(m_typeChecker.lock());

	if (typeChecker)
	{
		m_retType = typeChecker->GetTypeId(typekey);
	}
}

std::list<std::weak_ptr<ScriptVariable>> ScriptFunction::GetArgumentList()
{
	std::list<std::weak_ptr<ScriptVariable>> arglist;

	for (auto &ptr : m_variableList)
	{
		if (ptr.lock()->IsArgument())
			arglist.push_back(ptr);
	}
	return arglist;
}

ScriptVariable* ScriptFunction::CreateVariable()
{
	m_latestVariable = std::make_unique<ScriptVariable>(this);

	return m_latestVariable.get();
}

ScriptVariable *ScriptFunction::GetLatestVariable() const
{
	return m_latestVariable.get();
}

ScriptVariable *ScriptFunction::GetVariable(const std::string &varname)
{
	auto varIterator = m_variableMap.find(varname);

	if (varIterator != m_variableMap.end())
	{
		return varIterator->second.get();
	}
	return nullptr;
}

//변수를 풀에 등록합니다. 변수명 중복 시 오류입니다
std::shared_ptr<ScriptVariable> ScriptFunction::AppendVariable(const std::string &varName)
{
	auto varIterator = m_variableMap.find(varName);

	if (varIterator == m_variableMap.end())
	{
		std::shared_ptr<ScriptVariable> variable(std::move(m_latestVariable));

		variable->SetName(varName);
		m_variableMap.emplace(varName, variable);
		m_variableList.push_back(variable);

		return variable;
	}
	return nullptr;
}

void ScriptFunction::PrintAll()
{
	std::list<std::weak_ptr<ScriptVariable>> vlist = m_variableList;

	std::cout << m_functionName << "::variables:: " << vlist.size() << std::endl;
	while (vlist.size())
	{
		std::shared_ptr<ScriptVariable> var(vlist.front().lock());

		std::cout << "name: " << var->Name() << ", size: " << var->Size() << ", pointer?=" << var->IsPointer() << std::endl;
		vlist.pop_front();
	}
	std::cout << std::endl;
}

bool ScriptFunction::VarRefresh()
{
	std::list<std::weak_ptr<ScriptVariable>> vlist = m_variableList;
	uint32_t offset = 0;

	while (vlist.size())
	{
		std::shared_ptr<ScriptVariable> var(vlist.front().lock());

		if (var->Size() == 0)
		{
			PrintError(var->Name() + " array size 0");
			return false;
		}
		else
		{
			var->SetOffset(offset);
			//offset += var->Size();
            offset += var->AlignedSize();
		}
		if (var->IsArgument())
			m_argTypelist.push_back(var->Type());
		vlist.pop_front();
	}
	return true;
}

void ScriptFunction::PrintError(const std::string &message)
{
	std::cout << "error::function=" << m_functionName << "::" << message << std::endl;
}



void ScriptFunction::AddFunctionNumberMarkerVariable()
{
    /*if (m_variableMap.empty())
        return;*/

    ScriptTypes st;
    auto var = CreateVariable();

    var->SetType(st.GetTypeId("int"));
    AppendVariable(s_this_function_id);
}
