
#include "makeFunction.h"
#include "scriptStream.h"
#include "functionPool.h"
#include "scriptFunction.h"
#include "scriptVariable.h"
#include "tokenGroup.h"
#include "scriptToken.h"
#include "scriptTypes.h"

MakeFunction::MakeFunction(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<FunctionPool> functionPoolRef)
	: ScriptProcess(), m_externKey("extern"), m_overrideKey("static")
{
	m_typeChecker = std::make_unique<ScriptTypes>();
	m_functionPoolRef = functionPoolRef;
	m_streamRef = streamRef;
}

MakeFunction::~MakeFunction()
{ }

void MakeFunction::Initialize()
{
	std::shared_ptr<FunctionPool> funcPool = m_functionPoolRef.lock();

	if (funcPool)
	{
		m_globalFunction = funcPool->GetGlobalFunction();
		m_currentFunction = m_globalFunction;			//초기에는 전역함수를 현재함수로 지정
	}
	std::shared_ptr<ScriptStream> streamRef = m_streamRef.lock();

	if (streamRef)
		m_rootGroup = streamRef->GetTokenRoot();
}

//함수 argument 등록
bool MakeFunction::MakeArgs(std::shared_ptr<ScriptUnit> argGroup, const std::string &functionName)
{
	std::list<std::shared_ptr<ScriptUnit>> tokens = dynamic_cast<TokenGroup *>(argGroup.get())->GetList();
	std::vector<std::shared_ptr<ScriptUnit>> argV;
	std::shared_ptr<FunctionPool> fpool = m_functionPoolRef.lock();
	std::shared_ptr<ScriptFunction> currentFunction = fpool->GetFunctionNode(functionName);

	while (tokens.size() > 0)
	{
		if (tokens.front()->UnitType() == ScriptUnit::SINGLE)
		{
			ScriptToken *scrToken = dynamic_cast<ScriptToken *>(tokens.front().get());

			if (scrToken->Type() == ScriptToken::KEYWORD)
			{
				if (m_typeChecker->IsValid(scrToken->Name()))
				{
					if (argV.size())
						return false;	//syntax error!
					argV.resize(3);
					argV[0] = tokens.front();
				}
				else if (argV.size())
					argV[2] = tokens.front();
				else
					return false;	//syntax error!
			}
			else if (scrToken->Type() == ScriptToken::OPERATOR)
			{
				if (argV.size() > 0)
					argV[1] = tokens.front();
			}
		}
		else
			return false;
		if (argV.size())
		{
			if (argV[0] && argV[2])
			{
				uint32_t typeNumber = m_typeChecker->GetTypeId(dynamic_cast<ScriptToken *>(argV[0].get())->Name());
				bool isPointer = argV[1] ? dynamic_cast<ScriptToken *>(argV[1].get())->FirstChar() == '*' : false;
				std::string varName = dynamic_cast<ScriptToken *>(argV[2].get())->Name();
				ScriptVariable *var = currentFunction->CreateVariable();
				
				if (var)
				{
					if (!currentFunction->AppendVariable(varName))
						return false;
					var->SetArgument();
					var->SetType(typeNumber);
					if (isPointer)
						var->SetPointer();
				}
				else
					return false;	//error- 변수명 중복
				argV.clear();
			}
		}
		tokens.pop_front();
	}
	argGroup->SetEliminate();
	return argV.size() == 0;
}

bool MakeFunction::Make(std::shared_ptr<ScriptUnit> currentGroup)
{
	std::shared_ptr<FunctionPool> functionPool = m_functionPoolRef.lock();
	std::list<std::shared_ptr<ScriptUnit>> tokens = dynamic_cast<TokenGroup *>(currentGroup.get())->GetList();
	std::list<std::shared_ptr<ScriptUnit>> fkeylist;
	std::shared_ptr<ScriptUnit> prev;
	uint32_t ftype = 0;

	tokens.emplace_back(new ScriptToken);

	while (tokens.size() > 0)
	{
		do
		{
			if (!prev || prev->Eliminated()) break;
			if (prev->UnitType() == ScriptUnit::SINGLE)
			{
				if (prev->UnitTokenType() == ScriptToken::KEYWORD)
				{
					if (m_typeChecker->IsValid(prev->UnitName()))	//타입이 왔습니다
						fkeylist.push_back(prev);
				}
				else if (prev->UnitTokenType() == ScriptToken::SCR_COMMIT)	//예약어가 먼저오고
				{
					do
					{
						if (prev->UnitName() == m_overrideKey)
							ftype = 2;
						else if (prev->UnitName() == m_externKey)
							ftype = 1;
						else
							break;
						prev->SetEliminate();
						if (tokens.front()->UnitTokenType() == ScriptToken::KEYWORD)
						{
							if (m_typeChecker->IsValid(tokens.front()->UnitName()))
								break;
						}
						m_errorList.emplace_back("::syntax error::", tokens.front());
					} while (false);
				}
			}

			if (fkeylist.size() > 0)
			{
				if (fkeylist.front() != prev)
					fkeylist.push_back(prev);
			}
			while (fkeylist.size() == 3) //타입+그룹+ (그룹?)
			{
				std::shared_ptr<ScriptUnit> typenode = fkeylist.front();

				fkeylist.pop_front();
				if (fkeylist.front()->UnitType() != ScriptUnit::GROUP || fkeylist.back()->UnitType() != ScriptUnit::GROUP)
				{
					fkeylist.clear();
					break;
				}

				TokenGroup *node = dynamic_cast<TokenGroup *>(fkeylist.front().get());

				if (node->GroupType() == TokenGroup::TINY_TYPE)
				{
					if (node->GroupName().length())
					{
						if (!functionPool->Append(node->GroupName(), dynamic_cast<ScriptToken *>(typenode.get())->Name()))
						{
							m_errorList.emplace_back(node->GroupName() + " function already exist", typenode);
							return false;
						}
						if (!MakeArgs(fkeylist.front(), node->GroupName()))
						{
							m_errorList.emplace_back(node->GroupName() + " function - wrong argument", typenode);
							return false;
						}
						if (fkeylist.back()->UnitName().length())
						{
							m_errorList.emplace_back(fkeylist.back()->UnitName() + "? unknown keyword ", fkeylist.back());
						}

						fkeylist.back()->SetFunctionEntry();
						fkeylist.back()->SetUnitName(node->GroupName());
						typenode->SetEliminate();

						switch (ftype)
						{
						case 1:
							functionPool->GetLatestFunction()->SetExtern();
							break;
						case 2:
							functionPool->GetLatestFunction()->SetOverrideFunction();
							break;
						default:
							break;
						}

					}
				}
				ftype = 0;
				fkeylist.clear();
				break;
			}
		} while (false);
		prev = tokens.front();
		tokens.pop_front();
	}
	return fkeylist.size() == 0;
}

bool MakeFunction::Process()
{
	bool pResult = Make(m_rootGroup.lock());

	PrintErrorList();

	return pResult & (m_errorList.size() == 0);
}

void MakeFunction::PrintErrorList()
{
	while (m_errorList.size() > 0)
	{
		std::string errorMessage = std::get<0>(m_errorList.front());
		std::shared_ptr<ScriptUnit> scrUnit = std::get<1>(m_errorList.front()).lock();

		if (scrUnit)
			ErrorReport("error::", scrUnit->UnitName(), " :: ", errorMessage, "::line:", scrUnit->Linenumber());
		m_errorList.pop_front();
	}
}

void MakeFunction::Deinitialize(bool pResult)
{
	if (pResult)
	{
		std::shared_ptr<TokenGroup> root(m_rootGroup.lock());

		root->Refresh();
	}
}