
#include "postScrData.h"
#include "postScrFunction.h"

struct PostScrData::PostStringData
{
    std::string m_str;
    int m_index;
};

PostScrData::PostScrData()
	: FileStream("file", FileMode::FILE_MODE_WRITE)
{
	m_order = 0;
    m_strCounter = 0;
	m_libraryEntryFunctionName = "NOXLibraryEntryPointFunction";	//is it very unique name?
}

PostScrData::~PostScrData()
{ }

uint32_t PostScrData::AppendScrString(const std::string &str)
{
    auto strData = std::make_unique<PostStringData>();

    strData->m_str=str;
    strData->m_index = m_strCounter;
	m_scrStrings.push_back(std::move(strData));
    m_strMap[m_strCounter] = std::prev(m_scrStrings.end());
    m_strIndexMap[str] = m_strCounter;
    return m_strCounter++;
}

//void PostScrData::AppendScrString(std::string &&str)
//{
//	m_scrStrings.emplace_back(std::forward<std::string>(str));
//}

void PostScrData::AppendScrFunction(const std::string &functionName)
{
	std::shared_ptr<PostScrFunction> scrFunction(new PostScrFunction(this));

	m_funcIndexMap.emplace(m_order, scrFunction);
	scrFunction->SetFunctionIndex(m_order++);
	m_funcMap.emplace(functionName, scrFunction);
	m_funcList.push_back(scrFunction);
}

void PostScrData::AppendScrFunction(const std::shared_ptr<PostScrFunction> &function)
{
	std::shared_ptr<PostScrFunction> copies(new PostScrFunction(*function));
	std::string functionName = copies->Name();

	m_funcIndexMap.emplace(m_order, copies);
	copies->SetFunctionIndex(m_order++);
	copies->ReplaceComplete();	//added
	m_funcMap.emplace(functionName, copies);
	m_funcList.push_back(copies);
}

std::shared_ptr<PostScrFunction> PostScrData::LastFunction()
{
	if (m_funcList.size())
	{
		return m_funcList.back().lock();
	}
	return nullptr;
}

std::shared_ptr<PostScrFunction> PostScrData::FindFunction(const std::string &name)
{
	auto keyIterator = m_funcMap.find(name);

	if (keyIterator != m_funcMap.end())
	{
		return keyIterator->second;
	}
	return nullptr;
}

std::shared_ptr<PostScrFunction> PostScrData::FindFunctionWithIndex(uint32_t index)
{
	auto keyIterator = m_funcIndexMap.find(index);

	if (keyIterator != m_funcIndexMap.end())
	{
		return keyIterator->second.lock();
	}
	return nullptr;
}

std::shared_ptr<PostScrFunction> PostScrData::GetGlobalFunction()
{
	if (m_funcList.size())
	{
		return (++m_funcList.begin())->lock();
	}
	return nullptr;
}

std::shared_ptr<PostScrFunction> PostScrData::FindLibraryEntryFunction()
{
	if (m_funcList.size())
	{
		auto func = m_funcMap.find(m_libraryEntryFunctionName);

		if (func != m_funcMap.end())
			return func->second;
	}
	return nullptr;
}

void PostScrData::CheckingAllFunctions()
{
	for (auto &functionRef : m_funcList)
	{
		std::shared_ptr<PostScrFunction> function = functionRef.lock();

		if (function)
		{
			function->CheckOpcode();
		}
		else
		{
			return;
		}
	}
}

void PostScrData::CommitOutlineSize(uint32_t commit)
{
	if (m_outstream.capacity() < commit)
		m_outstream.reserve(commit);
}

std::list<std::string> PostScrData::GetStringList() const {
    std::list<std::string> copied;

    for (auto &data : m_scrStrings)
        copied.push_back(data->m_str);
    return copied;
}

bool PostScrData::FindString(uint32_t index, std::string &found) const
{
    auto iter = m_strMap.find(index);

    if (iter == m_strMap.cend())
        return false;

    found = (*iter->second)->m_str;
    return true;
}

bool PostScrData::FindStringIndex(const std::string &s, uint32_t &index) const
{
    auto iter = m_strIndexMap.find(s);

    if (iter == m_strIndexMap.cend())
        return false;

    index = iter->second;
    return true;
}

bool PostScrData::AppendExternFunctionCall(const std::string &functionName, const std::shared_ptr<PostScrFunction> &entryFunction)
{
	auto extFunction = FindFunction(functionName);
	auto global = GetGlobalFunction();

	if (extFunction && global)
	{
		std::vector<uint32_t> codes(2);
		codes[0] = 0x46;
		codes[1] = extFunction->FunctionIndex();
		global->AppendCodeFront(codes);
		if (entryFunction)
			entryFunction->AppendCodeFront(codes);
		return true;
	}
	return false;
}

void PostScrData::InsertNumber(uint32_t number)
{
	uint32_t fieldLength = 4;
	std::vector<uint8_t> numberV(fieldLength);
	uint32_t *hack = reinterpret_cast<uint32_t*>(numberV.data());

	*hack = number;
	m_outstream.insert(m_outstream.end(), numberV.begin(), numberV.end());
}

void PostScrData::MakeScriptStringSection()
{
    std::string s;

	for (auto &strData : m_scrStrings)
	{
        s = strData->m_str;
		InsertNumber(s.length());
		m_outstream.insert(m_outstream.end(), s.begin(), s.end());
	}
}

void PostScrData::MakeFunctionSection()
{
	std::string functionHeader = "FUNC", functionName;
	std::string symbHeader = "SYMB";
	std::string dataHeader = "DATA";
	std::list<uint32_t> symbols, codeline;
	uint32_t codechunkSize = 4;

	functionName.reserve(256);
	for (const auto &functionRef : m_funcList)
	{
		std::shared_ptr<PostScrFunction> function = functionRef.lock();

		if (function)
		{
			m_outstream.insert(m_outstream.end(), functionHeader.begin(), functionHeader.end());
			functionName = function->Name();
			InsertNumber(functionName.length());
			m_outstream.insert(m_outstream.end(), functionName.begin(), functionName.end());
			InsertNumber(function->ReturnType());
			InsertNumber(function->ArgumentCount());
			m_outstream.insert(m_outstream.end(), symbHeader.begin(), symbHeader.end());
			symbols = function->SymbolList();
			InsertNumber(symbols.size());
			InsertNumber(0);
			for (const auto &symbol : symbols)
			{
				InsertNumber(symbol);
			}
			m_outstream.insert(m_outstream.end(), dataHeader.begin(), dataHeader.end());
			function->ExtractDataList(codeline);
			InsertNumber(codeline.size() * codechunkSize);
			for (const auto &code : codeline)
			{
				InsertNumber(code);
			}
		}
	}
}

bool PostScrData::MakeOutput()
{
	std::string scrHeader = "SCRIPT03";
	m_outstream.insert(m_outstream.end(), scrHeader.begin(), scrHeader.end());

	std::string scrstrHeader = "STRG";
	m_outstream.insert(m_outstream.end(), scrstrHeader.begin(), scrstrHeader.end());

	InsertNumber(m_scrStrings.size());
	MakeScriptStringSection();

	std::string codeheader = "CODE";
	m_outstream.insert(m_outstream.end(), codeheader.begin(), codeheader.end());

	InsertNumber(m_funcList.size());
	MakeFunctionSection();
	return true;
}

void PostScrData::ReportWriteReady(std::vector<uint8_t> *fstream)
{
	if (MakeOutput())
	{
		*fstream = m_outstream;
	}
}

