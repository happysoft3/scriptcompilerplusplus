
#ifndef OPTYPES_H__
#define OPTYPES_H__

#include <string>

enum class OpTypes
{
	OP_NONE,
	OP_ARROW,
	OP_A_ADD,
	OP_A_AND,
	OP_A_DIV,
	OP_A_MOD,
	OP_A_MUL,
	OP_A_OR,
	OP_A_SAL,
	OP_A_SAR,
	OP_A_SUB,
	OP_A_XOR,
	OP_DEC,
	OP_EQ,
	OP_GE,
	OP_INC,
	OP_LE,
	OP_LOGAND,
	OP_LOGOR,
	OP_NE,
	OP_SAL,
	OP_SAR,

	OP_XOR,
	OP_MULPLY,
	OP_ASIGN,
	OP_NOT,
	OP_MOD,
	OP_DIV,
	OP_MINUS,
	OP_LEFT,
	OP_RIGHT
};

class OptypeChecker
{
private:
	explicit OptypeChecker();
	~OptypeChecker();

public:
	static OptypeChecker *GetInstance();
	bool IsAssignOpcode(OpTypes optype);
	std::string GetOpcodeIdent(OpTypes optype);
};

#endif

