
#ifndef MAKE_CODE_H__
#define MAKE_CODE_H__

#include "scriptProcess.h"

#include <memory>
#include <list>

#include <fstream>

class ScriptStream;
class FunctionPool;
class ScriptTypes;
class ScriptFunction;
class TokenGroup;
class ScriptUnit;
class ScriptVariable;
class ScriptString;

class MakeCode : public ScriptProcess
{
private:
	std::weak_ptr<ScriptStream> m_streamRef;
	std::weak_ptr<FunctionPool> m_functionPoolRef;
	std::weak_ptr<FunctionPool> m_builtinsPoolRef;
	std::weak_ptr<ScriptString> m_strManager;
	std::unique_ptr<ScriptTypes> m_typeChecker;

	std::weak_ptr<ScriptFunction> m_globalFunction;
	std::weak_ptr<ScriptFunction> m_currentFunction;

	std::weak_ptr<TokenGroup> m_rootGroup;

	using nodeErrorTypes = std::tuple<std::string, std::weak_ptr<ScriptUnit>, std::weak_ptr<ScriptFunction>>;
	std::list<nodeErrorTypes> m_errorList;

	uint32_t m_writeCodeline;
	std::ofstream m_fileoutLine;

	std::list<std::string> m_codeline;

	const std::string m_ignoreObjectType;

public:
	explicit MakeCode(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<FunctionPool> functionPoolRef, 
		std::shared_ptr<ScriptString> strManager, std::shared_ptr<FunctionPool> builtinsPoolRef);
	virtual ~MakeCode() override;

private:
	virtual void Initialize() override;
	std::list<std::string> WriteFunctionMeta(std::shared_ptr<ScriptUnit> scrunit);

	ScriptVariable *FindVar(std::shared_ptr<ScriptUnit> scrunit, bool noException = false);
	uint32_t GetBuiltins(const std::string &name);
	bool CheckValidFunction(const std::string &name);

    bool ArraySizeOfPrivate(const std::shared_ptr<ScriptUnit> &scrunit);

	void WriteCodeline(std::shared_ptr<ScriptUnit> scrunit);
	bool ArrayWriteCondition(std::shared_ptr<ScriptUnit> arr);
	bool StartWriteSymbols(std::shared_ptr<ScriptUnit> scrunit);
	bool EndWriteSymbols(std::shared_ptr<ScriptUnit> scrunit);

	bool WriteSpecialOp(std::shared_ptr<ScriptUnit> scrunit, uint32_t optype);

    void writeVarCondition(ScriptVariable *var);
	bool WriteKeyword(std::shared_ptr<ScriptUnit> scrunit);
	bool WriteLExpress(std::shared_ptr<ScriptUnit> lex);
	bool WriteScriptString(std::shared_ptr<ScriptUnit> scrstr);
	bool WriteChunk(std::shared_ptr<ScriptUnit> scrunit);
	bool WriteVarDeclare(std::shared_ptr<ScriptUnit> scrunit);

	void ErrorContinue() {}
	bool Make(std::shared_ptr<ScriptUnit> scrunit);

	void MakeCompleted();

	virtual bool Process() override;
	virtual void Deinitialize(bool pResult) override;
	void PrintErrorList();
};

#endif