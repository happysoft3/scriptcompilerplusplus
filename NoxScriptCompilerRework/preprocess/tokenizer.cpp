
#include "tokenizer.h"
#include "scriptStream.h"

#include "scriptToken.h"
#include "tokenGroup.h"
#include "scriptString.h"

#include <algorithm>
#include <iterator>

#define READ_C_EOF	-1


Tokenizer::Tokenizer(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<ScriptString> strManager)
	: ScriptProcess()
{
	m_streamRef = streamRef;
	m_strManager = strManager;
	m_strToken = strManager->StringSymbol();

	m_linenumber = 1;
}

Tokenizer::~Tokenizer()
{ }

int Tokenizer::Readc()
{
	if (m_tokens.empty())
		return READ_C_EOF;		//EOF

	int c = m_tokens.front();

	m_tokens.pop_front();
	return c;
}

void Tokenizer::Storec(int c)
{
	m_destTokens.push_back(static_cast<uint8_t>(c));
}

void Tokenizer::Unreadc(int c)
{
	m_tokens.push_front(static_cast<uint8_t>(c));
}

bool Tokenizer::Next(int expect)
{
	int c = Readc();

	if (c == expect)
		return true;
	Unreadc(c);
	return false;
}

int Tokenizer::Peek()
{
	int r = Readc();

	Unreadc(r);
	return r;
}

bool Tokenizer::SkipBlockComment()
{
	bool maybeEnd = false;

	while (1)
	{
		int c = Readc();
		if (c == READ_C_EOF)
			break;
		if (c == '/' && maybeEnd)
			return true;
		if (c == '\n')
		{
			++m_linenumber;
			Storec(c);
		}
		else
			maybeEnd = (c == '*');
	}
	return false;
}

void Tokenizer::SkipLineComment()
{
	while (1)
	{
		int c = Readc();
		if (c == READ_C_EOF)
			break;
		if (c == '\n')
		{
			Unreadc(c);
			break;
		}
	}
}

bool Tokenizer::IsWhiteSpace(int c)
{
	switch (c)
	{
	case ' ': case '\t': case '\f': case '\v':
		return true;
	}
	return false;
}

bool Tokenizer::DoSkipSpace()
{
	int c = Readc();

	if (c == READ_C_EOF)
		return false;
	if (IsWhiteSpace(c))
		return true;
	if (c == '/')
	{
		if (Next('*'))
		{
			SkipBlockComment();
			return true;
		}
		if (Next('/'))
		{
			SkipLineComment();
			return true;
		}
	}
	Unreadc(c);
	return false;
}

bool Tokenizer::SkipSpace()
{
	if (!DoSkipSpace())
		return false;
	while (DoSkipSpace());
	return true;
}

bool Tokenizer::Nextoct()
{
	int c = Peek();

	return '0' <= c && c <= '7';
}

int Tokenizer::ReadOctalChar(int c)
{
	int r = c - '0';

	if (!Nextoct())
		return r;
	r = (r << 3) | (Readc() - '0');
	if (!Nextoct())
		return r;
	return (r << 3) | (Readc() - '0');
}

std::list<char> Tokenizer::ReadEscapedChar()
{
	int c = Readc();

	if (c == READ_C_EOF)
		return{};

	switch (c)
	{
	case '\'': case '"': case '?': case '\\':
		return{ '\\', static_cast<char>(c) };
	case 'a': //return '\a';
	case 'b': //return '\b';
	case 'f': //return '\f';
	case 'n': //return '\n';
	case 'r': //return '\r';
	case 't': //return '\t';
	case 'v': //return '\v';
			  //return{'\\', c};
			  /*case 'e': return '\033';  // '\e' is GNU extension
			  case 'x': return read_hex_char();
			  case 'u': return read_universal_char(4);
			  case 'U': return read_universal_char(8);
			  case '0' ... '7': return read_octal_char(c);*/
		break;
	}
	return{ '\\', static_cast<char>(c) };
}

int Tokenizer::ReadEscapedCharReal()
{
	int c = Readc();

	if (c == READ_C_EOF)
		return -1;

	switch (c)
	{
	case '\'': case '"': case '?': case '\\':
		return c;
	case 'a': return '\a';
	case 'b': return '\b';
	case 'f': return '\f';
	case 'n': return '\n';
	case 'r': return '\r';
	case 't': return '\t';
	case 'v': return '\v';
	case 'e': return '\033';
	case '0': case '1': case '2': case '3':
	case '4': case '5': case '6': case '7':
		return ReadOctalChar(c);
	}
	return -1;	//unknown escape char
}

std::shared_ptr<ScriptUnit> Tokenizer::ReadString()
{
	std::list<char> buffer;

	for (; ; )
	{
		int c = Readc();
		if (c == READ_C_EOF)	//unterminated string
		{
			return MakeInvalid("unterminated string");
		}
		if (c == '\"')
			break;
		if (c != '\\')
		{
			buffer.push_back(static_cast<char>(c));
			continue;
		}
		const auto &&cs = ReadEscapedChar();

		if (cs.size() == 0)
			return MakeInvalid("unterminated escape char");
		for (const auto &e : cs)
			buffer.push_back(e);
	}
	char expr = '"';	//fuck them. i must append it at both ends
	std::string strkey = m_strToken + std::to_string(m_strManager.lock()->Append(expr + std::string(buffer.begin(), buffer.end()) + expr));

	return std::make_shared<ScriptToken>(strkey, m_linenumber, ScriptToken::SCR_STRING);
}

std::shared_ptr<ScriptUnit> Tokenizer::ReadChar()
{
	int c = Readc();
	auto r = (c == '\\') ? ReadEscapedCharReal() : c;

	if (r == -1)
		return MakeInvalid("unknown escape char");
	c = Readc();
	if (c != '\'')
		return MakeInvalid("unterminated char");	//error: unterminated char

	return std::make_shared<ScriptToken>(std::to_string(r), m_linenumber, ScriptToken::NUMBER);
}

std::shared_ptr<ScriptUnit> Tokenizer::ReadNumber(char c)
{
	std::list<char> buffer(1, c);
	char last = c;
	while (1)
	{
		int c = Readc();
		bool flonum = strchr("eEpP", last) && strchr("+-", c);
		if (!isdigit(c) && !isalpha(c) && c != '.' && !flonum)
		{
			Unreadc(c);
			return std::make_shared<ScriptToken>(std::string(buffer.begin(), buffer.end()), m_linenumber, ScriptToken::NUMBER);
		}
		buffer.push_back(c);
		last = c;
	}
}

std::shared_ptr<ScriptUnit> Tokenizer::ReadIdent(char c)
{
	std::list<char> buffer(1, c);

	while (1)
	{
		c = Readc();
		if (isalnum(c) || (c & 0x80) || c == '_' || c == '$')
		{
			buffer.push_back(c);
			continue;
		}
		Unreadc(c);
		auto &&key = std::string(buffer.begin(), buffer.end());
		bool commonkey = m_streamRef.lock()->FindCommonkey(key);

		return std::make_shared<ScriptToken>(key, m_linenumber, commonkey ? ScriptToken::SCR_COMMIT : ((buffer.front() == '#') ? ScriptToken::DEFINE : ScriptToken::KEYWORD));
	}
}

std::shared_ptr<ScriptUnit> Tokenizer::MakeOperator(OpTypes id, char n)
{
	auto opcheck = OptypeChecker::GetInstance();
	std::string sid = (n == 0) ? opcheck->GetOpcodeIdent(id) : std::string(1, n);
	std::shared_ptr<ScriptUnit> token = std::make_shared<ScriptToken>(sid, m_linenumber, ScriptToken::OPERATOR);
	auto tokenraw = static_cast<ScriptToken *>(token.get());

	if (!n)
		tokenraw->SetAssignop(opcheck->IsAssignOpcode(id));
	tokenraw->SetTokenOpType(id);
	return token;
}

std::shared_ptr<ScriptUnit> Tokenizer::ReadRepeat(char expect, OpTypes id, OpTypes els)
{
	return MakeOperator(Next(expect) ? id : els);
}

std::shared_ptr<ScriptUnit> Tokenizer::ReadRepeatBinaryOper(char expect1, OpTypes id1, char expect2, OpTypes id2, char els)
{
	if (Next(expect1))
		return MakeOperator(id1);
	if (Next(expect2))
		return MakeOperator(id2);
	else
		return MakeOperator(OpTypes::OP_NONE, els);
}

std::shared_ptr<ScriptUnit> Tokenizer::MakeNewscope(char c)
{
	TokenGroup *makenode = new TokenGroup(m_rootnode);
	std::shared_ptr<ScriptUnit> childnode(makenode);

	makenode->SetLinenumber(m_linenumber);
	makenode->SetGroupTypeFromChar(c);

	auto tail = m_rootnode->Back();
	if (tail)
	{
		auto type = tail->UnitTokenType();
		if (type == ScriptToken::KEYWORD || type == ScriptToken::SCR_COMMIT)
		{
			makenode->SetGroupName(tail->UnitName());
			m_rootnode->Popback();
		}
	}

	PushScopeStack(childnode);
	
	return childnode;
}

std::shared_ptr<ScriptUnit> Tokenizer::MakeEndscope(char c)
{
	auto parent = m_rootnode->GetParent();

	if (parent)
		m_rootnode = parent;
	else
		return MakeInvalid("scope parent unmatched");

	if (!PushScopeStack(std::make_shared<ScriptToken>(std::string(1, c), m_linenumber, ScriptToken::NONE)))
		return MakeInvalid("scope pair dismatched");

	return std::make_shared<ScriptToken>("endscope", m_linenumber, ScriptToken::END_SCOPE);
}

bool Tokenizer::PushScopeStack(std::shared_ptr<ScriptUnit> scope)
{
	if (scope->UnitType() == ScriptUnit::GROUP)
	{
		m_scopeStack.push_back(scope.get());
		return true;
	}
	else
	{
		if (static_cast<TokenGroup *>(m_scopeStack.back())->GetScopeName() == scope->UnitName()[0])
		{
			m_scopeStack.pop_back();
			return true;
		}
	}
	return false;
}

std::shared_ptr<ScriptUnit> Tokenizer::MakeInvalid(const std::string &name)
{
	return std::make_shared<ScriptToken>(name, m_linenumber, ScriptToken::INVALID);
}

void Tokenizer::Initialize()
{
	std::shared_ptr<ScriptStream> streamRef = m_streamRef.lock();

	if (streamRef)
		m_tokens = streamRef->GetListStream();
}

std::shared_ptr<ScriptUnit> Tokenizer::DoReadToken()
{
	if (SkipSpace())
		return std::make_shared<ScriptToken>("space", m_linenumber, ScriptToken::SPACE_TOKEN);
	//mark()
	int c = Readc();

	switch (c)
	{
	case '\r': return std::make_shared<ScriptToken>("nothing", m_linenumber, ScriptToken::NONE);
	case '\n': return std::make_shared<ScriptToken>(std::string(1, '\n'), m_linenumber++, ScriptToken::NEW_LINE);
	//case ':': return nullptr; //nothing not yet
	case '#': return ReadIdent(c);
	case '+': return ReadRepeatBinaryOper('+', OpTypes::OP_INC, '=', OpTypes::OP_A_ADD, '+');
	case '*': return ReadRepeat('=', OpTypes::OP_A_MUL, OpTypes::OP_MULPLY);
	case '=': return ReadRepeat('=', OpTypes::OP_EQ, OpTypes::OP_ASIGN);
	case '!': return ReadRepeat('=', OpTypes::OP_NE, OpTypes::OP_NOT);
	case '&': return ReadRepeatBinaryOper('&', OpTypes::OP_LOGAND, '=', OpTypes::OP_A_AND, '&');
	case '|': return ReadRepeatBinaryOper('|', OpTypes::OP_LOGOR, '=', OpTypes::OP_A_OR, '|');
	case '^': return ReadRepeat('=', OpTypes::OP_A_XOR, OpTypes::OP_XOR);
	case '"': return ReadString();
	case '\'': return ReadChar();
	case '/': return MakeOperator(Next('=') ? OpTypes::OP_A_DIV : OpTypes::OP_DIV);
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
		return ReadNumber(c);
	case '.':
		if (isdigit(Peek()))
			return ReadNumber(c);
		return std::make_shared<ScriptToken>(std::string(1, '.'), m_linenumber, ScriptToken::OPERATOR);
	case '(': case '[': case '{': return MakeNewscope(c);
	case ')': case ']': case '}': return MakeEndscope(c);
	case ':': case ',': case ';': case '?': case '~':
		return std::make_shared<ScriptToken>(std::string(1, static_cast<char>(c)), m_linenumber, ScriptToken::OPERATOR);
	case '-':
		if (Next('-')) return MakeOperator(OpTypes::OP_DEC);
		if (Next('=')) return MakeOperator(OpTypes::OP_A_SUB);
		return MakeOperator(OpTypes::OP_MINUS);
	case '<':
		if (Next('<')) return ReadRepeat('=', OpTypes::OP_A_SAL, OpTypes::OP_SAL);
		if (Next('=')) return MakeOperator(OpTypes::OP_LE);
		return MakeOperator(OpTypes::OP_RIGHT);
	case '>':
		if (Next('=')) return MakeOperator(OpTypes::OP_GE);
		if (Next('>')) return ReadRepeat('=', OpTypes::OP_A_SAR, OpTypes::OP_SAR);
		return MakeOperator(OpTypes::OP_LEFT);
	case '%': return ReadRepeat('=', OpTypes::OP_A_MOD, OpTypes::OP_MOD);
	case '_': case '$': return ReadIdent(c);
	case READ_C_EOF: return std::make_shared<ScriptToken>("eof", m_linenumber, ScriptToken::END_OF_FILE);
	default:
		if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
			return ReadIdent(c);
		return MakeInvalid("unknown token " + std::to_string(c));
	}
}

bool Tokenizer::Process()
{
	m_rootnode = std::make_shared<TokenGroup>();
	m_rootbase = m_rootnode;

	while (m_tokens.size())
	{
		auto &&token = DoReadToken();

		if (token == nullptr)
			return false;
		if (token->UnitType() == ScriptUnit::SINGLE)
		{
			switch (token->UnitTokenType())
			{
			case ScriptToken::INVALID:
				std::cout << "error::" + token->UnitName() + ", line:" + std::to_string(token->Linenumber()) << std::endl;
				return false;
			case ScriptToken::END_OF_FILE:
				std::cout << "eof\n";
				return true;
			case ScriptToken::END_SCOPE:
			case ScriptToken::NONE: case ScriptToken::NEW_LINE: case ScriptToken::SPACE_TOKEN:
				break;
			case ScriptToken::OPERATOR:
			case ScriptToken::DEFINE: case ScriptToken::SCR_STRING: case ScriptToken::SCR_COMMIT:
			case ScriptToken::KEYWORD: case ScriptToken::NUMBER:
				m_rootnode->Append(token);
			}
		}
		else	//pack
		{
			m_rootnode->Append(token);
			m_rootnode = std::dynamic_pointer_cast<TokenGroup>(token);
		}
	}
	return true;
}

void Tokenizer::Deinitialize(bool pResult)
{
	if (!pResult)
		return;

	auto streamRef = m_streamRef.lock();

	if (streamRef)
	{
		m_rootbase = nullptr;
		streamRef->SetRootToken(std::move(m_rootnode));
	}
}

std::string Tokenizer::ProcessName()
{
	return "lexer";
}