
#include "systemEnv.h"

#include "resource.h"
#include <Windows.h>

#include <mmsystem.h>

#pragma comment(lib, "winmm.lib")
#include <iostream>


static SystemEnv *s_systemEnv = nullptr;

SystemEnv::SystemEnv()
{
	s_systemEnv = this;
	m_appVersion = 174;
	m_hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);

	GetConsoleMode(m_hConsoleInput, reinterpret_cast<LPDWORD>(&m_prevMode));
	SetConsoleMode(m_hConsoleInput, m_prevMode & ENABLE_EXTENDED_FLAGS);
	SetConsoleOutputCP(CP_UTF8);	//allow print with utf-8

	m_hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	m_prevColor = ConsoleColor::COLOR_DARKWHITE;
}

SystemEnv::~SystemEnv()
{
	SetConsoleMode(m_hConsoleInput, m_prevMode);
	ResetTextColor();

	s_systemEnv = nullptr;
}

void SystemEnv::ChangeTextColor(SystemEnv::ConsoleColor color)
{
	SetConsoleTextAttribute(m_hConsoleOutput, static_cast<WORD>(color));
}

void SystemEnv::ResetTextColor()
{
	ChangeTextColor(m_prevColor);
}

SystemEnv *SystemEnv::GetInstance()
{
	if (s_systemEnv == nullptr)
		return new SystemEnv();
	return s_systemEnv;
}

void SystemEnv::PlayWav(uint32_t resourceId)
{
	uint32_t rid;
	switch (resourceId)
	{
	case 0:
		rid = IDR_WAVE1;
		break;
	case 1:
		rid = IDR_WAVE2;
		break;
	default:
		return;
	}
	PlaySound(MAKEINTRESOURCE(rid), nullptr, SND_RESOURCE | SND_ASYNC);
}