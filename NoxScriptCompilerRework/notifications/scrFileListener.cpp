
#include "scrFileListener.h"
#include "utils\stringUtils.h"

ScrFileListener::ScrFileListener()
{
	m_requestCb = nullptr;
}

ScrFileListener::~ScrFileListener()
{ }

void ScrFileListener::RegistRequestCallback(std::function<void(std::string, uint32_t)> cb)
{
	m_requestCb = cb;
}

bool ScrFileListener::CheckScriptFileExtension(const std::string &filename)
{
	uint32_t findExtent = filename.find_last_of('.');

	if (findExtent != std::string::npos)
	{
        std::string extension = filename.substr(findExtent);

        if (extension == ".c")
            return true;
        if (extension == ".h")
            return true;
	}
	return false;
}

void ScrFileListener::SendRequest(const std::wstring &path, uint32_t actionId)
{
	if (m_requestCb != nullptr)
	{
		std::wstring wfilename = path;
		std::string filename;

		StringUtils::UnicodeToAnsi(wfilename, filename);
		if (CheckScriptFileExtension(filename))
			m_requestCb(filename, actionId);
	}
}

void ScrFileListener::OnFileChange(const std::wstring& path)
{
	SendRequest(path, actionTypes::ACTION_CHANGED);
}

void ScrFileListener::OnFileAdded(const std::wstring& path)
{
	SendRequest(path, actionTypes::ACTION_ADDED);
}

void ScrFileListener::OnFileRemoved(const std::wstring& path)
{
	SendRequest(path, actionTypes::ACTION_DELETED);
}

void ScrFileListener::OnFileRenamed(const std::wstring& path)
{
	SendRequest(path, actionTypes::ACTION_RENAMED);
}
