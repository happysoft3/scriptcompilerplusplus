
#ifndef SCRIPT_VARIABLE_H__
#define SCRIPT_VARIABLE_H__

#include <string>

class ScriptFunction;

//@brief. 스크립트 변수 노드
class ScriptVariable
{
private:
	ScriptFunction *m_parentFunction;
	std::string m_name;
	uint32_t m_offset;
	uint32_t m_size;		//@brief. 배열의 경우 2 이상
	uint32_t m_type;
	bool m_isPointer;
	bool m_isArgument;

public:
	explicit ScriptVariable(ScriptFunction *parentFunction);
	~ScriptVariable();

	void SetType(uint32_t type) { m_type = type; }
	uint32_t Type() const { return m_type; }
	
	void SetName(const std::string &name) { m_name = name; }
	std::string Name() const { return m_name; }

	void SetOffset(uint32_t offset) { m_offset = offset; }
	uint32_t Offset() const { return m_offset; }

	void SetPointer() { m_isPointer = true; }
	bool IsPointer() { return m_isPointer; }
	void SetArgument() { m_isArgument = true; }
	bool IsArgument() { return m_isArgument; }

	void SetSize(uint32_t size) { m_size = size; }
	uint32_t Size() const { return m_size; }
    uint32_t AlignedSize() const;

	ScriptFunction *OwnerFunction() const { return m_parentFunction; }
};

#endif