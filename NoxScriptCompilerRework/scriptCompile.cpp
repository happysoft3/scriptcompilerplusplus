
#include "scriptCompile.h"
#include "preprocess\premodule.h"
#include "preprocess/scriptProcess.h"
#include "preprocess/tokenizer.h"
#include "preprocess/makeFunction.h"
#include "preprocess/importLib.h"
#include "preprocess/macroSwap.h"
#include "preprocess/makeVariable.h"
#include "preprocess/makeCode.h"
#include "preprocess/tokenFlatten.h"
#include "preprocess\premoduleProc.h"
#include "builtins/builtinsLoader.h"
#include "functionPool.h"
#include "scriptStream.h"
#include "scriptString.h"
#include "notifications\abstractFileListener.h"
#include "utils/timerThread.h"
#include "post/postProcess.h"
#include "utils/runclock.h"
#include "utils/stringHelper.h"
#include "systemEnv.h"

#include <algorithm>
#include "utils/incFilesystem.h"

#include <thread>
#include <future>

using namespace _StringHelper;


static ScriptCompile *s_compileInstance = nullptr;

ScriptCompile::ScriptCompile(std::shared_ptr<SystemEnv> systemEnv)
{
	m_builtinsPool = std::make_shared<FunctionPool>();	//빌트인 전용 입니다

	m_compilerPath = "noxscript";
	m_compilerExeName = "nsc.exe";
	m_running = false;

	s_compileInstance = this;

	m_systemEnv = systemEnv;

	m_modulePath = "bin\\";
}

ScriptCompile::~ScriptCompile()
{
	s_compileInstance = nullptr;
}

bool ScriptCompile::Initialize()
{
	if (!CompilerExist())
	{
		std::cout << "compiler is not exist\n";
		return false;
	}
	if (!LoadBuiltins())
	{
		std::cout << "builtins load fail!\n";
		return false;
	}
	else
	{
		std::shared_ptr<SystemEnv> env(m_systemEnv.lock());

		if (env)
		{
			env->ChangeTextColor(SystemEnv::ConsoleColor::COLOR_PINK);
			std::cout << "builtins loaded\n";
			env->ResetTextColor();
		}
	}
	return true;
}

void ScriptCompile::DoCompile(const std::string &filename)
{
	if (m_running)
		std::cout << "still running..\n";
	else
	{
		std::cout << filename << " file:: start compile\n";
		m_running = true;
		m_filename = filename;
		//here

		std::future<bool> presult = std::async(std::launch::async, [this]() { return this->Compile(); });
		//bool pResult = Compile();

		ReportCompileEnd(presult.get());
	}
}

void ScriptCompile::SetSomeUrl(const std::map<std::string, std::string> &urlpack)
{
	auto getUrl = [&urlpack](const std::string &key, std::string &dest)
	{
		auto keyIterator = urlpack.find(key);

		if (keyIterator != urlpack.end())
			dest = keyIterator->second;
	};

	getUrl("NoxPath", m_noxpath);
	getUrl("ScriptPath", m_scriptPath);
	getUrl("WorktreePath", m_repoPath);
	getUrl("ModulePath", m_modulePath);
	getUrl("HeaderPath", m_headerPath);

	if (m_headerPath.length() == 0)
		m_headerPath = m_scriptPath;
}

void ScriptCompile::SoundNotification(bool pResult)
{
	if (!m_systemEnv.expired())
	{
		std::shared_ptr<SystemEnv> env(m_systemEnv.lock());

		env->PlayWav(pResult ? 0 : 1);
	}
}

void ScriptCompile::ReportCompileEnd(bool pResult)
{
	if (m_running)
		m_running = false;
	m_filenameList.clear();

	if (m_systemEnv.expired())
		return;
	std::shared_ptr<SystemEnv> env(m_systemEnv.lock());

	env->ChangeTextColor(pResult ? SystemEnv::ConsoleColor::COLOR_CYAN : SystemEnv::ConsoleColor::COLOR_RED);
	std::cout << m_filename << " file compile result:: " << (pResult ? "OK" : "NG") << std::endl;
	SoundNotification(pResult);
	env->ResetTextColor();

	//brief. copy the file to repository path
	if (pResult && m_scrStream)
	{
		if (m_repoPath.length() > 0)
		{
			m_scrStream->ChangeFileName(m_repoPath + '\\' + m_filename);
			m_scrStream->ChangeFileMode(FileMode::FILE_MODE_WRITE);
			m_scrStream->FileProcess();
		}
	}
}

bool ScriptCompile::Compile()
{
	RunClock runclock;

	runclock.Reset();
	m_functionPool = std::make_shared<FunctionPool>();
	m_strManager = std::make_shared<ScriptString>();
	m_scrStream = std::make_shared<ScriptStream>(m_scriptPath + '\\' + m_filename);
	m_scrStream->SetCompilerPath(m_compilerPath);

	m_functionPool->MakeGlobal();

	std::string corelibname = "noxmemlib.h";
	std::string coremod = m_headerPath + '\\' + corelibname;
	if (!FileStream::Exist(coremod))
	{
		std::cout << coremod << " not exist\n";
		return false;
	}
    m_scrStream->SetHeaderPath(m_headerPath);
	m_scrStream->AppendModuleName(corelibname);

	if (m_scrStream->FileProcess())
	{
		m_processes.emplace_back(std::make_unique<Tokenizer>(m_scrStream, m_strManager));
        m_processes.emplace_back(std::make_unique<ImportLib>(m_scrStream, m_strManager));   	//added
		m_processes.emplace_back(std::make_unique<MacroSwap>(m_scrStream));
		m_processes.emplace_back(std::make_unique<MakeFunction>(m_scrStream, m_functionPool));
		m_processes.emplace_back(std::make_unique<MakeVariable>(m_scrStream, m_functionPool));
		m_processes.emplace_back(std::make_unique<TokenFlatten>(m_scrStream, m_functionPool));
		m_processes.emplace_back(std::make_unique<PremoduleProc>(m_functionPool, m_scrStream));
		m_processes.emplace_back(std::make_unique<MakeCode>(m_scrStream, m_functionPool, m_strManager, m_builtinsPool));

		bool pResult = true;
		std::string errorProcess;
		while (m_processes.size())
		{
			if (pResult)
			{
				m_processes.front()->Initialize();
				pResult = m_processes.front()->Process();

				m_processes.front()->Deinitialize(pResult);
				if (!pResult)
				{
					if (!errorProcess.length())
						errorProcess = m_processes.front()->ProcessName();
				}
			}
			m_processes.pop_front();
		}
		if (pResult == false)
			std::cout << "compile error -" << errorProcess << std::endl;
		else
		{
			std::cout << " compiling..." << std::endl;
			if (CompilerExec())
			{
				if (EntryPost())
				{
					ReportPost();
					pResult = true;
				}
				else
				{
					std::cout << "error in post\n";
					pResult = false;
				}
			}
			//std::cout << " end compile" << std::endl;
			std::cout << stringFormat("build complete. %s\n", runclock.Show());
		}
		return pResult;
	}
	else
		std::cout << "compile error::no such directory '" << m_filename << "' " << std::endl;
	return false;
}

bool ScriptCompile::CompilerExist()
{
	std::string compileFullPath = m_compilerPath + '\\' + m_compilerExeName;

	return NAMESPACE_FILESYSTEM::exists(compileFullPath);
}

bool ScriptCompile::CompilerExec()
{
	if (CompilerExist())
	{
		std::string cmd = stringFormat("cd %s & %s %sout.ns", m_compilerPath, m_compilerExeName, m_scrStream->ScriptFileName());

		m_resultObjFilename = stringFormat("%s\\%sout.obj", m_compilerPath, m_scrStream->ScriptFileName());;
		std::remove(m_resultObjFilename.c_str());
		std::system(cmd.c_str());
		return true;
	}
	return false;
}

bool ScriptCompile::LoadBuiltins()
{
	BuiltinsLoader loader(m_builtinsPool);

	loader.SetBuiltinsPath(m_compilerPath);
	return loader.Load();
}

void ScriptCompile::PopRequest()
{
	if (m_filenameList.size())
	{
		DoCompile(m_filenameList.back());
		m_filenameList.clear();
	}
	if (m_timerOn)
		m_timerOn = false;
}

void ScriptCompile::EndTimer()
{
	if (s_compileInstance == nullptr)
		return;
	s_compileInstance->PopRequest();
}

void ScriptCompile::PushRequest(const std::string &filename)
{
	if (!m_timerOn)
	{
		m_timerOn = true;

		TimerThread timer(1000, true, &ScriptCompile::EndTimer);
	}
	m_filenameList.push_back(filename);
}

void ScriptCompile::RequestCompile(std::string filename, uint32_t actionId)
{
	if (s_compileInstance == nullptr)
		return;
	switch (actionId)
	{
	case AbstractFileListener::actionTypes::ACTION_CHANGED:
		s_compileInstance->PushRequest(filename);
		break;
	case AbstractFileListener::actionTypes::ACTION_ADDED:
	case AbstractFileListener::actionTypes::ACTION_DELETED:
	case AbstractFileListener::actionTypes::ACTION_NONE:
	case AbstractFileListener::actionTypes::ACTION_RENAMED:
	default:
		break;
	}	
}

bool ScriptCompile::EntryPost()
{
	if (NAMESPACE_FILESYSTEM::exists(m_resultObjFilename))
	{
		m_postProc = std::make_unique<PostProcess>(m_resultObjFilename);

        m_postProc->SetMapPath(m_scrStream->ScriptFileName(), m_noxpath, m_modulePath);
		if (m_functionPool)
			m_postProc->RegistSpecialFunctions(m_functionPool->GetSpecialFunctions());

		std::list<std::string> modules = m_scrStream->GetModuleList();
		std::string libPath = m_modulePath;

		if (libPath.back() != '\\')
			libPath.push_back('\\');
		for (const auto &url : modules)
		{
			if (!m_postProc->AppendModule(libPath + FileStream::GetOnlyFilename(url), m_scrStream->ReleaseModuleObject(url), m_functionPool))
			{
				std::cout << url << " module loading error\n";
				return false;
			}
			else
				std::cout << url << " module loading... ok\n";
		}

		return m_postProc->Process();
	}
	else
		std::cout << m_resultObjFilename << " error\n";
	return false;
}

void ScriptCompile::ReportPost()
{
	std::shared_ptr<SystemEnv> env(m_systemEnv.lock());
	std::list<std::string> report = m_postProc->ReportResult();

	if (env)
	{
		env->ChangeTextColor(SystemEnv::ConsoleColor::COLOR_LIGHTGREEN);
		for (const std::string &str : report)
			std::cout << str;
		env->ResetTextColor();
	}
}

