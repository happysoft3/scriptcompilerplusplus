
#include "makeCode.h"
#include "scriptStream.h"
#include "functionPool.h"
#include "scriptFunction.h"
#include "scriptVariable.h"
#include "tokenGroup.h"
#include "scriptToken.h"
#include "scriptTypes.h"
#include "scriptString.h"
#include "utils\stringHelper.h"

#include "preprocess_generic.h"
#include "global/builtinsDef.h"
#include <sstream>

using namespace _StringHelper;

#define LINK_PATCHED

MakeCode::MakeCode(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<FunctionPool> functionPoolRef, std::shared_ptr<ScriptString> strManager, std::shared_ptr<FunctionPool> builtinsPoolRef)
	: ScriptProcess(), m_ignoreObjectType("\n//@ignore_object_type")
{
	m_streamRef = streamRef;
	m_functionPoolRef = functionPoolRef;
	m_builtinsPoolRef = builtinsPoolRef;
	m_typeChecker = std::make_unique<ScriptTypes>();
	m_strManager = strManager;

	m_writeCodeline = 1;
}

MakeCode::~MakeCode()
{ }

void MakeCode::Initialize()
{
	std::shared_ptr<FunctionPool> functionPool(m_functionPoolRef.lock());

	m_globalFunction = functionPool->GetGlobalFunction();
	m_currentFunction = m_globalFunction;

	functionPool->ResetListCursorPosition();

	std::shared_ptr<ScriptStream> streamRef(m_streamRef.lock());

	m_rootGroup = streamRef->GetTokenRoot();

	std::string outfilename = streamRef->CompilerPath() + '\\' + streamRef->ScriptFileName() + "out.ns";
	m_fileoutLine = std::ofstream(outfilename, std::ios::binary);

	//m_strSymbol = m_strManager.lock()->StringSymbol();
}

std::list<std::string> MakeCode::WriteFunctionMeta(std::shared_ptr<ScriptUnit> scrunit)
{
	//ret name(t n, t n...)
	std::shared_ptr<ScriptFunction> function = m_functionPoolRef.lock()->GetFunctionNode(scrunit->UnitName());
	std::list<std::string> destlist;

	if (!function)
	{
		m_errorList.emplace_back("undefined function", scrunit, m_currentFunction);
		return{};
	}
	std::list<std::weak_ptr<ScriptVariable>> arglist = function->GetArgumentList();
	uint32_t argcount = arglist.size();

	destlist.emplace_back(m_typeChecker->GetRealTypeName(function->GetReturnType()));	//return type
	destlist.emplace_back(' ' + function->FunctionName());	//FunctionName
	destlist.emplace_back(1, '(');
	for (auto &var : arglist)
	{
		destlist.emplace_back(m_typeChecker->GetRealTypeName(var.lock()->Type()));
		destlist.emplace_back(1, ' ');
		destlist.emplace_back(var.lock()->Name());
		argcount--;
		if (argcount)
			destlist.emplace_back(1, ',');
	}
	destlist.emplace_back(1, ')');
	return destlist;
}

ScriptVariable *MakeCode::FindVar(std::shared_ptr<ScriptUnit> scrunit, bool noException)
{
	ScriptVariable *var = m_currentFunction.lock()->GetVariable(scrunit->UnitName());

	while (var == nullptr)
	{
		if (m_currentFunction.lock() != m_globalFunction.lock())
		{
			var = m_globalFunction.lock()->GetVariable(scrunit->UnitName());
			if (var != nullptr)
				break;
		}
		if (!noException)
			m_errorList.emplace_back("undefined keyword", scrunit, m_currentFunction);
		return nullptr;
	}
	return var;
}

uint32_t MakeCode::GetBuiltins(const std::string &name)
{
	std::shared_ptr<FunctionPool> builtins = m_builtinsPoolRef.lock();

	if (builtins)
	{
		std::shared_ptr<ScriptFunction> function = builtins->GetFunctionNode(name);
		if (function)
			return function->FunctionNumber();
	}
	return -1;
}

bool MakeCode::CheckValidFunction(const std::string &name)
{
	if (name.length() == 0)
		return true;
	std::shared_ptr<FunctionPool> pool = m_functionPoolRef.lock();
	
	if (pool)
	{
		if (pool->GetFunctionNode(name))
			return true;
	}
	if (m_streamRef.lock()->FindCommonkey(name))	//added
		return true;
	return GetBuiltins(name) != -1;
}

bool MakeCode::ArraySizeOfPrivate(const std::shared_ptr<ScriptUnit> &scrunit)
{
    if (scrunit->UnitName() != "sizeof")
        return false;

    TokenGroup *gr = static_cast<TokenGroup*>(scrunit.get());

    if (gr->Front() != gr->Back())
    {
        m_errorList.emplace_back("wrong syntax", scrunit, m_currentFunction);
        return false;
    }

    if (gr->Front()->UnitType() != ScriptUnit::SINGLE)
    {
        m_errorList.emplace_back("support array type only", scrunit, m_currentFunction);
        return false;   //only array type
    }

    scrunit->SetUnitName({});
    gr->Front()->SetSpecialProperty(ScriptUnit::OP_ARR_SIZEOF);
    return true;
}

void MakeCode::WriteCodeline(std::shared_ptr<ScriptUnit> scrunit)
{
	uint32_t cline = scrunit->Linenumber();

	while (m_writeCodeline < cline)
	{
		m_fileoutLine << '\n';
		++m_writeCodeline;
	}
	for (auto &str : m_codeline)
	{
		m_fileoutLine << str;
	}
	m_codeline.clear();
}

bool MakeCode::ArrayWriteCondition(std::shared_ptr<ScriptUnit> arr)
{
	TokenGroup *gcast = static_cast<TokenGroup*>(arr.get());

	gcast->SetGroupType(TokenGroup::TINY_TYPE);
	if (arr->Declared())
	{
		m_errorList.emplace_back("array declare error", arr, m_currentFunction);
		return false;
	}
	else if (arr->LeftAssign())
	{
        std::string name = gcast->PointerArray() ? "VPointerOffsetToPtr" : "VOffsetToPtr";

		gcast->SetGroupName(name + m_typeChecker->GetUnDwordTypeName(gcast->ArrayType()));
	}
	else if (arr->GetSpecialProperty())
	{
        std::string name, nonDwordTyname = m_typeChecker->GetUnDwordTypeName(gcast->ArrayType());

		switch (arr->GetSpecialProperty())
		{
		case ScriptUnit::OP_AFTER_MINUS:
            name = gcast->PointerArray() ? "UOperatorAfterMinusPointer" : "UOperatorAfterMinus";
			break;
		case ScriptUnit::OP_AFTER_PLUS:
            name = gcast->PointerArray() ? "UOperatorAfterPlusPointer" : "UOperatorAfterPlus";
			break;
		case ScriptUnit::OP_BEFORE_MINUS:
            name = gcast->PointerArray() ? "UOperatorBeforeMinusPointer" : "UOperatorBeforeMinus";
			break;
		case ScriptUnit::OP_BEFORE_PLUS:
            name = gcast->PointerArray() ? "UOperatorBeforePlusPointer" : "UOperatorBeforePlus";
			break;
		case ScriptUnit::OP_RIGHT_REF:
            name = gcast->PointerArray() ? "VPointerOffsetToPtr" : "VOffsetToPtr";
			break;
        case ScriptUnit::OP_ARR_REF:
        {
            uint32_t arrTy = gcast->ArrayType();
            name = gcast->PointerArray() ? "UOperator%sPtrArrayRef" : "UOperator%sArrayRef";
            name = stringFormat(name, m_typeChecker->GetRealTypeName(arrTy));
            gcast->Append(std::make_unique<ScriptToken>(",", 0, ScriptToken::TokenType::OPERATOR));
            gcast->Append(std::make_unique<ScriptToken>(std::to_string(m_typeChecker->TypeSize(arrTy-1)), 0, ScriptToken::TokenType::NUMBER));
            nonDwordTyname.clear();
            break;
        }

		default:
			m_errorList.emplace_back("undefined action", arr, m_currentFunction);
			return false;
		}
        gcast->SetGroupName(name + nonDwordTyname);
	}
	else
	{
		std::string arrtype;
		if (gcast->ArrayType() > 0)
			arrtype = m_typeChecker->GetTypeName(gcast->ArrayType());
		else
		{
			m_errorList.emplace_back("array type error", arr, m_currentFunction);
			return false;
		}
		gcast->SetGroupName((gcast->PointerArray() ? "UOperatorGetPointerValue" : "UOperatorGetValue") + arrtype);
	}

	return true;
}

bool MakeCode::StartWriteSymbols(std::shared_ptr<ScriptUnit> scrunit)
{
	bool lExpr = false;

	if (scrunit->FunctionEntry())
	{
		m_currentFunction = m_functionPoolRef.lock()->NextFunction();
        auto currentFn = m_currentFunction.lock();
		std::list<std::string> funcMeta = WriteFunctionMeta(scrunit);

		while (funcMeta.size())
		{
			m_codeline.emplace_back(funcMeta.front());
			funcMeta.pop_front();
		}
        m_codeline.emplace_back(1, static_cast<TokenGroup*>(scrunit.get())->GetScopeName(true));

        //if (currentFn->VariableCount())
        //{
            if (!currentFn->IsExtern() || currentFn->IsOverride())  //-- 가져온 함수는, 아래줄이 실행되면 안됨!//
                m_codeline.emplace_back(stringFormat("%d;int %s=%d;", s_uniq_ident_num, s_this_function_id, currentFn->FunctionNumber()));
            //m_codeline.emplace_back(stringFormat("%d; int %s=%d;", s_uniq_ident_num, s_this_function_id, m_currentFunction.lock()->FunctionNumber()));
        //}
        
        return true;
	}

	if (static_cast<TokenGroup*>(scrunit.get())->GroupType() == TokenGroup::ARRAY_TYPE)
	{
		if (!ArrayWriteCondition(scrunit))
			return false;
	}
	else
	{
		while (!CheckValidFunction(scrunit->UnitName()))
		{
            if (ArraySizeOfPrivate(scrunit))
                break;

			m_errorList.emplace_back("undefined function", scrunit, m_currentFunction);
			return false;
		}
	}
	m_codeline.emplace_back(scrunit->UnitName());	//here
	m_codeline.emplace_back(1, static_cast<TokenGroup*>(scrunit.get())->GetScopeName(true));
	return true;
}

bool MakeCode::EndWriteSymbols(std::shared_ptr<ScriptUnit> scrunit)
{
	if (scrunit->FunctionEntry())
		m_currentFunction = m_globalFunction;
	m_codeline.emplace_back(1, static_cast<TokenGroup*>(scrunit.get())->GetScopeName());
	return true;
}

bool MakeCode::WriteSpecialOp(std::shared_ptr<ScriptUnit> scrunit, uint32_t optype)
{
	ScriptVariable *var = FindVar(scrunit);
	bool nothing(false);
    std::string byteTy = var->IsPointer() ? "" : m_typeChecker->GetUnDwordTypeName(var->Type());

	if (var == nullptr) return false;
	switch (optype)
	{
	case ScriptUnit::OP_AFTER_MINUS:
		m_codeline.emplace_back("UOperatorAfterMinus" + byteTy);
		break;
	case ScriptUnit::OP_AFTER_PLUS:
		m_codeline.emplace_back("UOperatorAfterPlus" + byteTy);
		break;
	case ScriptUnit::OP_BEFORE_MINUS:
		m_codeline.emplace_back("UOperatorBeforeMinus" + byteTy);
		break;
	case ScriptUnit::OP_BEFORE_PLUS:
		m_codeline.emplace_back("UOperatorBeforePlus" + byteTy);
		break;
	case ScriptUnit::OP_RIGHT_REF:
		m_codeline.emplace_back("UOperatorVOffsetToPtr" + m_typeChecker->GetRealTypeName(var->Type()));
		break;
    case ScriptUnit::OP_ARR_SIZEOF:
        if (var->Size() <= 1)
        {
            m_errorList.emplace_back("the variable is not an array", scrunit, m_currentFunction);
            return false;
        }
        m_codeline.emplace_back(std::to_string(var->Size()));
        nothing = true;
        break;
    case ScriptUnit::OP_ARR_REF:
        m_codeline.push_back("UOperatorArrayRef");
        break;
	default:
		nothing = true;
	}
	if (!nothing)
	{
		ScriptFunction *owner = var->OwnerFunction();

		if (owner == nullptr)
		{
			m_errorList.emplace_back("function is null pointer", scrunit, m_currentFunction);
			return false;
		}
		std::string functionNumber(std::to_string(owner->FunctionNumber()));
		std::string voffset(std::to_string(var->Offset()));
		//m_codeline.emplace_back('(' + functionNumber + ',' + voffset + ",0)");

        auto curFn = m_currentFunction.lock();
        //std::cout << stringFormat("cur func: %s, owner func: %s, var: %s\n", curFn->FunctionName(), owner->FunctionName(), var->Name());
        std::string fnId = curFn ? (curFn->FunctionNumber() == owner->FunctionNumber() ? s_this_function_id : functionNumber) : functionNumber;
        //m_codeline.emplace_back('(' + functionNumber + ',' + voffset + ",0)");
        m_codeline.emplace_back(stringFormat("(%s,%s,%d)", fnId, voffset, 0));
	}
	return true;
}

void MakeCode::writeVarCondition(ScriptVariable *var)
{
    if ((var->Type() == ScriptTypes::SCR_CHAR || var->Type() == ScriptTypes::SCR_SHORT) && !var->IsPointer())
    {
        if (var->Size() == 1)
        {
            auto curFn = m_currentFunction.lock();
            ScriptFunction *owner = var->OwnerFunction();
            std::string functionNumber(std::to_string(owner->FunctionNumber()));
            std::string voffset(std::to_string(var->Offset()));
            std::string fnId = curFn ? (curFn->FunctionNumber() == owner->FunctionNumber() ? s_this_function_id : functionNumber) : functionNumber;

            m_codeline.emplace_back(stringFormat("UOperatorGetValue%s(%s,%s,0)", m_typeChecker->GetTypeName(var->Type()), fnId, voffset));
            return;
        }
    }
    if (var->Size() > 1) //array type
    {
        auto globalFn = m_globalFunction.lock();
        std::string fnId = (var->OwnerFunction() == globalFn.get()) ? std::to_string(var->OwnerFunction()->FunctionNumber()) : s_this_function_id;
        m_codeline.push_back(stringFormat("UOperatorVOffsetToPtr%s(%s,%s,0)", m_typeChecker->GetRealTypeName( var->Type() ), fnId, std::to_string(var->Offset())) );
        return;
    }
    m_codeline.emplace_back(var->Name());
}

bool MakeCode::WriteKeyword(std::shared_ptr<ScriptUnit> scrunit)
{
	ScriptVariable *var = FindVar(scrunit, true);

    if (var != nullptr)
    {
        //std::cout << __FILE__ << __LINE__ << scrunit->UnitName() << std::endl;
        //m_codeline.emplace_back(scrunit->UnitName());
        writeVarCondition(var);
    }
	else
	{
		auto func = m_functionPoolRef.lock()->GetFunctionNode(scrunit->UnitName());
		if (func)
		{
            //Todo. 여기를 패치하면 됨.
#ifndef LINK_PATCHED
			m_codeline.emplace_back(std::to_string(func->FunctionNumber()));
#else
            m_codeline.emplace_back(stringFormat("%s(%d)", BUILTINS_LOAD_USER_FUNCTION, func->FunctionNumber())); //Todo. 글로벌 함수일 때에는, 이거 타면 안됨!!
#endif
		}
		else if (m_streamRef.lock()->FindCommonkey(scrunit->UnitName()))
		{
			m_codeline.emplace_back(scrunit->UnitName() + '_');
		}
		else
		{
			m_errorList.emplace_back("unknown keyword", scrunit, m_currentFunction);
			return false;
		}
	}
	return true;
}

bool MakeCode::WriteLExpress(std::shared_ptr<ScriptUnit> lex)
{
	ScriptVariable *var = FindVar(lex);
	if (var == nullptr)
		return false;

	ScriptFunction *owner = var->OwnerFunction();

	if (owner == nullptr)
	{
		m_errorList.emplace_back("function is null pointer", lex, m_currentFunction);
		return false;
	}
	std::string functionNumber(std::to_string(owner->FunctionNumber()));
	std::string voffset(std::to_string(var->Offset()));
	m_codeline.emplace_back("VOffsetToPtr" + m_typeChecker->GetUnDwordTypeName(var->Type()));

    auto curFn = m_currentFunction.lock();
    //std::cout << stringFormat("cur func: %s, owner func: %s, var: %s\n", curFn->FunctionName(), owner->FunctionName(), var->Name());
    std::string fnId = curFn ? (curFn->FunctionNumber() == owner->FunctionNumber() ? s_this_function_id : functionNumber) : functionNumber;
	//m_codeline.emplace_back('(' + functionNumber + ',' + voffset + ",0)");
    m_codeline.emplace_back(stringFormat("(%s,%s,%d)", fnId, voffset, 0));
	return true;
}

bool MakeCode::WriteScriptString(std::shared_ptr<ScriptUnit> scrstr)
{
	if (m_strManager.expired())
		return false;
	std::shared_ptr<ScriptString> strManager = m_strManager.lock();
	std::string scrstrValue = strManager->FindStringWithKey(scrstr->UnitName());

	if (scrstrValue.length())
	{
//#ifndef LINK_PATCHED
		m_codeline.emplace_back(scrstrValue);
//#else
//        m_codeline.emplace_back(stringFormat("%s(%s)", BUILTINS_LOAD_STRING, scrstrValue));
//#endif
		return true;
	}
	m_errorList.emplace_back("string key error", scrstr, m_currentFunction);
	return false;
}

bool MakeCode::WriteChunk(std::shared_ptr<ScriptUnit> scrunit)
{
	if (scrunit->Declared())
	{
		if (!WriteVarDeclare(scrunit))
			return false;
	}
	else if (scrunit->UnitTokenType() == ScriptToken::KEYWORD)
	{
		//var_key -- inc, dec, ref
		//function_key
		if (scrunit->LeftAssign())
		{
			if (!WriteLExpress(scrunit))
				return false;
		}
		else if (scrunit->GetSpecialProperty())	//var_key -- inc, dec, ref
		{
			if (!WriteSpecialOp(scrunit, scrunit->GetSpecialProperty()))
				return false;
		}
		else
		{
			if (!WriteKeyword(scrunit))
				return false;
		}
	}
	else if (scrunit->UnitTokenType() == ScriptToken::SCR_COMMIT)
	{
		m_codeline.emplace_back(scrunit->UnitName() + ' ');
	}
	else if (scrunit->UnitTokenType() == ScriptToken::SCR_STRING)
	{
		if (!WriteScriptString(scrunit))
			return false;
	}
	else if (scrunit->UnitTokenType() == ScriptToken::DEFINE)
		m_errorList.emplace_back("unknown keyword", scrunit, m_currentFunction);
	else
		m_codeline.emplace_back(scrunit->UnitName());
	return true;
}

bool MakeCode::WriteVarDeclare(std::shared_ptr<ScriptUnit> scrunit)
{
	ScriptVariable *var = m_currentFunction.lock()->GetVariable(scrunit->UnitName());

	if (var == nullptr)
	{
		m_errorList.emplace_back("var declare::unknown error", scrunit, m_currentFunction);
		return false;
	}
	m_codeline.emplace_back(m_typeChecker->GetRealTypeName(var->Type()));
	m_codeline.emplace_back(1, ' ');
	m_codeline.emplace_back(scrunit->UnitName());
	if (var->Size() > 1) //declare array
		m_codeline.emplace_back('[' + std::to_string(var->AlignedSize()) + ']');
	return true;
}

bool MakeCode::Make(std::shared_ptr<ScriptUnit> scrunit)
{
	std::shared_ptr<FunctionPool> functionPool(m_functionPoolRef.lock());
	std::list<std::shared_ptr<ScriptUnit>> tokens = dynamic_cast<TokenGroup*>(scrunit.get())->GetList();

	while (tokens.size())
	{
		if (tokens.front()->UnitType() == TokenGroup::GROUP)
		{
			if (tokens.front()->Declared())
			{
				if (!WriteVarDeclare(tokens.front()))
					ErrorContinue();
					//return false;
			}
			else
			{
				if (!StartWriteSymbols(tokens.front()))
					ErrorContinue();
					//return false;
				if (m_codeline.size())	//added	//@brief. 재귀 진입전에 한번 써야할 것 같아서...
					WriteCodeline(tokens.front());	//added
				if (!Make(tokens.front()))
					ErrorContinue();
					//return false;
				EndWriteSymbols(tokens.front());
			}
		}
		else
		{
			if (!WriteChunk(tokens.front()))
				ErrorContinue();
				//return false;
		}
		if (m_codeline.size())
			WriteCodeline(tokens.front());
		tokens.pop_front();
	}
	return true;
}

void MakeCode::MakeCompleted()
{
	std::shared_ptr<FunctionPool> pool(m_functionPoolRef.lock());

	m_fileoutLine << m_ignoreObjectType;
}

bool MakeCode::Process()
{
	bool pResult = false;

	if (m_fileoutLine.is_open())
	{
		pResult = Make(m_rootGroup.lock());

		if (pResult)
			MakeCompleted();
		m_fileoutLine.close();
		pResult = m_errorList.size() == 0;
		PrintErrorList();
		if (!pResult || (m_errorList.size() != 0))
			return false;
	}
	else
		std::cout << "can not open the file\n";
	return pResult;
}

void MakeCode::Deinitialize(bool pResult)
{ }

void MakeCode::PrintErrorList()
{
	while (m_errorList.size())
	{
		std::string errorMessage = std::get<0>(m_errorList.front());
		std::shared_ptr<ScriptUnit> scrUnit = std::get<1>(m_errorList.front()).lock();
		std::shared_ptr<ScriptFunction> errFunction = std::get<2>(m_errorList.front()).lock();

		if (errFunction)
			ErrorReport("error::", scrUnit->UnitName(), " :: ", errorMessage, " in function: ", errFunction->FunctionName(), "::line:", scrUnit->Linenumber());
		else
			ErrorReport("error::", scrUnit->UnitName(), " :: ", errorMessage, "::line:", scrUnit->Linenumber());
		m_errorList.pop_front();
	}
}
