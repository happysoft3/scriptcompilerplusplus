
#ifndef SYSTEM_ENV_H__
#define SYSTEM_ENV_H__

#include <stdint.h>
#include <memory>

class SystemEnv
{
public:
	enum class ConsoleColor
	{
		COLOR_DARKBLUE = 1,
		COLOR_GREEN,
		COLOR_EMERALD,
		COLOR_DARKRED,
		COLOR_VIOLET,
		COLOR_DARKYELLOW,
		COLOR_DARKWHITE = 7,
		COLOR_GREY = 8,
		COLOR_BLUE = 9,
		COLOR_LIGHTGREEN = 10,
		COLOR_CYAN,
		COLOR_RED,
		COLOR_PINK,
		COLOR_YELLOW,
		COLOR_WHITE
	};

private:
	using HandlePointer = void*;
	HandlePointer m_hConsoleInput;
	HandlePointer m_hConsoleOutput;
	uint32_t m_prevMode;
	uint32_t m_appVersion;
	
	ConsoleColor m_prevColor;

private:
	explicit SystemEnv();

public:
	~SystemEnv();

	void ChangeTextColor(ConsoleColor color);
	void ResetTextColor();
	static SystemEnv* GetInstance();
	uint32_t AppVersion() const { return m_appVersion; }

	void PlayWav(uint32_t resourceId);
};

#endif