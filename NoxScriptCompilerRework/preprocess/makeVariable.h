
#ifndef MAKE_VARIABLE_H__
#define MAKE_VARIABLE_H__

#include "scriptProcess.h"

#include <list>
#include <memory>
#include <vector>

class ScriptStream;
class FunctionPool;
class ScriptFunction;
class ScriptToken;
class ScriptTypes;
class ScriptUnit;
class TokenGroup;
class ScriptVariable;

//@brief. 함수의 변수를 만드는 루틴입니다
class MakeVariable : public ScriptProcess
{
private:
	std::weak_ptr<ScriptStream> m_streamRef;
	std::weak_ptr<FunctionPool> m_functionPoolRef;
	std::weak_ptr<ScriptFunction> m_globalFunction;
	std::unique_ptr<ScriptTypes> m_typeChecker;

	std::weak_ptr<TokenGroup> m_rootGroup;

	std::vector<std::weak_ptr<ScriptToken>> m_makeArgV;

	std::list<std::weak_ptr<ScriptUnit>> m_varStack;
	std::list<std::shared_ptr<ScriptUnit>> m_arrInitStack;

	using nodeErrorTypes = std::tuple<std::string, std::weak_ptr<ScriptUnit>, std::weak_ptr<ScriptFunction>>;
	std::list<nodeErrorTypes> m_errorList;

	uint32_t m_latestTypeNumber;

public:
	explicit MakeVariable(std::shared_ptr<ScriptStream> streamRef, std::shared_ptr<FunctionPool> functionPoolRef);
	virtual ~MakeVariable() override;

private:
	virtual void Initialize() override;
	bool PushStacklist(std::shared_ptr<ScriptUnit> unit, std::shared_ptr<ScriptFunction> currentFunction);
	uint32_t GetArrayInitSize(TokenGroup *initList, ScriptVariable *var);
	bool PushArrayStacklist(std::shared_ptr<ScriptUnit> unit, std::shared_ptr<ScriptFunction> currentFunction);
	bool Make(std::shared_ptr<ScriptUnit> currentGroup, std::shared_ptr<ScriptFunction> currentFunction);
	
	bool AddVariable(std::shared_ptr<ScriptUnit> typekey, std::shared_ptr<ScriptUnit> varunit, std::shared_ptr<ScriptFunction> cFunction, bool setpointer = false);
	bool ArrayInitList(std::shared_ptr<ScriptUnit> arr, std::shared_ptr<ScriptUnit> initlist, std::shared_ptr<ScriptFunction> cFunction);
	bool DeclareArray(std::shared_ptr<ScriptUnit> typekey, std::shared_ptr<ScriptUnit> arr, std::shared_ptr<ScriptFunction> cFunction);
	bool DeclareVariable(std::shared_ptr<ScriptUnit> typekey, std::shared_ptr<ScriptUnit> front, std::shared_ptr<ScriptUnit> back, std::shared_ptr<ScriptFunction> cFunction);
	bool Make2(std::shared_ptr<ScriptUnit> currentGroup, std::shared_ptr<ScriptFunction> cFunction);

	virtual bool Process() override;
	virtual void Deinitialize(bool pResult) override;

	void PrintErrorList();
};

#endif