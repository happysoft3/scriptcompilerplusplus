
#ifndef SCR_FILE_LISTENER_H__
#define SCR_FILE_LISTENER_H__

#include "abstractFileListener.h"

#include <functional>

class ScrFileListener : public AbstractFileListener
{
private:
	std::function<void(std::string, uint32_t)> m_requestCb;

public:
	explicit ScrFileListener();
	virtual ~ScrFileListener() override;

	void RegistRequestCallback(std::function<void(std::string, uint32_t)> cb);

private:
	bool CheckScriptFileExtension(const std::string &filename);
	void SendRequest(const std::wstring &path, uint32_t actionId);
	virtual void OnFileChange(const std::wstring& path) override;
	virtual void OnFileAdded(const std::wstring& path) override;
	virtual void OnFileRemoved(const std::wstring& path) override;
	virtual void OnFileRenamed(const std::wstring& path) override;
};

#endif