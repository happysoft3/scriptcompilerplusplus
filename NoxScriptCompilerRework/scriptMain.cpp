
#include "scriptMain.h"
#include "prepare/pathReader.h"
#include "notifications/scrFileWatcher.h"
#include "notifications/scrFileListener.h"
#include "scriptCompile.h"
#include "systemEnv.h"

ScriptMain::ScriptMain(std::shared_ptr<SystemEnv> sysEnv)
{
	m_systemEnv = sysEnv;
}

ScriptMain::~ScriptMain()
{
	m_compile.reset();
}

bool ScriptMain::Initialize()
{
	if (!FetchPaths())
		return false;

	m_systemEnv->ChangeTextColor(SystemEnv::ConsoleColor::COLOR_PINK);
	std::cout << "script url loaded\n";
	m_systemEnv->ResetTextColor();

	if (!InitBeforeCompile())
		return false;

	if (!InitFileWatcher())
		return false;

	std::cout << "nox script compiler plus::version::" << m_systemEnv->AppVersion() << std::endl;

	return true;
}

int ScriptMain::DirExists(const std::string &path)
{
	struct stat info;

	int statRC = stat(path.c_str(), &info);
	if (statRC != 0)
	{
		if (errno == ENOENT) { return 0; } // something along the path does not exist
		if (errno == ENOTDIR) { return 0; } // something in path prefix is not a dir
		return -1;
	}
	return (info.st_mode & S_IFDIR) ? 1 : 0;
}

bool ScriptMain::PushURL(const std::string &key)
{
	std::string url = m_pathReader->FindPath(key, true);

	if (url.length())
	{
		if (DirExists(url) == 1)
		{
			m_urlPack.emplace(key, url);
			return true;
		}
		m_lasterrorPath = url;
	}
	return false;
}

bool ScriptMain::FetchPaths()
{
	m_pathReader = std::make_unique<PathReader>();

	while (m_pathReader->FileProcess() != 0)
	{
		if (!PushURL("NoxPath"))
			break;
		if (!PushURL("ScriptPath"))
			break;
		PushURL("WorktreePath");
		PushURL("ModulePath");
		PushURL("HeaderPath");

		auto pir = m_urlPack.find("ScriptPath");

		if (pir != m_urlPack.end())
			m_scrpath = pir->second;
		return true;
	}
	std::cout << m_lasterrorPath << " is not a path\n";
	return false;
}

bool ScriptMain::InitBeforeCompile()
{
	m_compile = std::make_unique<ScriptCompile>(m_systemEnv);

	m_compile->SetSomeUrl(m_urlPack);
	return m_compile->Initialize();
}

bool ScriptMain::InitFileWatcher()
{
	m_fileListener = std::make_unique<ScrFileListener>();

	if (m_compile)
		m_fileListener->RegistRequestCallback(ScriptCompile::RequestCompile);
	if (m_scrpath.length() == 0)
		return false;
	m_fileWatcher = std::make_unique<ScrFileWatcher>(m_scrpath);

	m_fileWatcher->AddFileChangeListener(m_fileListener.get());

	return m_fileWatcher->Start();
}