
#ifndef IMPORT_LIB_H__
#define IMPORT_LIB_H__

#include "scriptProcess.h"

#include <memory>
#include <list>

class ScriptStream;
class ScriptUnit;
class TokenGroup;
class ScriptString;

class ImportLib : public ScriptProcess
{
private:
    bool m_isMain;
    std::weak_ptr<ScriptStream> m_selfstreamRef;
    std::weak_ptr<ScriptStream> m_mainstreamRef;
    std::weak_ptr<ScriptString> m_strManagerRef;
    std::weak_ptr<TokenGroup> m_rootGroup;
    const std::string m_importkey;

    using macroErrorTypes = std::tuple<std::string, std::weak_ptr<ScriptUnit>>;
    std::list<macroErrorTypes> m_errorList;

    std::string m_additionPath;

public:
    explicit ImportLib(std::shared_ptr<ScriptStream> selfStreamRef, std::shared_ptr<ScriptString> strManagerRef, std::shared_ptr<ScriptStream> mainStreamRef = nullptr);
    virtual ~ImportLib() override;

private:
    bool ImportModule(const std::string &modfullname, std::shared_ptr<ScriptUnit> cToken);
    std::string FixPathToken(const std::string &src);
    bool Traversal(std::shared_ptr<ScriptUnit> root);

private:
    virtual void Initialize() override;
    virtual bool Process() override;
    virtual void Deinitialize(bool pResult) override;
    void PrintErrorList();
};

#endif

