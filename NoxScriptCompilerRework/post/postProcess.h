
#ifndef POST_PROCESS_H__
#define POST_PROCESS_H__

#include <memory>
#include <string>
#include <list>
#include <set>
#include <map>

class PostScrData;
class PostStream;
class PostModule;
class PreModule;

class FunctionPool;

class PostProcess
{
private:
	std::shared_ptr<PostScrData> m_postData;
	std::unique_ptr<PostStream> m_postStream;
	std::string m_filename;
	std::string m_mapname;
	std::string m_noxpath;
    std::string m_altpath;

	std::list<std::unique_ptr<PostModule>> m_modules;

	std::list<std::string> m_extCallFunctions;
	std::set<std::string> m_extDupCheck;

	std::map<std::string, uint32_t> m_specialFuncs;

public:
	explicit PostProcess(const std::string &filename);	//filename= 처리하려는 파일 풀네임: noxscript\\xx.obj
	~PostProcess();

	bool SetMapPath(const std::string &mapname, const std::string &noxpath, const std::string &alt);	//mapname 은 말그대로 맵이름만
	bool Process();

	bool AppendModule(const std::string &modulePath, std::shared_ptr<PreModule> mod, std::shared_ptr<FunctionPool> preprocFnMap);
	std::list<std::string> ReportResult();

	void RegistSpecialFunctions(std::map<std::string, uint32_t> &&specials);

private:
	bool InsertCallModuleCode();
	bool InsertModule();
	bool CheckMapDirectory();
	std::string GetOutputObjectFileName();

	bool RegistNeedinitFunction(const std::string &name);
	uint32_t FindSpecialFunction(const std::string &key);
};

#endif